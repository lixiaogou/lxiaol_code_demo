package cn.lxiaol.login;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author lxiaol
 * @date 2021年05月08日 15:30
 */

public class LoginServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        if ("123".equals(username) && "123".equals(password)) {

            //登录 成功
            Cookie cookie = new Cookie("username", username);
            cookie.setMaxAge(60 * 60 * 24 * 7);//当前Cookie一周内有效
            resp.addCookie(cookie);

            System.out.println("登录 成功");
            resp.getWriter().write("登录 成功");

        } else {
//            登录 失败
            Cookie cookie = new Cookie("username", username);
            cookie.setMaxAge(0);//当前Cookie一周内有效
            resp.addCookie(cookie);

            System.out.println("登录 失败");
            resp.getWriter().write("登录 失败");

        }

    }
}
