package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_file_path")
@ApiModel(value = "AoaFilePath", description = "")
public class AoaFilePath implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "path_id", type = IdType.AUTO)

    private Long pathId;
    private Long parentId;
    private String pathName;
    private Long pathUserId;
    private Long pathIstrash;


}

