package cn.lxiaol.stream;


import java.util.ArrayList;
import java.util.List;

/**
 * 顾名思义，缩减操作，就是把一个流缩减成一个值，比如对一个集合求和、求乘积等。
 *
 * @author lxiaol
 * @date 2021年05月18日 17:35
 */
public class ReduceDemo2 {

    public static void main(String[] args) {
//        对象集合求和、求最值：
        List<Person> list = new ArrayList<>();
        list.add(new Person("Tom", 8900));
        list.add(new Person("Jack", 7000));
        list.add(new Person("Lily", 9000));

        // salary 求和，需要先获取所有salary 再求和
        Integer sum1 = list.stream().map(Person::getSalary).reduce(0, Integer::sum);
        System.out.println("sum1 = " + sum1);

        // 求最值
        Integer max1 = list.stream().map(Person::getSalary).reduce(0, Integer::max);
        System.out.println("max1 = " + max1);

    }
}
