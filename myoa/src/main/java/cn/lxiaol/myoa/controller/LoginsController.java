package cn.lxiaol.myoa.controller;

import cn.lxiaol.myoa.common.constants.Constants;
import cn.lxiaol.myoa.common.utils.VerifyCodeUtils;
import cn.lxiaol.myoa.entity.db1.AoaUser;
import cn.lxiaol.myoa.entity.db1.AoaUserLoginRecord;
import cn.lxiaol.myoa.mapper.db1.AoaUserMapper;
import cn.lxiaol.myoa.service.AoaUserLoginRecordService;
import cn.lxiaol.myoa.service.AoaUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Objects;
import java.util.Random;

@Slf4j
@Api(value = "/", tags = {"登陆相关接口"})
@Controller
@RequestMapping("/")
public class LoginsController {

    @Resource
    private AoaUserService aoaUserService;
    @Resource
    private AoaUserLoginRecordService aoaUserLoginRecordService;
    @Resource
    private AoaUserMapper aoaUserMapper;

    private Random rnd = new Random();

    /**
     * 登录界面的显示
     *
     * @return
     */
    @GetMapping(value = "logins")
    public String login() {
        return "login/login";
    }


    /**
     * 登录检查；
     * 1、根据(用户名或电话号码)+密码进行查找
     * 2、判断使用是否被冻结；
     *
     * @param session session
     * @param req     req
     * @param model   spring 默认的前后台传参的模型 类似于map
     * @return 页面
     * @throws UnknownHostException 未知端口异常
     */
    @ApiOperation(value = "登陆")
    @PostMapping(value = "logins")
    public String loginCheck(HttpSession session, HttpServletRequest req, Model model) throws UnknownHostException {
        String userName = req.getParameter("userName").trim();
        String password = req.getParameter("password");
        String ca = req.getParameter("code").toLowerCase();
        String sesionCode = (String) req.getSession().getAttribute(Constants.CAPTCHA_KEY);

        model.addAttribute("userName", userName);
//        if (!ca.equals(sesionCode.toLowerCase())) {
//            System.out.println("验证码输入错误!");
//            model.addAttribute("errormess", "验证码输入错误!");
//            return "login/login";
//        }

        /*
         * 将用户名分开查找；用户名或者电话号码；
         * 找不到返回用户不存在，
         * 找到了判断密码是否正确，提示密码错误
         * 密码也对，判断该账户是否冻结，提示冻结
         */
        LambdaQueryWrapper<AoaUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.nested(qw -> qw.eq(AoaUser::getUserName, userName).or().eq(AoaUser::getUserTel, userName));
        AoaUser aoaUser = aoaUserService.getOne(queryWrapper);
        // 账号不存在
        if (Objects.isNull(aoaUser)) {
            System.out.println("账号不存在!");
            model.addAttribute("errormess", "账号不存在!");
            return "login/login";
        }
        // 密码不对
        if (!password.equals(aoaUser.getPassword())) {
            System.out.println("密码错误!");
            model.addAttribute("errormess", "密码错误!");
            return "login/login";
        }

        // 账号冻结
        System.out.println("是否被锁：" + aoaUser.getIsLock());
        if (1 == aoaUser.getIsLock()) {
            System.out.println("账号已被冻结!");
            model.addAttribute("errormess", "账号已被冻结!");
            return "login/login";
        }

        // 账号已在线
        Object sessionId = session.getAttribute("userId");
        if (Objects.nonNull(sessionId) && sessionId == aoaUser.getUserId()) {
            session.setAttribute("thisuser", aoaUser);
            model.addAttribute("hasmess", "当前用户已经登录了；不能重复登录");
            return "login/login";
        }

        session.setAttribute("userId", aoaUser.getUserId());
        //获取系统的浏览器信息，包括手机和平板。
        String reqHeader = req.getHeader("User-Agent");
        Browser userAgent = UserAgent.parseUserAgentString(reqHeader).getBrowser();
        Version version = userAgent.getVersion(reqHeader);

        String browser = userAgent.getName() + "/" + version.getVersion();
        String ip = InetAddress.getLocalHost().getHostAddress();
        /*新增登录记录*/
        AoaUserLoginRecord aoaUserLoginRecord = new AoaUserLoginRecord();
        aoaUserLoginRecord.setIpAddr(ip).setLoginTime(new Date())
                .setBrowser(browser).setUserId(aoaUser.getUserId());
        aoaUserLoginRecordService.save(aoaUserLoginRecord);

        return "redirect:/index";
    }


    /**
     * 退出登录
     *
     * @param session
     * @return
     */
    @ApiOperation(value = "退出")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "uid", value = "", required = true, dataType = "String"),
//			@ApiImplicitParam(name = "isFormal", value = "", required = true, dataType = "String")
//	})
    @RequestMapping("loginout")
    public String loginout(HttpSession session) {
        session.removeAttribute("userId");
        return "redirect:/logins";
    }

    /**
     * 生成验证码
     *
     * @author lixiaolong
     * @date 2021/3/15 22:09
     */
    @RequestMapping("captcha")
    public void captcha(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException {
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");

        // 生成随机字串
        String verifyCode = VerifyCodeUtils.generateVerifyCode(4);

        // 生成图片
        int w = 135, h = 40;
        VerifyCodeUtils.outputImage(w, h, response.getOutputStream(), verifyCode);

        // 将验证码存储在session以便登录时校验
        session.setAttribute(Constants.CAPTCHA_KEY, verifyCode.toLowerCase());
    }
}
