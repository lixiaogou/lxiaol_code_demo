package cn.lxiaol.myoa.service;

import cn.lxiaol.myoa.entity.db1.AoaAttendsList;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaAttendsListService extends IService<AoaAttendsList> {

    AoaAttendsList findlastest(String nowdate, Long userId);

    Integer countrecord(String nowdate, Long userId);

    Long findoffworkid(String nowdate, Long userId);

    PageInfo<AoaAttendsList> singlepage(int pageNum, String baseKey, Long userid, String type, String status, String time);

    PageInfo<AoaAttendsList> paging(int pageNum, String baseKey, List<Long> ids, String type, String status, String time);

    Integer countoffwork(String month, Long userId);

    Integer counttowork(String month, Long userId);

    Integer countnum(String month, long statusId, Long userId);

    Integer countothernum(String month, long l, Long userId);
}
