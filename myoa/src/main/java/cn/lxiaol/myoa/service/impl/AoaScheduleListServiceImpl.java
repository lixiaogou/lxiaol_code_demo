package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaScheduleList;
import cn.lxiaol.myoa.entity.db1.AoaUser;
import cn.lxiaol.myoa.mapper.db1.AoaScheduleListMapper;
import cn.lxiaol.myoa.mapper.db1.AoaUserMapper;
import cn.lxiaol.myoa.service.AoaScheduleListService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaScheduleListServiceImpl extends ServiceImpl<AoaScheduleListMapper, AoaScheduleList> implements AoaScheduleListService {
    @Resource
    private AoaScheduleListMapper aoaScheduleListMapper;
    @Resource
    private AoaUserMapper aoaUserMapper;

    @Override
    public List<AoaScheduleList> aboutmeschedule(Long userId) {

        List<AoaScheduleList> myschedule = aoaScheduleListMapper.myschedule(userId);

        List<AoaScheduleList> aboutmerc = new ArrayList<>(myschedule);
        List<AoaScheduleList> otherschedule = aoaScheduleListMapper.otherschedule(userId);
        aboutmerc.addAll(otherschedule);

        for (AoaScheduleList scheduleList : aboutmerc) {
            AoaUser user1 = scheduleList.getUser();
            scheduleList.setUsername(user1.getRealName());
        }

        return aboutmerc;
    }

    @Override
    public List<AoaScheduleList> findstart(long userid) {
        LambdaQueryWrapper<AoaScheduleList> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaScheduleList::getUserId, userid);
        return this.list(qw);
    }
}
