package cn.lxiaol.myoa.service;

import cn.lxiaol.myoa.entity.db1.AoaStatusList;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaStatusListService extends IService<AoaStatusList> {

    List<AoaStatusList> findByStatusModel(String statusModel);

    String findname(Long statusId);

    String findcolor(Long statusId);
}
