package com.lxiaol.redisdemo.utils;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author lixiaolong
 * @date 2020/11/16 14:30
 * @description Jedispool的工具类  需要引入依赖，注意版本，不同版本具体方法会有些不同，不过根据方法签名还是可以看懂的
 * <dependency>
 * <groupId>redis.clients</groupId>
 * <artifactId>jedis</artifactId>
 * </dependency>
 * <p>
 * 注意：该类只用于测试，实际在springboot中 通过配置RedisTemplate来操作redis
 */
public class JedisPoolUtil {

    /**
     * @author lixiaolong
     * @date 2020/11/16 14:37
     * @description 获取jedispool  很多信息是可以放在配置文件中的。
     */
    public static JedisPool getJedisPoolInstance(int database) {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(1000);
        poolConfig.setMaxIdle(50);
        poolConfig.setMaxWaitMillis(100 * 1000);
        poolConfig.setTestOnBorrow(true);
        /**被volatile修饰的变量不会被本地线程缓存，对该变量的读写都是直接操作共享内存。*/
        return new JedisPool(poolConfig,
                "127.0.0.1", 6379, 2000, "123456", database);
    }
}
