package cn.lxiaol.service;

import cn.lxiaol.vo.DormVO;

import java.util.Map;



public interface DormService
{

   Map queryByPage(DormVO vo, int start, int pageSize);

   Map mod(DormVO dormVO);

   Map add(DormVO dormVO);

   Map del(String id);

   DormVO queryByAjaxWithId(String id);

   Map queryByAjax(DormVO dormVO);

}
