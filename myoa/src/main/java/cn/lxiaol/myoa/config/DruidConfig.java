package cn.lxiaol.myoa.config;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Druid连接池管控台配置
 *
 * @author zkm
 */
@Configuration
public class DruidConfig {

    /**
     * 这是配置druid的监控
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean<StatViewServlet> statViewServlet() {
        ServletRegistrationBean<StatViewServlet> bean =
                new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");
        Map<String, String> params = new HashMap<>();
        // 登录Druid的用户名
        params.put("loginUsername", "admin");
        // 登录Druid的密码
        params.put("loginPassword", "123456");
        // 默认允许所有
        params.put("allow", "");
        // params.put("deny", "192.168.***.***");//自己本机的ip地址
        bean.setInitParameters(params);
        return bean;
    }


    @Bean
    public FilterRegistrationBean<WebStatFilter> webStatFilter() {
        FilterRegistrationBean<WebStatFilter> bean = new FilterRegistrationBean<>();
        bean.setFilter(new WebStatFilter());
        Map<String, String> params = new HashMap<>();
        // 使静态文件访问，还有/druid/* 的访问不被拦截
        params.put("exclusions", "*.js,*.css,/druid/*");
        bean.setInitParameters(params);
        bean.setUrlPatterns(Collections.singletonList("/*"));
        return bean;
    }
}
