package com.example.jta.mapper.db1mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.jta.dao.Test;

/**
 * <p>
 * 系统用户 Mapper 接口
 * </p>
 *
 * @author lixiaolong
 * @since 2021-08-23
 */
public interface TestMapper extends BaseMapper<Test> {

}
