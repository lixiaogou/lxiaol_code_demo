package cn.lxiaol.myoa.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 配置填充策略
 *
 * @author lixiaolong
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * 插入时的填充策略
     *
     * @author lxiaol
     * @date 2021/4/15 21:42
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill.....");
//        setFieldValByName(String fieldName, Object fieldVal, MetaObject metaObject)

//        有些公司数据库喜欢使用这俩字段,而不是gmt字段
//        this.setFieldValByName("createTime",new Date(),metaObject);
//        this.setFieldValByName("updateTime",new Date(),metaObject);

        this.setFieldValByName("gmtCreate", new Date(), metaObject);
        this.setFieldValByName("gmtModified", new Date(), metaObject);
    }

    /**
     * 更新时的填充策略
     *
     * @author lxiaol
     * @date 2021/4/15 21:42
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill.....");
//        this.setFieldValByName("updateTime",new Date(),metaObject);
        this.setFieldValByName("gmtModified", new Date(), metaObject);
    }

}
