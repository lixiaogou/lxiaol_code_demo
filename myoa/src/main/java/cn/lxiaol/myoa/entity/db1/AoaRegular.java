package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_regular")
@ApiModel(value = "AoaRegular", description = "")
public class AoaRegular implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "regular_id", type = IdType.AUTO)

    private Long regularId;
    private String advice;
    private String deficiency;
    private String dobetter;
    private String experience;
    private String personnelAdvice;
    private String pullulate;
    private String understand;
    private Long proId;
    private String managerAdvice;
    private Integer days;


}

