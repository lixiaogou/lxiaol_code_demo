package cn.lxiaol.multipledb.service.impl;

import cn.lxiaol.multipledb.service.ITmsCompanyListService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.lxiaol.multipledb.entity.TmsCompanyList;
import cn.lxiaol.multipledb.mapper.db1.ITmsCompanyListMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 企业公司列表 服务实现类
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-09
 */
@Service
public class TmsCompanyListServiceImpl extends ServiceImpl<ITmsCompanyListMapper, TmsCompanyList> implements ITmsCompanyListService {

}
