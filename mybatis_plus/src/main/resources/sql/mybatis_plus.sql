/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : mybatis_plus

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 27/04/2020 20:54:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `deleted` int(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  `version` int(3) NULL DEFAULT 1 COMMENT '乐观锁',
  `gmt_create` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1254396570402791445 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'Jone', 18, 'test1@baomidou.com', 0, 1, '2020-04-26 21:27:19', '2020-04-26 21:27:22');
INSERT INTO `user` VALUES (2, 'Jack', 20, 'test2@baomidou.com', 0, 1, '2020-04-26 21:27:19', '2020-04-26 21:27:22');
INSERT INTO `user` VALUES (3, 'Tom', 28, 'test3@baomidou.com', 0, 1, '2020-04-26 21:27:19', '2020-04-26 21:27:23');
INSERT INTO `user` VALUES (4, 'Sandy', 21, 'test4@baomidou.com', 0, 1, '2020-04-26 21:27:19', '2020-04-26 21:27:25');
INSERT INTO `user` VALUES (5, 'Billie', 24, 'test5@baomidou.com', 0, 1, '2020-04-26 21:27:19', '2020-04-26 21:27:27');
INSERT INTO `user` VALUES (6, 'java成神之路', 18, '756078252@qq.com', 0, 1, '2020-04-26 21:27:19', '2020-04-26 21:27:29');
INSERT INTO `user` VALUES (1254396570402791426, '熊二', 10, '756078252@qq.com', 0, 1, '2020-04-26 21:27:19', '2020-04-26 21:27:30');
INSERT INTO `user` VALUES (1254396570402791427, '熊4', 20, '756078252@qq.com', 0, 1, '2020-04-26 21:27:19', '2020-04-26 21:27:31');
INSERT INTO `user` VALUES (1254396570402791428, '熊4', 20, '756078252@qq.com', 0, 1, '2020-04-26 21:27:19', '2020-04-26 21:27:32');
INSERT INTO `user` VALUES (1254396570402791429, '熊4', 20, '756078252@qq.com', 0, 1, '2020-04-26 21:30:27', '2020-04-26 21:30:31');
INSERT INTO `user` VALUES (1254396570402791430, '熊4', 20, '756078252@qq.com', 0, 1, '2020-04-26 21:30:55', '2020-04-26 21:30:55');
INSERT INTO `user` VALUES (1254396570402791431, '熊21', 20, '756078252@qq.com', 0, 1, '2020-04-26 21:31:41', '2020-04-26 21:31:41');
INSERT INTO `user` VALUES (1254396570402791432, '熊21', 20, '756078252@qq.com', 0, 1, '2020-04-26 21:33:36', '2020-04-26 21:33:36');
INSERT INTO `user` VALUES (1254396570402791433, 'java成神之路', 18, '756078252@qq.com', 0, 1, '2020-04-26 21:34:06', '2020-04-26 21:38:15');
INSERT INTO `user` VALUES (1254396570402791434, '熊21 ', 20, '756078252@qq.com', 0, 1, '2020-04-26 21:37:08', NULL);
INSERT INTO `user` VALUES (1254396570402791435, 'java成神之路', 18, '756078252@qq.com', 0, 1, '2020-04-26 21:39:14', '2020-04-26 21:40:02');
INSERT INTO `user` VALUES (1254396570402791436, '熊21 ', 20, '756078252@qq.com', 0, 1, NULL, NULL);
INSERT INTO `user` VALUES (1254396570402791437, '熊21 ', 20, '756078252@qq.com', 0, 1, NULL, NULL);
INSERT INTO `user` VALUES (1254396570402791438, '熊21 ', 20, '756078252@qq.com', 0, 1, NULL, NULL);
INSERT INTO `user` VALUES (1254396570402791439, '熊21 ', 20, '756078252@qq.com', 0, 1, NULL, NULL);
INSERT INTO `user` VALUES (1254396570402791440, '熊21 ', 20, '756078252@qq.com', 0, 1, '2020-04-26 22:18:49', '2020-04-26 22:18:49');
INSERT INTO `user` VALUES (1254396570402791441, '熊21 ', 20, '756078252@qq.com', 0, 1, NULL, NULL);
INSERT INTO `user` VALUES (1254396570402791442, '熊21 ', 20, '756078252@qq.com', 0, 1, NULL, NULL);
INSERT INTO `user` VALUES (1254396570402791443, 'java成神之路', 18, '756078252@qq.com', 0, 1, '2020-04-26 22:22:02', '2020-04-26 22:26:14');
INSERT INTO `user` VALUES (1254396570402791444, '熊21 ', 20, '756078252@qq.com', 0, 1, '2020-04-26 22:38:55', '2020-04-26 22:38:55');


SET FOREIGN_KEY_CHECKS = 1;
