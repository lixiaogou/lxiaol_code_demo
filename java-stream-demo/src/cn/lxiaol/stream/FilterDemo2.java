package cn.lxiaol.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 引用类型筛选
 *
 * @author lxiaol
 * @date 2021年05月18日 17:35
 */
public class FilterDemo2 {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<Person>();
        persons.add(new Person("Tom", 8900));
        persons.add(new Person("Jack", 7000));
        persons.add(new Person("Lily", 9000));

        List<Person> collect = persons.stream().filter(x -> x.getSalary() > 8000).collect(Collectors.toList());
        System.out.println(collect);

    }
}
