package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaMailReciver;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaMailReciverMapper extends BaseMapper<AoaMailReciver> {

    @Select("<script>" +
            "SELECT mailrecive0_.* FROM " +
            "	aoa_mail_reciver mailrecive0_ " +
            "	LEFT OUTER JOIN aoa_user user1_ ON mailrecive0_.mail_reciver_id = user1_.user_id  " +
            "WHERE " +
            "	mailrecive0_.is_read =0  " +
            "	AND mailrecive0_.is_del =0  " +
            "	AND user1_.user_id =#{userId} " +
            "</script>")
    List<AoaMailReciver> findByReadAndDelAndReciverId(Long userId);

}
