package cn.lxiaol.stream;


import java.util.ArrayList;
import java.util.List;

/**
 * 顾名思义，缩减操作，就是把一个流缩减成一个值，比如对一个集合求和、求乘积等。
 *
 * @author lxiaol
 * @date 2021年05月18日 17:35
 */
public class ReduceDemo3 {

    public static void main(String[] args) {
//        对象集合求和、求最值：
        List<Person> list = new ArrayList<>();
        list.add(new Person("Tom", 8000));
        list.add(new Person("Jack", 7000));
        list.add(new Person("Lily", 9000));

        Integer sum1 = list.stream().reduce(1, (x, p) -> x += p.getSalary(), Integer::sum);
        System.out.println("sum1 = " + sum1);

        Integer max1 = list.stream().reduce(100000, (max, p) -> max > p.getSalary() ? max : p.getSalary(), Integer::max);
        System.out.println("max1 = " + max1);


        mian();
    }

    public static void mian() {
        List<Person> personList = new ArrayList<Person>();
        personList.add(new Person("Tom", 8000));
        personList.add(new Person("Jack", 7000));
        personList.add(new Person("Lily", 9000));

        // 验证combiner-串行流
        Integer sumSalary = personList.stream().reduce(1, (sum, p) -> {
            System.out.format("accumulator: sum=%s; person=%s\n", sum, p.getName());
            return sum + p.getSalary();
        }, (sum1, sum2) -> {
            System.out.format("combiner: sum1=%s; sum2=%s\n", sum1, sum2);
            return sum1 + sum2;
        });
        System.out.println("串行流总和：" + sumSalary);
        // 输出结果：
        // accumulator: sum=0; person=Tom
        // accumulator: sum=8900; person=Jack
        // accumulator: sum=15900; person=Lily
        // 总和：24900

        // 验证combiner-并行流
        Integer sumSalary2 = personList.parallelStream().reduce(0, (sum, p) -> {
            System.out.format("accumulator: sum=%s; person=%s\n", sum, p.getName());
            return sum + p.getSalary();
        }, (sum1, sum2) -> {
            System.out.format("combiner: sum1=%s; sum2=%s\n", sum1, sum2);
            return sum1 + sum2;
        });
        System.out.println("并行流总和：" + sumSalary2);
        // 输出结果：
        // accumulator: sum=0; person=Jack
        // accumulator: sum=0; person=Tom
        // accumulator: sum=0; person=Lily
        // combiner: sum1=7000; sum2=9000
        // combiner: sum1=8900; sum2=16000
        // 总和：24900
    }
}
