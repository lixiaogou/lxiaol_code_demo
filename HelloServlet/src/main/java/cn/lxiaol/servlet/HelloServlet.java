package cn.lxiaol.servlet;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;

/**
 * @author lxiaol
 * @date 2021年05月06日 14:55
 */
public class HelloServlet implements Servlet {
    
    public HelloServlet(){
        System.out.println("HelloServlet()");
    }
    
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("init");

//        1、可以获取Servlet程序的别名servlet-name的值
        System.out.println("HelloServlet程序的别名是:" + servletConfig.getServletName());

//        2、获取初始化参数init-param
        System.out.println("初始化参数username的值是;" + servletConfig.getInitParameter("username"));
        System.out.println("初始化参数url的值是;" + servletConfig.getInitParameter("url"));

//        3、获取ServletContext对象
        System.out.println(servletConfig.getServletContext());

        Enumeration<String> initParameterNames = servletConfig.getInitParameterNames();
        while (initParameterNames.hasMoreElements()){
            System.out.println("initParameterNames = " + initParameterNames.nextElement());
        }

    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("3 service === Hello Servlet 被访问了");
        // 类型转换（因为它有getMethod()方法）
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        // 获取请求的方式
        String method = httpServletRequest.getMethod();

        if ("GET".equals(method)) {
            doGet();
        } else if ("POST".equals(method)) {
            doPost();
        }
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {
        System.out.println("destroy");
    }

    /**
     * 做get请求的操作
     */
    public void doGet(){
        System.out.println("get请求");
    }

    /**
     * 做post请求的操作
     */
    public void doPost(){
        System.out.println("post请求");
    }
}
