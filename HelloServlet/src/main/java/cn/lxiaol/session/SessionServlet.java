package cn.lxiaol.session;

import cn.lxiaol.base.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author lxiaol
 * @date 2021年05月08日 15:27
 */

public class SessionServlet extends BaseServlet {

    /**
     * 创建Session 和获取(id 号,是否为新)
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    public void createOrGetSession(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 创建和获取Session会话对象
        HttpSession session = req.getSession();

        // 判断 当前Session会话，是否是新创建出来的
        boolean isNew = session.isNew();

        // 获取Session会话的唯一标识 id
        String id = session.getId();

        resp.getWriter().write("得到的Session，它的id是：" + id + " <br /> ");
        resp.getWriter().write("这个Session是否是新创建的：" + isNew + " <br /> ");

    }

    /**
     * Session 域数据的保存
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    public void setAttribute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 创建和获取Session会话对象
        HttpSession session = req.getSession();
        session.setAttribute("key1", "value1");
        resp.getWriter().write("已经往Session中保存了数据");

    }


    /**
     * Session 域数据的获取
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    public void getAttribute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 创建和获取Session会话对象
        HttpSession session = req.getSession();
        Object sessionAttribute = session.getAttribute("key1");
        resp.getWriter().write("从Session中获取出key1的数据是：" + sessionAttribute);
    }

    /**
     * 默认生命周期 30分钟 因为在tomcat里面有设置
     * 因为在Tomcat 服务器的配置文件web.xml 中默认有以下的配置，它就表示配置了当前Tomcat 服务器下所有的Session
     * 超时配置默认时长为：30 分钟。
     * <session-config>
     * <session-timeout>30</session-timeout>
     * </session-config>
     * <p></p>
     * 可以在你自己的web.xml 配置文件中做
     * 以上相同的配置。就可以修改你的web 工程所有Seession 的默认超时时长。
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    public void defaultLife(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 获取了Session的默认超时时长
        int maxInactiveInterval = req.getSession().getMaxInactiveInterval();
        resp.getWriter().write("Session的默认超时时长为：" + maxInactiveInterval + " 秒 ");

    }

    /**
     * 设置5秒后Session销毁
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    public void life3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 先获取Session对象
        HttpSession session = req.getSession();
        // 设置当前Session5秒后超时
        session.setMaxInactiveInterval(5);

        resp.getWriter().write("当前Session已经设置为3秒后超时");
    }

    /**
     * 让Session会话马上超时
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    public void deleteNow(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 先获取Session对象
        HttpSession session = req.getSession();
        // 让Session会话马上超时
        session.invalidate();

        resp.getWriter().write("Session已经设置为超时（无效）");
    }


}
