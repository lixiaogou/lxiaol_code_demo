package cn.lxiaol.myoa.service;

import cn.lxiaol.myoa.entity.db1.AoaUserLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaUserLogService extends IService<AoaUserLog> {

    List<AoaUserLog> findByUserId(Long userId);

    AoaUserLog findByUserlaset(long l);
}
