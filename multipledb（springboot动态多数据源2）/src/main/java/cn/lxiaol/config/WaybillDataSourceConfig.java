package cn.lxiaol.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * Waybill 从数据源 配置
 *
 * @author lxiaol
 * @date 2021/4/15 20:43
 */
@Configuration
@MapperScan(basePackages = "cn.lxiaol.dao.waybill", sqlSessionTemplateRef = "waybillSqlSessionTemplate")
public class WaybillDataSourceConfig {

    @Bean(name = "waybillDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.waybill")
    public DataSource setDataSource() {
        return new DruidDataSource();
//        return DataSourceBuilder.create().build();
    }

    @Bean(name = "waybillTransactionManager")
    public DataSourceTransactionManager setTransactionManager(@Qualifier("waybillDataSource") DataSource waybillDataSource) {
        return new DataSourceTransactionManager(waybillDataSource);
    }

    @Bean(name = "waybillSqlSessionFactory")
    public SqlSessionFactory setSqlSessionFactory(@Qualifier("waybillDataSource") DataSource waybillDataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(waybillDataSource);
        //映射驼峰命名   即数据库的字段，如 aBc 中间要是存在"_" 时，则会自动转换匹配
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        configuration.setMapUnderscoreToCamelCase(true);
        bean.setConfiguration(configuration);
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mybatis/waybill/*.xml"));
        return bean.getObject();
    }

    @Bean(name = "waybillSqlSessionTemplate")
    public SqlSessionTemplate setSqlSessionTemplate(@Qualifier("waybillSqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

}
