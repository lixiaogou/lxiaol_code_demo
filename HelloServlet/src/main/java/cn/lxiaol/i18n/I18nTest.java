package cn.lxiaol.i18n;


import java.util.Locale;
import java.util.ResourceBundle;

public class I18nTest {

    public void testLocale() {
        // 获取你系统默认的语言。国家信息
        Locale locale = Locale.getDefault();
        System.out.println(locale);

        for (Locale availableLocale : Locale.getAvailableLocales()) {
            System.out.println(availableLocale);
        }

        // 获取中文，中文的常量的Locale对象
        System.out.println(Locale.CHINA);
        // 获取英文，美国的常量的Locale对象
        System.out.println(Locale.US);

    }

    public void testI18n() {
        // 得到我们需要的Locale对象
        Locale locale = Locale.US;
        // 通过指定的basename和Locale对象，读取 相应的配置文件
        ResourceBundle bundle = ResourceBundle.getBundle("i18n", locale);

        System.out.println("username：" + bundle.getString("username"));
        System.out.println("password：" + bundle.getString("password"));
        System.out.println("Sex：" + bundle.getString("sex"));
        System.out.println("age：" + bundle.getString("age"));
    }

    public static void main(String[] args) {
        I18nTest i18nTest = new I18nTest();
        i18nTest.testLocale();
        System.out.println("=================================");
        i18nTest.testI18n();
    }


}
