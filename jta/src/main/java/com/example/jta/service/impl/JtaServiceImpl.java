package com.example.jta.service.impl;

import com.example.jta.dao.Test;
import com.example.jta.dao.Test2;
import com.example.jta.service.JtaService;
import com.example.jta.service.Test2Service;
import com.example.jta.service.TestService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * <p>
 * 系统用户 服务实现类
 * </p>
 *
 * @author lixiaolong
 * @since 2021-08-23
 */
@Service
public class JtaServiceImpl implements JtaService {

    @Resource
    private TestService testService;
    @Resource
    private Test2Service testService2;



    @Override
    @Transactional
    public void add() {
        Test t1 = new Test().setUsername("master-"+ UUID.randomUUID());
        testService.save(t1);
        Test2 t2 = new Test2().setUsername("master-"+ UUID.randomUUID());
        testService2.save(t2);
        int  a = 1/0;

    }
}
