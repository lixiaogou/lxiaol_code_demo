package cn.lxiaol.myoa.service;

import cn.lxiaol.myoa.entity.db2.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface SysUserService extends IService<SysUser> {

}
