package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_evectionmoney")
@ApiModel(value = "AoaEvectionmoney", description = "")
public class AoaEvectionmoney implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "evectionmoney_id", type = IdType.AUTO)

    private Long evectionmoneyId;
    private String financialAdvice;
    private String managerAdvice;
    private Double money;
    private String name;
    private Long proId;
    private Integer pro;


}

