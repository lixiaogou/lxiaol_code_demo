package cn.lxiaol.multipledb.service.impl;

import cn.lxiaol.multipledb.service.ICommonWaybillNumService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.lxiaol.multipledb.entity.CommonWaybillNum;
import cn.lxiaol.multipledb.mapper.db1.ICommonWaybillNumMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 电子运单编号 服务实现类
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-09
 */
@Service
public class CommonWaybillNumServiceImpl extends ServiceImpl<ICommonWaybillNumMapper, CommonWaybillNum> implements ICommonWaybillNumService {

}
