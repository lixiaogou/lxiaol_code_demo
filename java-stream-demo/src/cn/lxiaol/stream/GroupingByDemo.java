package cn.lxiaol.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lxiaol
 * @date 2021年05月19日 14:22
 */
public class GroupingByDemo {

    public static void main(String[] args) {
        List<Person> personList = new ArrayList<Person>();
        personList.add(new Person("Tom", 8900));
        personList.add(new Person("Tom", 10000));
        personList.add(new Person("Tom", 10000));
        personList.add(new Person("Jack", 10000));
        personList.add(new Person("Lily", 9000));


        Map<String, List<Person>> group1 = personList.stream().collect(Collectors.groupingBy(Person::getName));
        System.out.println("group1 = " + group1);

        Map<String, Map<Integer, List<Person>>> group2 = personList.stream()
                .collect(Collectors.groupingBy(Person::getName, Collectors.groupingBy(Person::getSalary)));
        System.out.println("group2 = " + group2);

        Map<Integer, Map<String, List<Person>>> group3 = personList.stream()
                .collect(Collectors.groupingBy(Person::getSalary, Collectors.groupingBy(Person::getName)));
        System.out.println("group3 = " + group3);


    }

}
