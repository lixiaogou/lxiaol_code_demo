package cn.lxiaol.myoa.controller;

import cn.lxiaol.myoa.entity.db1.*;
import cn.lxiaol.myoa.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@Slf4j
@Api(value = "/", tags = {"首页相关接口"})
@Controller
@RequestMapping("/")
public class IndexController {

    @Resource
    private AoaProcessListService aoaProcessListService;
    @Resource
    private AoaNotepaperService aoaNotepaperService;
    @Resource
    private AoaPlanListService aoaPlanListService;
    @Resource
    private AoaAttendsListService aoaAttendsListService;
    @Resource
    private AoaTypeListService aoaTypeListService;
    @Resource
    private AoaStatusListService aoaStatusListService;
    @Resource
    private AoaDiscussListService aoaDiscussListService;
    @Resource
    private AoaDirectorService aoaDirectorService;
    @Resource
    private AoaFileListService aoaFileListService;
    @Resource
    private AoaUserLogService aoaUserLogService;
    @Resource
    private AoaTaskUserService aoaTaskUserService;
    @Resource
    private AoaMailReciverService aoaMailReciverService;
    @Resource
    private AoaNoticeUserRelationService aoaNoticeUserRelationService;
    @Resource
    private AoaNoticeListService aoaNoticeListService;
    @Resource
    private AoaUserService aoaUserService;
    @Resource
    private AoaSysMenuService aoaSysMenuService;
    @Autowired
    private AoaScheduleListService aoaScheduleListService;
    // 格式转化导入
    DefaultConversionService service = new DefaultConversionService();

    @ApiOperation(value = "跳转首页")
    @RequestMapping("index")
    public String index(HttpServletRequest req, Model model) {
        HttpSession session = req.getSession();
        Object sessionId = session.getAttribute("userId");

        // 如果是空则重回登陆页面
        if (Objects.isNull(sessionId) || Strings.isBlank(sessionId.toString())) {
            return "login/login";
        }

        Long userId = Long.parseLong(sessionId.toString());
        AoaUser aoaUser = aoaUserService.findOne(userId);

        //查找角色菜单
        aoaSysMenuService.findMenuSys(req, aoaUser);

        List<AoaScheduleList> aboutmenotice = aoaScheduleListService.aboutmeschedule(userId);
        for (AoaScheduleList dto : aboutmenotice) {
            Integer isreminded = dto.getIsreminded();
            if (Objects.nonNull(isreminded) && 0 != isreminded) {
                Date startTime = dto.getStartTime();
                System.out.println("startTime = " + startTime);

                long start = dto.getStartTime().getTime();
                long now = System.currentTimeMillis();
                long cha = start - now;
                //一小时的时间戳 3,600,000 一天的时间戳 86400000
                long dayMillis = 24L * 60 * 60 * 1000;
                if (0 < cha && cha < dayMillis) {
                    AoaNoticeList aoaNoticeList = new AoaNoticeList();

                    aoaNoticeList.setTypeId(11L);
                    aoaNoticeList.setStatusId(15L);
                    aoaNoticeList.setTitle("您有一个日程即将开始");
                    aoaNoticeList.setUrl("/daycalendar");
                    aoaNoticeList.setUserId(userId);
                    aoaNoticeList.setNoticeTime(new Date());
                    aoaNoticeListService.save(aoaNoticeList);

                    AoaNoticeUserRelation aoaNoticeUserRelation =
                            new AoaNoticeUserRelation(aoaNoticeList.getNoticeId(), userId, 0);
                    aoaNoticeUserRelationService.save(aoaNoticeUserRelation);

                    dto.setIsreminded(1);
                    aoaScheduleListService.updateById(dto);
                }
            }


        }

        //通知
        List<AoaNoticeUserRelation> notice = aoaNoticeUserRelationService.findByReadAndUserId(userId);

        //邮件
        List<AoaMailReciver> mail = aoaMailReciverService.findByReadAndDelAndReciverId(userId);

        //新任务
        List<AoaTaskUser> task = aoaTaskUserService.findByUserIdAndStatusId(userId);

        model.addAttribute("notice", notice.size());
        model.addAttribute("mail", mail.size());
        model.addAttribute("task", task.size());
        model.addAttribute("user", aoaUser);

        //展示用户操作记录 由于现在没有登陆 不能获取用户id
        List<AoaUserLog> userLogs = aoaUserLogService.findByUserId(userId);
        req.setAttribute("userLogList", userLogs);

        return "index/index";
    }

    /**
     * 控制面板主页
     *
     * @param model
     * @return
     * @parsession
     */
    @GetMapping("test2")
    public String test2(HttpSession session, Model model, HttpServletRequest request) {
        Long userId = Long.parseLong(session.getAttribute("userId") + "");
        AoaUser aoaUser = aoaUserService.findOne(userId);
//        request.setAttribute("user", aoaUser);

        //计算三个模块的记录条数
        request.setAttribute("filenum", aoaFileListService.count(null));
        request.setAttribute("directornum", aoaDirectorService.count(null));
        request.setAttribute("discussnum", aoaDiscussListService.count(null));

        List<Map<String, Object>> list = aoaNoticeListService.findMyNoticeLimit(userId);
        model.addAttribute("user", aoaUser);

        for (Map<String, Object> map : list) {
            AoaStatusList aoaStatusList = aoaStatusListService.getById((Long) map.get("status_id"));
            AoaTypeList aoaTypeList = aoaTypeListService.getById((Long) map.get("type_id"));
            AoaUser aoaUser1 = aoaUserService.findOne((Long) map.get("user_id"));


            map.put("status", aoaStatusList.getStatusName());
            map.put("type", aoaTypeList.getTypeName());
            map.put("statusColor", aoaStatusList.getStatusColor());
            map.put("userName", aoaUser1.getUserName());
            map.put("deptName", aoaUser1.getDept().getDeptName());
        }
        // List<Map<String, Object>>
        // noticeList=informRService.setList(noticeList1);

        // 显示用户当天最新的记录
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String nowdate = sdf.format(new Date());
        AoaAttendsList aList = aoaAttendsListService.findlastest(nowdate, userId);
        if (Objects.nonNull(aList)) {
            String type = aoaTypeListService.findname(aList.getTypeId());
            model.addAttribute("type", type);
        }
        model.addAttribute("alist", aList);


//        showalist(model, userId);
        System.out.println("通知" + list);
        model.addAttribute("noticeList", list);


        //列举计划
        List<AoaPlanList> plans = aoaPlanListService.findByUserlimit(userId);
        model.addAttribute("planList", plans);
        List<AoaTypeList> ptype = aoaTypeListService.findByTypeModel("aoa_plan_list");
        List<AoaStatusList> pstatus = aoaStatusListService.findByStatusModel("aoa_plan_list");
        model.addAttribute("ptypelist", ptype);
        model.addAttribute("pstatuslist", pstatus);

        //列举便签
        List<AoaNotepaper> notepapers = aoaNotepaperService.findByUserIdOrderByCreateTimeDesc(userId);
        model.addAttribute("notepaperList", notepapers);

        //列举几个流程记录
        List<AoaProcessList> pList = aoaProcessListService.findlastthree(userId);
        model.addAttribute("processlist", pList);
        List<AoaStatusList> processstatus = aoaStatusListService.findByStatusModel("aoa_process_list");
        model.addAttribute("prostatuslist", processstatus);
        return "systemcontrol/control";
    }

    private void showalist(Model model, Long userId) {
        // 显示用户当天最新的记录
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String nowdate = sdf.format(new Date());
        AoaAttendsList aList = aoaAttendsListService.findlastest(nowdate, userId);
        if (Objects.nonNull(aList)) {
            String type = aoaTypeListService.findname(aList.getTypeId());
            model.addAttribute("type", type);
        }
        model.addAttribute("alist", aList);
    }


}
