package cn.lxiaol.stream;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author lxiaol
 * @date 2021年05月19日 15:26
 */
public class IterateDemo {

    public static void main(String[] args) {
        String[] arr1 = {"a", "b", "c", "d"};
        String[] arr2 = {"d", "e", "f", "g"};
        String[] arr3 = {"i", "j", "k", "l"};

        /**
         * 可以把两个stream合并成一个stream（合并的stream类型必须相同）,只能两两合并
         * 预期结果：a b c d e（为节省篇幅，空格代替换行）
         */

        Stream<String> arr11 = Stream.of(arr1);
        Stream<String> arr21 = Stream.of(arr2);
        List<String> collect = Stream.concat(arr11, arr21).collect(Collectors.toList());
        System.out.println("collect = " + collect);

        Stream<String> stream1 = Stream.of(arr1);
        Stream<String> stream2 = Stream.of(arr2);
        List<String> collect1 = Stream.concat(stream1, stream2).distinct().collect(Collectors.toList());
        System.out.println("collect1 = " + collect1);



        /*
         * limit，限制从流中获得前n个数据
         * 预期结果：1 3 5 7 9 11 13 15 17 19
         */
        List<Integer> collect2 = Stream.iterate(1, x -> x + 2).limit(10).collect(Collectors.toList());
        System.out.println("collect2 = " + collect2);

        /*
         * skip，跳过前n个数据
         * 预期结果：5 7 9 11 13
         */
        List<Integer> collect3 = Stream.iterate(1, x -> x + 2).skip(2).limit(5).collect(Collectors.toList());
        System.out.println("collect3 = " + collect3);

    }


}
