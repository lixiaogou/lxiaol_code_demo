package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_mailnumber")
@ApiModel(value = "AoaMailnumber", description = "")
public class AoaMailnumber implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "mail_number_id", type = IdType.AUTO)

    private Long mailNumberId;
    private String mailAccount;
    private Date mailCreateTime;
    private String mailDes;
    private Long mailType;
    private String mailUserName;
    private String password;
    private Long status;
    private Long mailNumUserId;


}

