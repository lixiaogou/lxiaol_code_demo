package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaSysMenu;
import cn.lxiaol.myoa.entity.db1.AoaUser;
import cn.lxiaol.myoa.entity.dto.RoleMenuDTO;
import cn.lxiaol.myoa.mapper.db1.AoaRolePowerListMapper;
import cn.lxiaol.myoa.mapper.db1.AoaSysMenuMapper;
import cn.lxiaol.myoa.service.AoaSysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaSysMenuServiceImpl extends ServiceImpl<AoaSysMenuMapper, AoaSysMenu> implements AoaSysMenuService {

    @Resource
    private AoaRolePowerListMapper aoaRolePowerListMapper;


    @Override
    public void findMenuSys(HttpServletRequest req, AoaUser aoaUser) {
        Long roleId = aoaUser.getRole().getRoleId();
        List<RoleMenuDTO> oneMenuAll = aoaRolePowerListMapper.findbyparentxianall(0L, roleId, 1, 1);
        List<RoleMenuDTO> twoMenuAll = aoaRolePowerListMapper.findbyparentsxian(0L, roleId, 1, 1);
        req.setAttribute("oneMenuAll", oneMenuAll);
        req.setAttribute("twoMenuAll", twoMenuAll);
    }
}