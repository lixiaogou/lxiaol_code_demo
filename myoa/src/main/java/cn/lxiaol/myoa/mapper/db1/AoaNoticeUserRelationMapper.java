package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaNoticeUserRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaNoticeUserRelationMapper extends BaseMapper<AoaNoticeUserRelation> {

    @Select("<script>" +
            "SELECT noticeuser0_.* FROM " +
            "	aoa_notice_user_relation noticeuser0_ " +
            "	LEFT OUTER JOIN aoa_user user1_ ON noticeuser0_.relatin_user_id = user1_.user_id  " +
            "WHERE " +
            "	noticeuser0_.is_read = 0 " +
            "	AND user1_.user_id =#{userId} " +
            "</script>")
    List<AoaNoticeUserRelation> findByReadAndUserId(Long userId);


    @Select("<script>" +
            "SELECT noticeuser0_.* FROM " +
            "	aoa_notice_user_relation noticeuser0_ " +
            "	LEFT OUTER JOIN aoa_notice_list noticeslis1_ ON noticeuser0_.relatin_notice_id = noticeslis1_.notice_id  " +
            "WHERE " +
            "	noticeslis1_.notice_id = #{noticeId} " +
            "</script>")
    List<AoaNoticeUserRelation> findByNoticeId(Long noticeId);


}
