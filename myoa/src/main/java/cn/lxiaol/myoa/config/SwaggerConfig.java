package cn.lxiaol.myoa.config;

import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * @author lixiaolong
 */
@Component
@EnableSwagger2
public class SwaggerConfig {

    //    配置Swagger 的 Docket bean实例
//    environment 获取项目环境
    @Bean
    public Docket docket(Environment environment) {

//        设置要显示的swagger环境
        Profiles profiles = Profiles.of("dev", "test");
//        environment.acceptsProfiles判断 当前项目所处的环境
        boolean flag = environment.acceptsProfiles(profiles);


//        此时还没有配置自定义扫描包路径 会扫描全局的
//        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo());
//        配置自定义扫描包路径
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
//                配置API文档的分组
                .groupName("分组名")
//                设置打开swagger2 可根据项目环境来判断啊
                .enable(flag)
                .select()
//                RequestHandlerSelectors 配置扫描接口的方式
//                basePackage 指定要扫描的包
//                any 全扫描
//                none 不扫描
//                withClassAnnotation 通过类注解 参数是一个类注解的反射对象 例如 RestController.class 只扫描类上有这个注解的接口
//                withMethodAnnotation 通过方法注解 类似类注解
                .apis(RequestHandlerSelectors.basePackage("cn.lxiaol.myoa.controller"))
//              配置只扫描哪些请求路径url  只扫描/api/ 开头的请求路径
                .paths(PathSelectors.ant("/**/**"))
                .build();

    }

    //    配置Swagger文档信息 感觉这些配置信息可以从项目的配置文件中读取
    private ApiInfo apiInfo() {
//        作者信息
        Contact contact = new Contact("作者名", "https://gitee.com/lixiaogou", "756078252@qq.com");
        return new ApiInfo("SwaggerAPI配置文档"
                , "我是文档描述"
                , "v1.0" // 版本号
                , "https://user.qzone.qq.com" //url
                , contact //作者信息
                , "Apache 2.0" //开源版本号
                , "http://www.apache.org/licenses/LICENSE-2.0" //开源版本号url
                , new ArrayList());
    }

}
