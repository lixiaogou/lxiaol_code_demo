package cn.lxiaol.myoa.aop;

import cn.lxiaol.myoa.config.DataSourceContextHolder;
import cn.lxiaol.myoa.enums.DBTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = -100)
@Slf4j
@Aspect
public class DataSourceAspect {

    @Pointcut("execution(* cn.lxiaol.myoa.mapper.db1.*.*(..))")
    private void db1Aspect() {
    }

    @Pointcut("execution(* cn.lxiaol.myoa.mapper.db2.*.*(..))")
    private void db2Aspect() {
    }


    @Before("db1Aspect()")
    public void db1() {
//        log.info("切换到db1数据源...");
        DataSourceContextHolder.setDbType(DBTypeEnum.db1);
    }


    @After("db1Aspect()")
    public void clearDb1() {
//        log.info("清除掉db1数据源...");
        DataSourceContextHolder.clearDbType();
    }

    @Before("db2Aspect()")
    public void db2() {
//        log.info("切换到db2数据源...");
        DataSourceContextHolder.setDbType(DBTypeEnum.db2);
    }

    @After("db2Aspect()")
    public void clearDb2() {
//        log.info("清除掉db2数据源...");
        DataSourceContextHolder.clearDbType();
    }

}
