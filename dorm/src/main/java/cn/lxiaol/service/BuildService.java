package cn.lxiaol.service;

import cn.lxiaol.vo.BuildVO;

import java.util.Map;


public interface BuildService
{

   Map queryByPage(BuildVO vo, int start, int pageSize);

   Map mod(BuildVO buildVO);

   Map add(BuildVO buildVO);

   Map del(String id);

   BuildVO queryByAjaxWithId(String id);

   Map queryByAjax(BuildVO vo, String user_id);

}
