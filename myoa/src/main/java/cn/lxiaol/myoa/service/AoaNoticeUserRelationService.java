package cn.lxiaol.myoa.service;

import cn.lxiaol.myoa.entity.db1.AoaNoticeUserRelation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaNoticeUserRelationService extends IService<AoaNoticeUserRelation> {

    List<AoaNoticeUserRelation> findByReadAndUserId(Long userId);

    List<AoaNoticeUserRelation> findByNoticeId(Long noticeId);
}
