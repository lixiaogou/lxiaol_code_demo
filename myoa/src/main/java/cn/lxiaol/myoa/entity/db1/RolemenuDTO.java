package cn.lxiaol.myoa.entity.db1;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "RolemenuDTO", description = "角色和菜单的关系")
public class RolemenuDTO {

    private Long menuId; // 菜单id

    private String menuName; // 菜单名字

    private String menuUrl; // 菜单链接

    private Integer show = 0; // 菜单是否显示

    private Integer check = 0; // 是否分配资源

    private String menuIcon; // 菜单图标

    private Integer sortId; // 菜单排序id

    private Integer menuGrade; // 权限值分数

    private Long parentId;

}
