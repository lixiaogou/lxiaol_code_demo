package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_resign")
@ApiModel(value = "AoaResign", description = "")
public class AoaResign implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "resign_id", type = IdType.AUTO)

    private Long resignId;
    private String financialAdvice;
    private Boolean isFinish;
    private String nofinish;
    private String personnelAdvice;
    private String suggest;
    private Long handUser;
    private Long proId;
    private String managerAdvice;


}

