package cn.lxiaol.excel_demo.controller;

import cn.lxiaol.excel_demo.common.listeners.CommonExcelListener;
import cn.lxiaol.excel_demo.dto.UserExcelDto;
import cn.lxiaol.excel_demo.dto.UserExcelParamDto;
import cn.lxiaol.excel_demo.service.ExcelService;
import com.alibaba.excel.EasyExcel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author lxiaol
 * @date 2021年08月15日 14:07
 */
@RestController
@RequestMapping("/api/excel")
@Slf4j
public class ExcelController {
    @Resource
    private ExcelService excelService;

    @PostMapping(value = "/dataImport", headers = "content-type=multipart/form-data")
    public String dataImport(@RequestParam("file") MultipartFile file) {
        String result = "success";
        try {
            // 实例化对象并传入泛型类型
            CommonExcelListener<UserExcelDto> listener = new CommonExcelListener<>();
            // 调用easyexcel的方法，传入文件流，目标类型，和read监听器,
            // 设置表头所在行，自动去除空字符，设置读取第几个sheet页，并开始读取
            EasyExcel.read(file.getInputStream(), UserExcelDto.class, listener)
                    .headRowNumber(1).autoTrim(true).sheet(0).doRead();
            //读取结束，得到读取到的数据
            List<UserExcelDto> list = listener.getList();
            if (!list.isEmpty()) {
                //.....具体业务逻辑
                System.out.println("读取到数据，进行具体的后续操作");
            } else {
                result = "excel内容不能为空";
            }

        } catch (Exception e) {
            log.error("xxxx导入 报错：", e);
            result = "excel导入报错，请检查数据是否合规";
        }
        return result;
    }

    @PostMapping("/dataExport")
    public String dataExport(@RequestBody UserExcelParamDto dto, HttpServletRequest request, HttpServletResponse response) {
        String result = "success";
        try {
            // .....调用具体的业务方法
            excelService.export(dto, request, response);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("安装上线导出 接口报错：", e);
            result = "excel导出报错，请检查数据是否合规";
        }
        return result;
    }


}
