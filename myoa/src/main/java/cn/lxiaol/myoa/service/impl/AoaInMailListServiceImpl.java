package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaInMailList;
import cn.lxiaol.myoa.mapper.db1.AoaInMailListMapper;
import cn.lxiaol.myoa.service.AoaInMailListService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaInMailListServiceImpl extends ServiceImpl<AoaInMailListMapper, AoaInMailList> implements AoaInMailListService {

}
