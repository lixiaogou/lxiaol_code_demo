package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaUserMapper extends BaseMapper<AoaUser> {

    @Select("<script>" +
            "SELECT * FROM aoa_user WHERE user_id = #{userId}" +
            "</script>")
    @Results({
            @Result(column = "user_id", property = "userId"),
            @Result(column = "dept_id", property = "deptId"),
            @Result(column = "position_id", property = "positionId"),
            @Result(column = "role_id", property = "roleId"),
            @Result(column = "user_id", property = "aSet", many = @Many(select = "cn.lxiaol.myoa.mapper.db1.AoaAttendsListMapper.findSetByUserId")),
            @Result(column = "dept_id", property = "dept", one = @One(select = "cn.lxiaol.myoa.mapper.db1.AoaDeptMapper.findOneByDeptId")),
            @Result(column = "position_id", property = "position", one = @One(select = "cn.lxiaol.myoa.mapper.db1.AoaPositionMapper.findOneByPositionId")),
            @Result(column = "role_id", property = "role", one = @One(select = "cn.lxiaol.myoa.mapper.db1.AoaRoleMapper.findOneByRoleId")),
    })
    AoaUser findOne(Long userId);


    @Select("<script>" +
            "SELECT * FROM aoa_user " +
            "</script>")
    @Results({
            @Result(column = "user_id", property = "userId"),
            @Result(column = "dept_id", property = "deptId"),
            @Result(column = "position_id", property = "positionId"),
            @Result(column = "role_id", property = "roleId"),
            @Result(column = "user_id", property = "aSet", many = @Many(select = "cn.lxiaol.myoa.mapper.db1.AoaAttendsListMapper.findSetByUserId")),
            @Result(column = "dept_id", property = "dept", one = @One(select = "cn.lxiaol.myoa.mapper.db1.AoaDeptMapper.findOneByDeptId")),
            @Result(column = "position_id", property = "position", one = @One(select = "cn.lxiaol.myoa.mapper.db1.AoaPositionMapper.findOneByPositionId")),
            @Result(column = "role_id", property = "role", one = @One(select = "cn.lxiaol.myoa.mapper.db1.AoaRoleMapper.findOneByRoleId")),
    })
    List<AoaUser> findAll();
}
