package cn.lxiaol.multipledb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.lxiaol.multipledb.entity.TmsCompanyList;

/**
 * <p>
 * 企业公司列表 服务类
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-09
 */
public interface ITmsCompanyListService extends IService<TmsCompanyList> {

}
