package cn.lxiaol.myoa.mapper.db2;

import cn.lxiaol.myoa.entity.db2.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
