package cn.lxiaol.dao;

import java.util.List;

public interface MenuDao {

    List getMenu(String type, String level);

}
