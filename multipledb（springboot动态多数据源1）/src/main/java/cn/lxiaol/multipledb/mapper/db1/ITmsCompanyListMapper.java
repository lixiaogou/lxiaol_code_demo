package cn.lxiaol.multipledb.mapper.db1;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.lxiaol.multipledb.entity.TmsCompanyList;

/**
 * <p>
 * 企业公司列表 Mapper 接口
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-09
 */
public interface ITmsCompanyListMapper extends BaseMapper<TmsCompanyList> {

}
