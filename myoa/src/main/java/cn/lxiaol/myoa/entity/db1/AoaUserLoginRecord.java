package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_user_login_record")
@ApiModel(value = "AoaUserLoginRecord", description = "登陆记录")
public class AoaUserLoginRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "record_id", type = IdType.AUTO)

    private Long recordId;
    private String browser;
    private String ipAddr;
    private Date loginTime;
    private String sessionId;
    private Long userId;


}

