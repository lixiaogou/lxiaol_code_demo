package cn.lxiaol.util;

import org.apache.commons.lang.StringUtils;

import java.util.Objects;

public class StringHelper {
    public static String convertStringNull(String strOrig) {
        if (Objects.isNull(strOrig)) {
            return StringUtils.EMPTY;
        } else {
            return strOrig.trim();
        }
    }

}
