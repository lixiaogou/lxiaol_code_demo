package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_notice_list")
@ApiModel(value = "AoaNoticeList", description = "")
public class AoaNoticeList implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "notice_id", type = IdType.AUTO)

    private Long noticeId;
    private String content;
    private Integer isTop;
    private Date modifyTime;
    private Date noticeTime;
    private Long statusId;
    private String title;
    private Long typeId;
    private String url;
    private Long userId;


}

