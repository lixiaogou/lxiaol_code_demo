package cn.lxiaol.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author lxiaol
 * @date 2021年05月19日 14:15
 */
public class CollectorsReduceDemo {

    public static void main(String[] args) {
        List<Person> list = new ArrayList<Person>();
        list.add(new Person("Tom", 8900));
        list.add(new Person("Jack", 7000));
        list.add(new Person("Lily", 9000));

        Integer sumSalary = list.stream().collect(Collectors.reducing(0, Person::getSalary, (i, j) -> i + j));
        System.out.println(sumSalary);

        Optional<Integer> sumSalary2 = list.stream().map(Person::getSalary).reduce(Integer::sum);
        System.out.println(sumSalary2);


    }
}
