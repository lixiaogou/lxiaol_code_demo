package cn.lxiaol.servlet;

import cn.lxiaol.vo.UserVO;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RightFilter extends HttpFilter {


    @Override
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain filterChain) throws IOException, ServletException {
        HttpSession session = req.getSession();
        UserVO userVO = (UserVO) session.getAttribute("CurrentUser");
        if (Objects.nonNull(userVO)) {//通过
            filterChain.doFilter(req, resp);
        } else {//跳转到登录页面
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        }
    }


}  