package cn.lxiaol.duoshujuyuan.model.slave;

import lombok.Data;

import java.io.Serializable;

@Data
public class SlaveUser implements Serializable {
    private String username;
}