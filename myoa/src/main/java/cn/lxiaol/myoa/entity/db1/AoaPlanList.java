package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_plan_list")
@ApiModel(value = "AoaPlanList", description = "")
public class AoaPlanList implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "plan_id", type = IdType.AUTO)

    private Long planId;
    private Date createTime;
    private Date endTime;
    private String label;
    private String planComment;
    private String planContent;
    private String planSummary;
    private Date startTime;
    private Long statusId;
    private String title;
    private Long typeId;
    private Long planUserId;
    private Long attachId;


}

