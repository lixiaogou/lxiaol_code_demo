package cn.lxiaol.idempotence.exception;


/**
 * Copyright (C), 2019-2020
 * FileName: IdempotentException
 * Author:   SixJR.
 * Date:     2020年11月16日 16:07
 * Description: 幂等自定义异常处理代码
 * History:
 * <author>          <time>          <version>          <desc>
 */

public class IdempotentException extends RuntimeException {
    public IdempotentException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}