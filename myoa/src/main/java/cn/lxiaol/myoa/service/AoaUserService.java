package cn.lxiaol.myoa.service;

import cn.lxiaol.myoa.entity.db1.AoaUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 用户服务类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaUserService extends IService<AoaUser> {

    AoaUser findOne(Long userId);

    List<AoaUser> findAll();

    List<AoaUser> findByFatherId(Long userId);

    PageInfo<AoaUser> findmyemployuser(int pageNum, String baseKey, Long userId);

}
