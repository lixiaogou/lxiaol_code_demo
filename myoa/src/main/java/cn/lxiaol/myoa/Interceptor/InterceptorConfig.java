package cn.lxiaol.myoa.Interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//配置拦截器
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    /**
     * 不拦截的路由
     */
    private final List<String> ALLOWED_PATHS = Collections.unmodifiableList(
            Arrays.asList("/bootstrap/**", "/css/**", "/easyui/**", "/images/**", "/js/**", "/plugins/**"));

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        addInterceptor：需要一个实现HandlerInterceptor接口的拦截器实例
//        addPathPatterns：用于设置拦截器的过滤路径规则
//        excludePathPatterns：用于设置不需要拦截的过滤规则

        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns(ALLOWED_PATHS)
                .excludePathPatterns("/logins")
                .excludePathPatterns("/captcha");

    }

    /* springboot 配置拦截器后出现静态资源无法访问：No mapping for GET的解决办法
    出现该问题是因为springboot 2.0后静态资源也经过拦截器导致的。
    由于WebMvcConfigurerAdapter 的废弃，现今配置拦截器可使用
    继承 WebMvcConfigurationSupport 类和实现 WebMvcConfigurer接口 两种方法。
    建议采用后一种方法（实现接口），否则会出现如题所示的方法。
    如果坚持使用继承WebMvcConfigurationSupport类 的方法，则必须要覆盖重写 WebMvcConfigurationSupport 类 的
    protected void addResourceHandlers(ResourceHandlerRegistry registry);
    然后上方的拦截器也要放开指定路径

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/")
                .addResourceLocations("classpath:/templates/");
        super.addResourceHandlers(registry);
    }
    */

}