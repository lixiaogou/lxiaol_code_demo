package cn.lxiaol.domain;

import lombok.Data;

@Data
public class User {

    //用户id
    private String id;
    //昵称  默认如下
    private String nickName;
    //真实姓名
    private String realName;
    //性别默认0未知，1：男性；2：女性
    private Integer sex;
}
