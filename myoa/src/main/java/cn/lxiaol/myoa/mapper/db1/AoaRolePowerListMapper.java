package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaRolePowerList;
import cn.lxiaol.myoa.entity.dto.RoleMenuDTO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Repository
public interface AoaRolePowerListMapper extends BaseMapper<AoaRolePowerList> {

    //    @Select("<script>" +
//            " SELECT a.* " +
//            " FROM tms_account_receivable a " +
//            "<where>  " +
//            "<if test=\"companyId != null and companyId != ''\"> and a.company_id = #{companyId} </if>" +
//            "<if test=\"accountNum != null and accountNum != ''\"> and a.account_num = #{accountNum} </if>" +
//            //结转单号暂时不需要
//            "</where>" +
//            "</script>")
    @Select("<script>" +
            "SELECT " +
            "	systemmenu1_.menu_id AS menuId, " +
            "	systemmenu1_.menu_name AS menuName, " +
            "	systemmenu1_.menu_url AS menuUrl, " +
            "	systemmenu1_.is_show AS `show`, " +
            "	rolepowerl0_.is_show AS `check`, " +
            "	systemmenu1_.parent_id AS parentId, " +
            "	systemmenu1_.menu_icon AS menuIcon, " +
            "	systemmenu1_.sort_id AS sortId, " +
            "	systemmenu1_.menu_grade AS menuGrade  " +
            "FROM " +
            "	aoa_role_power_list rolepowerl0_ " +
            "	CROSS JOIN aoa_sys_menu systemmenu1_  " +
            "WHERE " +
            "	rolepowerl0_.menu_id = systemmenu1_.menu_id  " +
            "	AND systemmenu1_.parent_id = #{l}  " +
            "	AND rolepowerl0_.role_id =#{roleId} " +
            "	AND systemmenu1_.is_show =#{i} " +
            "	AND rolepowerl0_.is_show =#{i1} " +
            "ORDER BY " +
            "	systemmenu1_.sort_id" +
            "</script>")
    List<RoleMenuDTO> findbyparentxianall(@Param("l") long l,
                                          @Param("roleId") Long roleId,
                                          @Param("i") int i,
                                          @Param("i1") int i1);

    @Select("<script>" +
            "SELECT " +
            "	systemmenu1_.menu_id AS menuId, " +
            "	systemmenu1_.menu_name AS menuName, " +
            "	systemmenu1_.menu_url AS menuUrl, " +
            "	systemmenu1_.is_show AS `show`, " +
            "	rolepowerl0_.is_show AS `check`, " +
            "	systemmenu1_.parent_id AS parentId, " +
            "	systemmenu1_.menu_icon AS menuIcon, " +
            "	systemmenu1_.sort_id AS sortId, " +
            "	systemmenu1_.menu_grade AS menuGrade  " +
            "FROM " +
            "	aoa_role_power_list rolepowerl0_ " +
            "	CROSS JOIN aoa_sys_menu systemmenu1_  " +
            "WHERE " +
            "	rolepowerl0_.menu_id = systemmenu1_.menu_id  " +
            "	AND systemmenu1_.parent_id != #{l}  " +
            "	AND rolepowerl0_.role_id =#{roleId} " +
            "	AND systemmenu1_.is_show =#{i} " +
            "	AND rolepowerl0_.is_show =#{i1} " +
            "ORDER BY " +
            "	systemmenu1_.sort_id" +
            "</script>")
    List<RoleMenuDTO> findbyparentsxian(@Param("l") long l,
                                        @Param("roleId") Long roleId,
                                        @Param("i") int i,
                                        @Param("i1") int i1);
}
