package cn.lxiaol.duoshujuyuan.service.impl;

import cn.lxiaol.duoshujuyuan.anno.MultiTransactional;
import cn.lxiaol.duoshujuyuan.mapper.master.MasterUserMapper;
import cn.lxiaol.duoshujuyuan.mapper.slave.SlaveUserMapper;
import cn.lxiaol.duoshujuyuan.model.master.MasterUser;
import cn.lxiaol.duoshujuyuan.model.slave.SlaveUser;
import cn.lxiaol.duoshujuyuan.service.TestService;
import cn.lxiaol.duoshujuyuan.transaction.DbTxConstants;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.UUID;

@Service
public class TestServiceImpl implements TestService {
    @Resource
    private MasterUserMapper masterUserMapper;
    @Resource
    private SlaveUserMapper slaveUserMapper;


    @Override
    @MultiTransactional(value = {DbTxConstants.DB1_TX, DbTxConstants.DB2_TX})
    public void createUserWithAnnotation() {
        MasterUser masterUser = new MasterUser();

        /*主数据库插入*/
        masterUser.setUsername("master"+ UUID.randomUUID());
        masterUserMapper.insertSelective(masterUser);

        /*从数据库插入*/
        SlaveUser slaveUser = new SlaveUser();
        slaveUser.setUsername("slave"+ UUID.randomUUID());
        slaveUserMapper.insertSelective(slaveUser);
        int a = 1/0;
    }
}