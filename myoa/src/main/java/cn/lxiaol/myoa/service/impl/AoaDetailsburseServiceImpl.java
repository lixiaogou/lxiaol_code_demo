package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaDetailsburse;
import cn.lxiaol.myoa.mapper.db1.AoaDetailsburseMapper;
import cn.lxiaol.myoa.service.AoaDetailsburseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaDetailsburseServiceImpl extends ServiceImpl<AoaDetailsburseMapper, AoaDetailsburse> implements AoaDetailsburseService {

}
