package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_discuss_list")
@ApiModel(value = "AoaDiscussList", description = "")
public class AoaDiscussList implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "discuss_id", type = IdType.AUTO)

    private Long discussId;
    private Integer attachmentId;
    private String content;
    private Date createTime;
    private Long statusId;
    private String title;
    private Long typeId;
    private Integer visitNum;
    private Long discussUserId;
    private Long voteId;
    private Date modifyTime;


}

