package com.atguigu.spring5;

import java.lang.reflect.Proxy;

/**
 * 3 使用Proxy类 创建接口代理对象
 */
public class JDKProxy {
    public static void main(String[] args) {

        /*
        参数一 类加载器
        参数二 增强方法的所在的接口
        参数三 实现InvocationHandler类创建代理对象，编写增强方法
         */
        Class[] interfaces = {UserDao.class};//增强方法所在的接口
        // 方式一 使用匿名内部类来做
//        Proxy.newProxyInstance(JDKProxy.class.getClassLoader(), interfaces, new InvocationHandler() {
//            @Override
//            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//                return null;
//            }
//        });

        // 方式二 不通过匿名内部类 而是通过 接口实现类
        UserDao userDao = new UserDaoImpl();
        UserDao dao = (UserDao)Proxy.newProxyInstance(JDKProxy.class.getClassLoader(), interfaces, new UserDaoProxy(userDao));
        dao.add(1, 3);
        dao.update("36");
    }
}





















