package cn.lxiaol.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lxiaol
 * @date 2021年05月19日 14:52
 */
public class SortDemo {

    public static void main(String[] args) {
        String[] strArr = {"abc", "m", "M", "bcd"};

        List<String> sort1 = Arrays.stream(strArr).sorted().collect(Collectors.toList());
        System.out.println("sort1 = " + sort1);


        int[] intArr = {4, 3, 8, 1, 7};
        Arrays.stream(intArr).sorted().forEach(System.out::println);
    }
}
