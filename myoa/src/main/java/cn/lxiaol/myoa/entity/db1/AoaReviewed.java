package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_reviewed")
@ApiModel(value = "AoaReviewed", description = "")
public class AoaReviewed implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "reviewed_id", type = IdType.AUTO)

    private Long reviewedId;
    private String advice;
    private Date reviewedTime;
    private Long statusId;
    private Long proId;
    private Long userId;
    private Integer del;


}

