package com.example.jta.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jta.dao.Test2;

/**
 * <p>
 * 系统用户 服务类
 * </p>
 *
 * @author lixiaolong
 * @since 2021-08-23
 */
public interface Test2Service extends IService<Test2> {

}
