package com.atguigu.spring5;

/**
 * 通过有参构造进行属性注入
 */
public class Orders {

    private String oname;
    private String oaddress;

    public Orders(String oname, String oaddress) {
        this.oname = oname;
        this.oaddress = oaddress;
    }


    public void listName() {
        System.out.println("订单名：" + this.oname+",地址："+ this.oaddress);
    }
}
