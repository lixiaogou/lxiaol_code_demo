package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaNotepaper;
import cn.lxiaol.myoa.mapper.db1.AoaNotepaperMapper;
import cn.lxiaol.myoa.service.AoaNotepaperService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaNotepaperServiceImpl extends ServiceImpl<AoaNotepaperMapper, AoaNotepaper> implements AoaNotepaperService {

    @Override
    public List<AoaNotepaper> findByUserIdOrderByCreateTimeDesc(Long userId) {
        IPage<AoaNotepaper> iPage = new Page<>(1, 5);

        LambdaQueryWrapper<AoaNotepaper> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaNotepaper::getNotepaperUserId, userId);
        qw.orderByDesc(AoaNotepaper::getCreateTime);
        IPage<AoaNotepaper> page = page(iPage, qw);
        return page.getRecords();
    }
}
