package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_file_list")
@ApiModel(value = "AoaFileList", description = "")
public class AoaFileList implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "file_id", type = IdType.AUTO)

    private Long fileId;
    private String fileName;
    private String filePath;
    private String fileShuffix;
    private String contentType;
    private String model;
    private Long pathId;
    private Long size;
    private Date uploadTime;
    private Long fileUserId;
    private Long fileIstrash;
    private Long fileIsshare;


}

