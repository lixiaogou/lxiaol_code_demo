package cn.lxiaol.stream;


import java.util.Arrays;
import java.util.List;

/**
 * 顾名思义，缩减操作，就是把一个流缩减成一个值，比如对一个集合求和、求乘积等。
 *
 * @author lxiaol
 * @date 2021年05月18日 17:35
 */
public class ReduceDemo {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 3, 2);

//    普通集合求和、求最值
        // 求和 写法1 推荐
        Integer sum1 = list.stream().reduce(0, Integer::sum);
        System.out.println("sum1 = " + sum1);

        // 求和 写法2
        Integer sum2 = list.stream().reduce(1, (x, y) -> x + y);
        System.out.println("sum2 = " + sum2);


        // 求积 写法1
        Integer cheng = list.stream().reduce(1, (x, y) -> x * y);
        System.out.println("cheng = " + cheng);

        // 最值 写法1 推荐
        Integer max = list.stream().reduce(0, Integer::max);
        System.out.println("max = " + max);

        // 最值 写法2
        Integer max2 = list.stream().reduce(0, (x, y) -> x >= y ? x : y);
        System.out.println("max2 = " + max2);

    }
}
