package cn.lxiaol.excel_demo.common.listeners;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author lxiaol
 * @date 2021年08月15日 14:14
 */
@Slf4j
public class CommonExcelListener<T> extends AnalysisEventListener<T> {

    private final List<T> list = new ArrayList<>(1000);

    @Override
    public void invoke(T data, AnalysisContext analysisContext) {
        log.info("解析到一条数据:" + data.toString());
        this.list.add(data);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        log.info("解析完成:共解析到{}数据", this.list.size());
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        log.info("解析到一条头数据：{}, currentRowHolder: {}", headMap.toString(), context.readRowHolder().getRowIndex());
        headMap.entrySet().removeIf((h) -> Objects.isNull(h.getValue()) || "".equals(h.getValue()));
        log.info("表头列总数：{},列头为：{}", headMap.size(), headMap.values());
    }

    public List<T> getList() {
        return this.list;
    }

}
