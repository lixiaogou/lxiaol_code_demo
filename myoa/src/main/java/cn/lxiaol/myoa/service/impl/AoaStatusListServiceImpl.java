package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaStatusList;
import cn.lxiaol.myoa.mapper.db1.AoaStatusListMapper;
import cn.lxiaol.myoa.service.AoaStatusListService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaStatusListServiceImpl extends ServiceImpl<AoaStatusListMapper, AoaStatusList> implements AoaStatusListService {

    @Override
    public List<AoaStatusList> findByStatusModel(String statusModel) {
        LambdaQueryWrapper<AoaStatusList> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaStatusList::getStatusModel, statusModel);
        return this.list(qw);
    }

    @Override
    public String findname(Long statusId) {
        LambdaQueryWrapper<AoaStatusList> qw = new LambdaQueryWrapper<>();

        qw.eq(AoaStatusList::getStatusId, statusId);
        AoaStatusList one = this.getById(qw);
        return one.getStatusName();
    }

    @Override
    public String findcolor(Long statusId) {
        LambdaQueryWrapper<AoaStatusList> qw = new LambdaQueryWrapper<>();

        qw.eq(AoaStatusList::getStatusId, statusId);
        AoaStatusList one = this.getById(qw);
        return one.getStatusColor();
    }
}
