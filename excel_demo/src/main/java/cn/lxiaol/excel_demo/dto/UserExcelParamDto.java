package cn.lxiaol.excel_demo.dto;

import lombok.Data;

/**
 * @author lxiaol
 * @date 2021年08月15日 14:16
 */
@Data
public class UserExcelParamDto {
    private String username;

    private String phone;

}
