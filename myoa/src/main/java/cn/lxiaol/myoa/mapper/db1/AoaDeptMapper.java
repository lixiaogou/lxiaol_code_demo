package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaDeptMapper extends BaseMapper<AoaDept> {

    @Select("<script>" +
            "SELECT * FROM aoa_dept WHERE dept_id = #{deptId} limit 1 " +
            "</script>")
    AoaDept findOneByDeptId(@Param("deptId") Long deptId);
}
