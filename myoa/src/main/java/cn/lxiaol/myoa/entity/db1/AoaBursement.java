package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_bursement")
@ApiModel(value = "AoaBursement", description = "")
public class AoaBursement implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "bursement_id", type = IdType.AUTO)

    private Long bursementId;
    private Double allMoney;
    private Integer allinvoices;
    private Date burseTime;
    private String financialAdvice;
    private String managerAdvice;
    private String name;
    private Long typeId;
    private Long operationName;
    private Long proId;
    private Long userName;


}

