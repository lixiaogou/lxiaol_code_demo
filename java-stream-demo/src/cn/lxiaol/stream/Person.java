package cn.lxiaol.stream;

/**
 * @author lxiaol
 * @date 2021年05月18日 17:34
 */
public class Person {
    private String name;
    private int salary;

    public Person() {
    }

    // 构造方法
    public Person(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                '}';
    }
}
