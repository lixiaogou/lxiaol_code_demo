package cn.lxiaol.idempotence.constants;

/**
 * Redis key 常量维护
 *
 * @author zkm
 */
public class RedisKeyConstants {

    /**
     * 存入 Redis 的 Token 键的前缀
     */
    public static final String IDEMPOTENT_TOKEN_PREFIX = "idempotent_token:";
}
