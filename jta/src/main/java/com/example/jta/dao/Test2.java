package com.example.jta.dao;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author lixiaolong
 * @since 2021-08-23
 */
@Data
@Accessors(chain = true)
@TableName("test2")
public class Test2 implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    private String username;


}

