/**
 * *****************************************************
 * Copyright (C) 2019 founder.com. All Rights Reserved
 * This file is part of founder.
 * Unauthorized copy of this file, via any medium is strictly prohibited.
 * Proprietary and Confidential.
 * ****************************************************
 **/
package cn.lxiaol.myoa.aop;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.text.MessageFormat;

/**
 * 日志切面
 *
 * @author lxiaol
 * @date 2021/4/27 10:57
 */
@Aspect
@Component
@Slf4j
public class LogAspect {
    private static final String POINT = "execution(public * cn.lxiaol.myoa.controller..*.*(..))";

    @Around(POINT)
    public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object[] args = joinPoint.getArgs();
        StringBuilder logInfo = new StringBuilder();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String reqType = request.getMethod();
        StringBuilder requestUrl = new StringBuilder(request.getRequestURL());
        logInfo.append("\n")
                .append(MessageFormat.format("requestURL: {0} ", requestUrl)).append("\n")
                .append(MessageFormat.format("requestMethod: {0} ", request.getMethod())).append("\n");
        String params = "";
        try {
            //参数不为空，为空不处理
            if (args.length > 0) {
                if (RequestMethod.GET.toString().equals(reqType)) {
                    params = request.getQueryString() == null ? null : request.getQueryString().replaceAll("&", ",");
                } else if (args[0] instanceof MultipartFile) {
                    MultipartFile file = (MultipartFile) args[0];
                    params = file.getOriginalFilename();
                } else {
                    params = JSON.toJSONString(args);
                }
            }
            logInfo.append(MessageFormat.format("请求参数为: {0}", params)).append("\n");
        } catch (Exception e) {
            logInfo.append("请求参数类型暂时不支持解析!").append("\n");
        }

        log.info("请求参数.{}", logInfo.toString());

        String resultStr;
        // 执行方法
        Object result;
        try {
            // 执行方法
            result = joinPoint.proceed();
        } catch (Exception e) {
            log.warn("异常返回.{}", e);
            throw e;
        }

        resultStr = JSON.toJSONString(result);
        StringBuilder resLogInfo = new StringBuilder();
        resLogInfo.append(MessageFormat.format("响应结果为: {0}", resultStr)).append("\n");
        long endTime = System.currentTimeMillis();
        resLogInfo.append(MessageFormat.format("请求响应时间为: {0}ms", String.valueOf(endTime - startTime)));
        log.info("正常返回.{}", resLogInfo.toString());


        return result;
    }

}
