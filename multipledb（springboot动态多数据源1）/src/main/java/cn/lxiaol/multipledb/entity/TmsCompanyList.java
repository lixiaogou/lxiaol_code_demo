package cn.lxiaol.multipledb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 企业公司列表
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-09
 */
@Data
@Accessors(chain = true)
@TableName("tms_company_list")
public class TmsCompanyList implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    private String id;
    /**
     * 公司类型：A：危险品物流公司 参考字典 company.type
     */
    private String type;
    /**
     * 公司编号：根据类型进行拼凑：A01,A02
     */
    private String num;
    /**
     * 公司名称
     */
    private String name;
    /**
     * 公司电话
     */
    private String phone;
    /**
     * 公司传真
     */
    private String faxes;
    /**
     * 联系人名称
     */
    private String contactName;
    /**
     * 联系人手机号
     */
    private String contactMobile;
    /**
     * 联系人邮箱
     */
    private String contactEmail;
    /**
     * 所在省份编码
     */
    private Integer provinceCode;
    /**
     * 所在城市编码
     */
    private Integer cityCode;
    /**
     * 所在区域编码
     */
    private Integer areaCode;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 经度
     */
    private BigDecimal longitude;
    /**
     * 纬度
     */
    private BigDecimal latitude;
    /**
     * 公司介绍
     */
    private String introduce;
    /**
     * 营业执照证号
     */
    private String licenseImageNo;
    /**
     * 营业执照
     */
    private String licenseImage;
    /**
     * 危险品道路运输许可证号
     */
    private String dangerousLicenseNo;
    /**
     * 危险品道路运输许可证
     */
    private String dangerousLicenseImage;
    /**
     * 创建时间
     */
    private Integer addTime;
    /**
     * 修改时间
     */
    private Integer updateTime;
    /**
     * 公章
     */
    private String imgzhang;


}

