package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaTaskUser;
import cn.lxiaol.myoa.mapper.db1.AoaTaskUserMapper;
import cn.lxiaol.myoa.service.AoaTaskUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaTaskUserServiceImpl extends ServiceImpl<AoaTaskUserMapper, AoaTaskUser> implements AoaTaskUserService {

    @Resource
    private AoaTaskUserMapper aoaTaskUserMapper;

    @Override
    public List<AoaTaskUser> findByUserIdAndStatusId(Long userId) {
        return aoaTaskUserMapper.findByUserIdAndStatusId(userId);
    }
}
