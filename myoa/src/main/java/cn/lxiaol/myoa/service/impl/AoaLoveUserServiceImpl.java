package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaLoveUser;
import cn.lxiaol.myoa.mapper.db1.AoaLoveUserMapper;
import cn.lxiaol.myoa.service.AoaLoveUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaLoveUserServiceImpl extends ServiceImpl<AoaLoveUserMapper, AoaLoveUser> implements AoaLoveUserService {

}
