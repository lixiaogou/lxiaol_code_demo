package cn.lxiaol.multipledb.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 企业公司列表 前端控制器
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-09
 */
@RestController
@RequestMapping("/migration/tmsCompanyList")
public class TmsCompanyListController {

}

