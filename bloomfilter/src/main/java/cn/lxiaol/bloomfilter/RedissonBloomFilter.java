package cn.lxiaol.bloomfilter;

import org.redisson.Redisson;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

public class RedissonBloomFilter {

    public static void main(String[] args) {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://127.0.0.1:2020");
        config.useSingleServer().setPassword("yiguan789");

        // 构造redis
        RedissonClient redissonClient = Redisson.create(config);

        // 获取布隆过滤器，并给该过滤器起一个名字，叫做 nickNameList
        RBloomFilter<Object> bloomFilter = redissonClient.getBloomFilter("nickNameList");

        // 初始化该过滤器，预计元素为 100万，误差率为3%  默认是3%
        bloomFilter.tryInit(1000000, 0.03);

        //将昵称 "蹬杆老王子" 插入到过滤器中
        bloomFilter.add("蹬杆老王子");


        //判断下面昵称是否在布隆过滤器中
        //输出false
        System.out.println(bloomFilter.contains("卡特机长"));
        //输出true
        System.out.println(bloomFilter.contains("蹬杆老王子"));

        redissonClient.shutdown();
    }

}
