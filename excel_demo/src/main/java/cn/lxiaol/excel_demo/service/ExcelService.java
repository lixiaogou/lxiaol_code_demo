package cn.lxiaol.excel_demo.service;

import cn.lxiaol.excel_demo.common.utils.ExcelExportUtil;
import cn.lxiaol.excel_demo.dto.UserExcelDto;
import cn.lxiaol.excel_demo.dto.UserExcelParamDto;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.DateUtil;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author lxiaol
 * @date 2021年08月15日 14:31
 */
@Service
@Slf4j
public class ExcelService {

    /**
     * 导出根据筛选条件查询到的数据
     *
     * @param dto      筛选条件
     * @param request
     * @param response
     * @throws IOException
     */
    public void export(UserExcelParamDto dto, HttpServletRequest request,
                       HttpServletResponse response) throws IOException {

        // 模拟从数据库查询到10条数据，真是业务可将这段改为查询数据库获取到list
        List<UserExcelDto> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            UserExcelDto userExcelDto = new UserExcelDto();
            userExcelDto.setUsername("用户：" + i);
            userExcelDto.setUsername("手机号：188****8888");
            list.add(userExcelDto);
        }
        // 将 list 数据存放到maps中，主要是为了导出时根据表头填写对应的值
        List<Map> maps = new ArrayList<>();
        list.forEach(po -> maps.add(JSONObject.parseObject(JSONObject.toJSONString(po), Map.class)));

        // 获取到excel导出工具类，并根据传入class设置了表头信息
        ExcelExportUtil excelExportUtil =
                ExcelExportUtil.getExcelExportUtil(maps, UserExcelDto.class.getDeclaredFields());

        excelExportUtil.setTitle("Excel导出_" + DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now()));
        excelExportUtil.exportExport(response);
    }
}
