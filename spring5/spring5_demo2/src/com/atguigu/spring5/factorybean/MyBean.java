package com.atguigu.spring5.factorybean;

import com.atguigu.spring5.collectiontype.Course;
import org.springframework.beans.factory.FactoryBean;

/**
 * 工厂bean  FactoryBean
 * 创建类 实现 FactoryBean接口 实现接口内方法
 */
public class MyBean implements FactoryBean<Course> {

    /**
     * 返回实例 定义返回的Bean类型
     * @return
     * @throws Exception
     */
    @Override
    public Course getObject() throws Exception {
        Course course = new Course();
        course.setCname("木香奈");
        return course;
    }

    /**
     * 返回类型
     * @return
     */
    @Override
    public Class<?> getObjectType() {
        return null;
    }

    /**
     * 是否单例
     * @return
     */
    @Override
    public boolean isSingleton() {
        return false;
    }
}
