package cn.lxiaol.bloomfilter;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

public class BloomFilterCase {

    /**
     * 预计要存入多少数据
     */
    private static final int size = 1000000;

    /**
     * 期望的误判率
     */
    private static double fpp = 0.03;

    /**
     * 布隆过滤器
     */
    private static BloomFilter<Integer> bloomFilter = BloomFilter.create(Funnels.integerFunnel(), size, fpp);


    private static final int total = 1000000;

    public static void main(String[] args) {

        // 插入100万条样本数据
        for (int i = 0; i < total; i++) {
            bloomFilter.put(i);
        }


        //用另外10万测试误判率
        int count = 0;
        for (int i = total; i < total + 100000; i++) {
            if (bloomFilter.mightContain(i)) {
                count++;
                System.out.println(i + "误判了");
            }
        }

        System.out.println("总误判数:" + count);
        System.out.println("fpp：" + 1.0 * count / 100000);

    }
}
