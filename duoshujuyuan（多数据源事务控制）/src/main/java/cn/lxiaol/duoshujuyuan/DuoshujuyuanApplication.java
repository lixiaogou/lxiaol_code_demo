package cn.lxiaol.duoshujuyuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DuoshujuyuanApplication {

    public static void main(String[] args) {
        SpringApplication.run(DuoshujuyuanApplication.class, args);
        System.out.println("success");
    }

}
