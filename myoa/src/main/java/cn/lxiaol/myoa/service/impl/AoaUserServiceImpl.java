package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaUser;
import cn.lxiaol.myoa.mapper.db1.AoaUserMapper;
import cn.lxiaol.myoa.service.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaUserServiceImpl extends ServiceImpl<AoaUserMapper, AoaUser> implements AoaUserService {

    @Resource
    private AoaUserMapper aoaUserMapper;
    @Resource
    private AoaAttendsListService aoaAttendsListService;
    @Resource
    private AoaDeptService aoaDeptService;
    @Resource
    private AoaPositionService aoaPositionService;
    @Resource
    private AoaRoleService aoaRoleService;


    @Override
    public AoaUser findOne(Long userId) {
        return aoaUserMapper.findOne(userId);
    }

    @Override
    public List<AoaUser> findAll() {
        return aoaUserMapper.findAll();
    }

    @Override
    public List<AoaUser> findByFatherId(Long userId) {
        LambdaQueryWrapper<AoaUser> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaUser::getFatherId, userId);
        return this.list(qw);
    }

    @Override
    public PageInfo<AoaUser> findmyemployuser(int pageNum, String baseKey, Long fatherId) {
        PageHelper.startPage(pageNum, 10);

        if (!Strings.isBlank(baseKey)) {
//            select u from User u where  (u.userName like %?1% or u.realName like %?1%) and u.fatherId=?2
            List<AoaUser> list = this.findbyFatherId(baseKey, fatherId);
            return new PageInfo<>(list);
        }

        LambdaQueryWrapper<AoaUser> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaUser::getFatherId, fatherId);
        List<AoaUser> list = this.list(qw);

        return new PageInfo<>(list);

    }

    public List<AoaUser> findbyFatherId(String baseKey, Long fatherId) {

        LambdaQueryWrapper<AoaUser> qw = new LambdaQueryWrapper<>();
        qw.nested(qw2 -> qw2.like(AoaUser::getUserName, baseKey).or().like(AoaUser::getRealName, baseKey));
        qw.eq(AoaUser::getFatherId, fatherId);

        List<AoaUser> list = this.list(qw);
        return list;
    }


}
