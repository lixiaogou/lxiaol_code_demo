package cn.lxiaol.duoshujuyuan.mapper.slave;

import cn.lxiaol.duoshujuyuan.model.slave.SlaveUser;

public interface SlaveUserMapper {

    int insertSelective(SlaveUser record);
}