package cn.lxiaol.myoa.controller;


import cn.lxiaol.myoa.entity.db1.AoaUser;
import cn.lxiaol.myoa.entity.db2.SysUser;
import cn.lxiaol.myoa.service.AoaUserService;
import cn.lxiaol.myoa.service.SysUserService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class TestController {

    @Resource
    private AoaUserService aoaUserService;
    @Resource
    private SysUserService sysUserService;

    /**
     * 销售预测发起后，新增预测明细数据和预测填报记录
     *
     * @return
     */
    @PostMapping("/testDb1")
    public IPage testDb1() {
        Page<AoaUser> page = new Page<>(1, 5);
        IPage<AoaUser> page1 = aoaUserService.page(page, null);
        return page1;
    }

    /**
     * 销售预测发起后，新增预测明细数据和预测填报记录
     *
     * @return
     */
    @PostMapping("/testDb2")
    public List<SysUser> testDb2() {
        List<SysUser> list = sysUserService.list(null);
        return list;
    }

}

