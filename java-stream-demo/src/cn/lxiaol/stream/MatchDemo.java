package cn.lxiaol.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 流的匹配，与筛选类似，也是按照规则提取元素，不同的是，匹配返回的是单个元素或单个结果。
 * 普通类型筛选
 *
 * @author lxiaol
 * @date 2021年05月18日 17:35
 */
public class MatchDemo {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(7, 6, 9, 3, 8, 2, 1);
        // 匹配大于7的第一个数
        Optional<Integer> findFirst = list.stream().filter(x -> x > 7).findFirst();
        System.out.println(findFirst);

        // 匹配任意（适用于并行流）
        Optional<Integer> findAny = list.parallelStream().filter(x -> x > 5).findAny();
        System.out.println(findAny);

        // 是否包含
        boolean anyMatch = list.stream().anyMatch(x -> x > 5);
        System.out.println(anyMatch);


    }
}
