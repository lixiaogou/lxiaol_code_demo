package cn.lxiaol.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * user 接口
 * @author lxiaol
 * @date 2021/4/15 20:44
 */
@Slf4j
@Controller
@RequestMapping(value = "/api/user")
public class UserController {

}
