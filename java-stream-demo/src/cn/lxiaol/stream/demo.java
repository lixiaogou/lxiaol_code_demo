package cn.lxiaol.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author lxiaol
 * @date 2021年05月18日 17:20
 */
public class demo {
    public static void main(String[] args) {
        //1.通过Arrays.stream
        //1.1基本类型
        int[] arr = new int[]{1, 2, 34, 5};
        IntStream intStream = Arrays.stream(arr);
        //1.2引用类型
        Student[] studentArr = new Student[]{new Student("s1", 29), new Student("s2", 27)};
        Stream<Student> studentStream = Arrays.stream(studentArr);
        //2.通过Stream.of
        Stream<Integer> stream1 = Stream.of(1, 2, 34, 5, 65);
        //注意生成的是int[]的流
        Stream<int[]> stream2 = Stream.of(arr, arr);
        stream2.forEach(System.out::println);


        List<String> strs = Arrays.asList("11212", "dfd", "2323", "dfhgf");
        //创建普通流
        Stream<String> stream = strs.stream();
        //创建并行流
        Stream<String> stream3 = strs.parallelStream();


    }


}
