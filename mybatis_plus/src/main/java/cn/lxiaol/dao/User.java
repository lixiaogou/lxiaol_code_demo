package cn.lxiaol.dao;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author lixiaolong
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    // 默认 ID_WORKER 全局唯一id

    // 主键生成策略 参考 https://www.cnblogs.com/haoxinyue/p/5208136.html
    // 1 数据库自增长序列或字段
    // 2 UUID
    // 3 UUID的变种 GUID
    // 4 redis 生成ID
    // 5 Twitter的snowflake算法  雪花算法 目前比较常用
    // 6 利用zookeeper生成唯一ID
    // 7 MongoDB的ObjectId 类似雪花算法

    //    雪花算法：
    //    snowflake是Twitter开源的分布式ID生成算法，结果是一个long型的ID。其核心思想是：使用41bit作为
    //    毫秒数，10bit作为机器的ID（5个bit是数据中心，5个bit的机器ID），12bit作为毫秒内的流水号（意味
    //    着每个节点在每毫秒可以产生 4096 个 ID），最后还有一个符号位，永远是0。可以保证几乎全球唯一

    //    AUTO(0), // 数据库id自增
    //    NONE(1), // 未设置主键
    //    INPUT(2), // 手动输入
    //    ID_WORKER(3), // 默认的全局唯一id 雪花算法
    //    UUID(4), // 全局唯一id uuid
    //    ID_WORKER_STR(5); //ID_WORKER 字符串表示法
    @TableId(type = IdType.AUTO) // 自增 数据库对应字段应设置为自增
    private Long id;


    private String name;
    private Integer age;
    private String email;

//    配置自动填充    CURRENT_TIMESTAMP
    // 先删除数据库的默认值
    @TableField(fill= FieldFill.INSERT) // 新增
    private Date gmtCreate;

    @TableField(fill= FieldFill.INSERT_UPDATE) // 新增 修改
    private Date gmtModified;

    @Version //乐观锁Version注解
    private Integer version;

    @TableLogic //逻辑删除
    private Integer deleted;

}
