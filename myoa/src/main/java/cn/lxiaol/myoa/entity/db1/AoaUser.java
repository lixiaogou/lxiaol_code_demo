package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_user")
@ApiModel(value = "AoaUser", description = "用户表")
public class AoaUser implements Serializable {

        private static final long serialVersionUID = 1L;

        @TableId(value = "user_id", type = IdType.AUTO)
        private Long userId;
        private String address;
        private String bank;
        private Date birth;
        private String eamil;
        private Long fatherId;
        private Date hireTime;
        private String userIdcard;
        private String imgPath;
        private Integer isLock;
        private String lastLoginIp;
        private Date lastLoginTime;
        private Date modifyTime;
        private Long modifyUserId;
        private String password;
        private String realName;
        private Float salary;
        private String userSchool;
        private String sex;
        private String themeSkin;
        private String userEdu;
        private String userName;
    private String userSign;
    private String userTel;
    private Long deptId;
    private Long positionId;
    private Long roleId;
    private Integer superman;
    private Integer holiday;
    private String pinyin;


    //外键关联 职位表
    @TableField(exist = false)
    private AoaPosition position;

    //外键关联 部门表
    @TableField(exist = false)
    private AoaDept dept;

    //外键关联 角色表
    @TableField(exist = false)
    private AoaRole role;

    @TableField(exist = false)
    private List<AoaScheduleList> scheduleLists;

    @TableField(exist = false)
    private List<AoaReplyList> replys;

    @TableField(exist = false)
    private List<AoaDiscussList> discuss;

    @TableField(exist = false)
    private List<AoaNoteList> note;

    @TableField(exist = false)
    private Set<AoaAttendsList> aSet;


}

