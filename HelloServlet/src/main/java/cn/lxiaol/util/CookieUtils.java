package cn.lxiaol.util;

import javax.servlet.http.Cookie;
import java.util.Objects;

/**
 * @author lxiaol
 * @date 2021年05月08日 15:26
 */

public class CookieUtils {
    /**
     * 查找指定名称的Cookie对象
     *
     * @param name
     * @param cookies
     * @return
     */
    public static Cookie findCookie(String name, Cookie[] cookies) {
        if (Objects.isNull(name) || Objects.isNull(cookies) || cookies.length == 0) {
            return null;
        }

        for (Cookie cookie : cookies) {
            if (name.equals(cookie.getName())) {
                return cookie;
            }
        }

        return null;
    }

}

