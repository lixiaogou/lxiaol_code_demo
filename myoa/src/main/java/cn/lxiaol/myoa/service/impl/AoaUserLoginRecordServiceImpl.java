package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaUserLoginRecord;
import cn.lxiaol.myoa.mapper.db1.AoaUserLoginRecordMapper;
import cn.lxiaol.myoa.service.AoaUserLoginRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaUserLoginRecordServiceImpl extends ServiceImpl<AoaUserLoginRecordMapper, AoaUserLoginRecord> implements AoaUserLoginRecordService {

    @Resource
    private AoaUserLoginRecordMapper aoaUserLoginRecordMapper;

    @Override
    public Integer countlog(String format) {
        return aoaUserLoginRecordMapper.countlog(format);
    }
}
