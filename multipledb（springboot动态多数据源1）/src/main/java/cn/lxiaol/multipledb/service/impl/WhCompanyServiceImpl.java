package cn.lxiaol.multipledb.service.impl;

import cn.lxiaol.multipledb.service.IWhCompanyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.lxiaol.multipledb.entity.WhCompany;
import cn.lxiaol.multipledb.mapper.db2.IWhCompanyMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-09
 */
@Service
public class WhCompanyServiceImpl extends ServiceImpl<IWhCompanyMapper, WhCompany> implements IWhCompanyService {

}
