package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaTaskUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaTaskUserMapper extends BaseMapper<AoaTaskUser> {

    @Select("<script>" +
            "SELECT taskuser0_.* FROM " +
            "	aoa_task_user taskuser0_ " +
            "	LEFT OUTER JOIN aoa_user user1_ ON taskuser0_.task_recive_user_id = user1_.user_id " +
            "WHERE " +
            "	user1_.user_id =#{userId} " +
            "	AND taskuser0_.status_id =3" +
            "</script>")
    List<AoaTaskUser> findByUserIdAndStatusId(Long userId);
}
