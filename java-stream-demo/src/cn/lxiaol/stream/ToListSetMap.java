package cn.lxiaol.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author lxiaol
 * @date 2021年05月19日 14:32
 */
public class ToListSetMap {


    public static void main(String[] args) {
        List<Person> list = new ArrayList<Person>();
        list.add(new Person("Tom", 8900));
        list.add(new Person("Jack", 7000));
        list.add(new Person("Lily", 9000));
        list.add(new Person("Lily", 5000));

        // toList
        List<String> toList = list.stream().map(Person::getName).collect(Collectors.toList());
        System.out.println("toList = " + toList);

        // toSet
        Set<String> toSet = list.stream().map(Person::getName).collect(Collectors.toSet());
        System.out.println("toSet = " + toSet);


        // toMap key重复会报错
        Map<String, Person> toMap = list.stream().collect(Collectors.toMap(Person::getName, p -> p));
        System.out.println("toMap = " + toMap);


    }
}
