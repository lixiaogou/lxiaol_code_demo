package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db2.SysUser;
import cn.lxiaol.myoa.mapper.db2.SysUserMapper;
import cn.lxiaol.myoa.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}
