package com.example.jta.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.jta.dao.Test;

/**
 * <p>
 * 系统用户 服务类
 * </p>
 *
 * @author lixiaolong
 * @since 2021-08-23
 */
public interface TestService extends IService<Test> {

}
