package cn.lxiaol.service;

import cn.lxiaol.vo.ManageBuildRelVO;
import cn.lxiaol.dao.UserDao;
import cn.lxiaol.dao.UserDaoImpl;
import cn.lxiaol.vo.UserVO;

import java.util.List;
import java.util.Map;


public class UserServiceImpl implements UserService {

	private UserDao userDao = new UserDaoImpl();
	@Override
    public UserVO findByNoAndPwd(String no, String pwd) {
		return userDao.findByNoAndPwd(no,pwd);
	}
   @Override
   public UserVO queryByAjaxWithId(String no)
   {
      return userDao.queryByAjaxWithId(no);
   }
   @Override
   public Map queryByPage(UserVO vo, int start, int pageSize)
   {
      return userDao.queryByPage(vo,start,pageSize);
   }
   @Override
   public Map add(UserVO userVO)
   {
      return userDao.add(userVO);
   }
   @Override
   public Map del(String id)
   {
      return userDao.del(id);
   }
   @Override
   public Map mod(UserVO userVO)
   {
      return userDao.mod(userVO);
   }
   @Override
   public Map queryByAjax(UserVO userVO)
   {
      return userDao.queryByAjax(userVO);
   }
   @Override
   public void addRel(ManageBuildRelVO manageBuildRelVO)
   {
      userDao.addRel(manageBuildRelVO);
   }
   @Override
   public void delRel(String id)
   {
      userDao.delRel(id);      
   }
   @Override
   public Map queryAdminByPage(UserVO vo, int start, int pageSize)
   {
      return userDao.queryAdminByPage(vo,start,pageSize);
   }
   @Override
   public UserVO queryByAjaxWithNo(String no)
   {
      return userDao.queryByAjaxWithNo(no);
   }
   @Override
   public UserVO queryAdminByAjaxWithId(String id)
   {
      return userDao.queryAdminByAjaxWithId(id);
   }
   @Override
   public String queryStudentCountByAjax(String id)
   {
      return userDao.queryStudentCountByAjax(id);
   }
   @Override
   public List queryStudentsByAjax(String id)
   {
      return userDao.queryStudentsByAjax(id);
   }
   @Override
   public void modPassword(UserVO userVO1)
   {
      userDao.modPassword(userVO1);
   }

}
