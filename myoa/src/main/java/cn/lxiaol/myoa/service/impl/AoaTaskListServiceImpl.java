package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaTaskList;
import cn.lxiaol.myoa.mapper.db1.AoaTaskListMapper;
import cn.lxiaol.myoa.service.AoaTaskListService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaTaskListServiceImpl extends ServiceImpl<AoaTaskListMapper, AoaTaskList> implements AoaTaskListService {

    @Override
    public Integer ountfinish(long l, Long userId) {
        LambdaQueryWrapper<AoaTaskList> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaTaskList::getStatusId, l).eq(AoaTaskList::getTaskPushUserId, userId);
        return this.count(qw);
    }
}
