package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_process_list")
@ApiModel(value = "AoaProcessList", description = "")
public class AoaProcessList implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "process_id", type = IdType.AUTO)

    private Long processId;
    private Date applyTime;
    private Long deeplyId;
    private Date endTime;
    private String processDes;
    private String processName;
    private Integer procseeDays;
    private Integer isChecked;
    private Date startTime;
    private Long statusId;
    private String typeName;
    private Long proFileId;
    private Long processUserId;
    private String shenuser;


}

