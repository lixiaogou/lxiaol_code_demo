package com.atguigu.spring5.autowire;

public class Emp {
    private Dept dept;
    private Dept2 dept2;

    public void setDept2(Dept2 dept2) {
        this.dept2 = dept2;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "dept=" + dept +
                ", dept2=" + dept2 +
                '}';
    }

    public void test() {
        System.out.println(dept);
        System.out.println(dept2);
    }
}
