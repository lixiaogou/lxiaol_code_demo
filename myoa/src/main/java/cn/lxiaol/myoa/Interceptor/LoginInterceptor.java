package cn.lxiaol.myoa.Interceptor;

import cn.lxiaol.myoa.common.Tool;
import cn.lxiaol.myoa.entity.db1.AoaSysMenu;
import cn.lxiaol.myoa.entity.db1.AoaUser;
import cn.lxiaol.myoa.entity.db1.AoaUserLog;
import cn.lxiaol.myoa.entity.dto.RoleMenuDTO;
import cn.lxiaol.myoa.mapper.db1.AoaRolePowerListMapper;
import cn.lxiaol.myoa.mapper.db1.AoaSysMenuMapper;
import cn.lxiaol.myoa.mapper.db1.AoaUserMapper;
import cn.lxiaol.myoa.service.AoaUserLogService;
import cn.lxiaol.myoa.service.impl.AoaUserLogServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 登陆拦截器
 *
 * @author lxiaol
 * @date 2021/4/24 12:33
 */
@Slf4j
@Component
public class LoginInterceptor implements HandlerInterceptor {

    /**
     * 该方法将在Controller处理之前进行调用。 当preHandle的返回值为false的时候整个请求就结束了
     * <p>请求之前调用该方法,进行登录权限等一系列判断,然后并跳转</p>
     *
     * @author lxiaol
     * @date 2021/4/22 17:16
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        HttpSession session = request.getSession();

        //不为空表示已经登陆了，否则重定向到登陆页面
        Object userId = session.getAttribute("userId");
        if (Objects.nonNull(userId)) {
            //导入dao类
            Long uid = Long.parseLong(userId + "");
            AoaUserMapper aoaUserMapper = Tool.getBean(AoaUserMapper.class, request);
            AoaRolePowerListMapper aoaRolePowerListMapper = Tool.getBean(AoaRolePowerListMapper.class, request);

            AoaUser user = aoaUserMapper.findOne(uid);
            Long roleId = user.getRole().getRoleId();

            List<RoleMenuDTO> oneMenuAll = aoaRolePowerListMapper.findbyparentxianall(0L, roleId, 1, 0);
            List<RoleMenuDTO> twoMenuAll = aoaRolePowerListMapper.findbyparentsxian(0L, roleId, 1, 0);
            List<RoleMenuDTO> all = new ArrayList<>();
            all.addAll(oneMenuAll);
            all.addAll(twoMenuAll);

            //获取当前访问的路径
            String url = request.getRequestURL().toString();
            log.info("当前访问路径 url={}", url);

            String dispatcher = "notlimit";

            for (RoleMenuDTO rolemenu : all) {
                String menuUrl = rolemenu.getMenuUrl();
                //如果角色菜单不拥有此时请求的url，直接返回
                if (!menuUrl.equals(url)) {
                    return true;
                } else {
                    //如果角色菜单拥有此时请求的url，转发请求
                    request.getRequestDispatcher(dispatcher).forward(request, response);
                }
            }

        } else {
            // 重定向到登录页
            response.sendRedirect("/logins");
            return false;
        }
        return true;

    }


    /**
     * 该方法也是需要当前对应的Interceptor的preHandle方法的返回值为true时才会执行。
     * 该方法将在整个请求完成之后，也就是DispatcherServlet渲染了视图执行。
     * （这个方法的主要作用是用于清理资源的）
     * <p>
     * 请求完成之后,添加操作日志
     * </p>
     *
     * @author lxiaol
     * @date 2021/4/22 17:18
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        HttpSession session = request.getSession();
        //导入dao类
        AoaUserMapper aoaUserMapper = Tool.getBean(AoaUserMapper.class, request);
        AoaSysMenuMapper aoaSysMenuMapper = Tool.getBean(AoaSysMenuMapper.class, request);

        AoaUserLogService aoaUserLogService = Tool.getBean(AoaUserLogServiceImpl.class, request);

        AoaUserLog uLog = new AoaUserLog();
        //首先就获取ip
        InetAddress ia = InetAddress.getLocalHost();
        String ip = ia.getHostAddress();
        uLog.setIpAddr(ip);
        String servletPath = request.getServletPath();
        uLog.setUrl(servletPath);
        uLog.setLogTime(new Date());

        Long id = (Long) session.getAttribute("userId");
        if (Objects.isNull(id)) {
            return;
        }
        uLog.setUser(aoaUserMapper.findOne(id));
        //从菜单表里面匹配
        List<AoaSysMenu> sMenus = (List<AoaSysMenu>) aoaSysMenuMapper.selectList(null);
        for (AoaSysMenu systemMenu : sMenus) {
            String menuUrl = systemMenu.getMenuUrl();
            if (menuUrl.equals(servletPath)) {
                //只有当该记录的路径不等于第一条的时候
                AoaUserLog aoaUserLog = aoaUserLogService.findByUserlaset(1L);
                if (!aoaUserLog.getUrl().equals(menuUrl)) {
                    uLog.setTitle(systemMenu.getMenuName());
                    //只要匹配到一个保存咯
                    aoaUserLogService.save(uLog);
                }
            }
        }
    }

}