package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaUserLog;
import cn.lxiaol.myoa.mapper.db1.AoaUserLogMapper;
import cn.lxiaol.myoa.service.AoaUserLogService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaUserLogServiceImpl extends ServiceImpl<AoaUserLogMapper, AoaUserLog> implements AoaUserLogService {

    @Override
    public List<AoaUserLog> findByUserId(Long userId) {
        IPage<AoaUserLog> page = new Page<>(1, 13);
        LambdaQueryWrapper<AoaUserLog> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaUserLog::getUserId, userId);
        qw.orderByDesc(AoaUserLog::getLogTime);
        IPage<AoaUserLog> pageInfo = this.page(page, qw);
        return pageInfo.getRecords();
    }

    @Override
    public AoaUserLog findByUserlaset(long l) {
//SELECT * from aoa_user_log where aoa_user_log.user_id=?1 ORDER BY aoa_user_log.log_time DESC LIMIT 0,1
        IPage<AoaUserLog> page = new Page<>(1, 1);
        LambdaQueryWrapper<AoaUserLog> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaUserLog::getUserId, l);
        qw.orderByDesc(AoaUserLog::getLogTime);
        IPage<AoaUserLog> pageInfo = this.page(page, qw);
        return pageInfo.getRecords().get(0);
    }
}
