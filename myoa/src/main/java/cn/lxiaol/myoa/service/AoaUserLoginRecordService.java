package cn.lxiaol.myoa.service;

import cn.lxiaol.myoa.entity.db1.AoaUserLoginRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 登录日志服务类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaUserLoginRecordService extends IService<AoaUserLoginRecord> {

    Integer countlog(String format);
}
