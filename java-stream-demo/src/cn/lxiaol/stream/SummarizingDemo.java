package cn.lxiaol.stream;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author lxiaol
 * @date 2021年05月19日 11:54
 */
public class SummarizingDemo {

    public static void main(String[] args) {
        List<Person> list = new ArrayList<Person>();
        list.add(new Person("Tom", 8000));
        list.add(new Person("Jack", 7000));
        list.add(new Person("Lily", 9000));

        DoubleSummaryStatistics sum1 = list.stream().collect(Collectors.summarizingDouble(Person::getSalary));
        System.out.println("sum1 = " + sum1);

        IntSummaryStatistics sum2 = list.stream().collect(Collectors.summarizingInt(Person::getSalary));
        System.out.println("sum2 = " + sum2);


        LongSummaryStatistics sum3 = list.stream().collect(Collectors.summarizingLong(Person::getSalary));
        System.out.println("sum3 = " + sum3);
    }
}
