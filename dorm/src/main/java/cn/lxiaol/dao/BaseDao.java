package cn.lxiaol.dao;

import cn.lxiaol.util.C3P0_Utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;


public class BaseDao {
    protected PreparedStatement ps = null;
    protected ResultSet rs = null;
    private Connection customConn = null;
    protected HashMap hashMap = null;

    public Connection getWrappedConnection() {
        if (customConn == null) {
            try {
                customConn = C3P0_Utils.getConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return customConn;
    }

    public void cleanUp() throws Exception {
        C3P0_Utils.release(customConn, ps, rs);
     /* try
      {
         if (rs != null)
         {
            rs.close();
            rs = null;
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         try
         {
            if (ps != null)
            {
               ps.close();
               ps = null;
            }
         }
         catch (Exception e)
         {
            e.printStackTrace();
         }
      }*/
    }
}
