package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_director")
@ApiModel(value = "AoaDirector", description = "")
public class AoaDirector implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "director_id", type = IdType.AUTO)

    private Long directorId;
    private String address;
    private String companyNumber;
    private String email;
    private Integer imagePath;
    private String phoneNumber;
    private String pinyin;
    private String remark;
    private String sex;
    private String userName;
    private Long userId;
    private String companyname;


}

