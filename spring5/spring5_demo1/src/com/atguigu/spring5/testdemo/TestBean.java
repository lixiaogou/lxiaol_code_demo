package com.atguigu.spring5.testdemo;

import com.atguigu.spring5.bean.Emp;
import com.atguigu.spring5.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestBean {

    /**
     * 属性注入--外部bean  一个类中的字段类型是另个一类
     */
    @Test
    public void testUserService() {
        // 加载spring的配置文件
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean2.xml");
        //获取配置创建的对象
        UserService user = ac.getBean("userService", UserService.class);
        user.add();
    }

    /**
     * 属性注入-内部bean
     */
    @Test
    public void testEmp() {
        // 加载spring的配置文件
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean2.xml");
        //获取配置创建的对象
        Emp user = ac.getBean("emp", Emp.class);
        user.add();
    }
    /**
     * 属性注入-级联赋值
     */
    @Test
    public void testEmp2() {
        // 加载spring的配置文件
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean2.xml");
        //获取配置创建的对象
        Emp user = ac.getBean("emp2", Emp.class);
        user.add();
    }

    /**
     * 属性注入-级联赋值
     */
    @Test
    public void testEmp3() {
        // 加载spring的配置文件
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean2.xml");
        //获取配置创建的对象
        Emp user = ac.getBean("emp3", Emp.class);
        user.add();
    }

}
