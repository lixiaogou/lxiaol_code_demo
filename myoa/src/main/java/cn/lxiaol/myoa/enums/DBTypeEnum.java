package cn.lxiaol.myoa.enums;
/**
 * 数据源切换
 *
 * @author lxiaol
 * @date 2021/4/21 11:38
 */
public enum DBTypeEnum {
    /**
     * 数据源切换
     */
    db1("db1"),
    db2("db2"),
    db3("db3");

    private final String value;

    DBTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
