package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaRoleMapper extends BaseMapper<AoaRole> {
    @Select("<script>" +
            "SELECT * FROM aoa_role_ WHERE role_id =#{roleId} limit 1 " +
            "</script>")
    AoaRole findOneByRoleId(@Param("roleId") Long roleId);
}
