package com.atguigu.spring5;

/**
 * 测试spring基于xml方式 创建对象 注入属性
 * set方式进行 属性注入
 */
public class Book {

    private String name;
    private String author;
    private String address;
    private String ocean;

    public void listName() {
        System.out.println("书名：" + this.name+",作者："+ this.author);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setOcean(String ocean) {
        this.ocean = ocean;
    }

    public void printAddress() {
        System.out.println("地址：" + this.address);
    }
    public void printOcean() {
        System.out.println("海洋：" + this.ocean);
    }



}
