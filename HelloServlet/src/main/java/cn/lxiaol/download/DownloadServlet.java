package cn.lxiaol.download;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import sun.misc.BASE64Encoder;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author lxiaol
 * @date 2021年05月07日 14:55
 */
public class DownloadServlet extends HttpServlet {
    /**
     * 用来处理下载的数据
     *
     * @param req  请求
     * @param resp 响应
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //1、获取要下载的文件名
        String dowloadFileName = "1.jpg";

        //2、读取要下载的文件内容(通过ServletContext对象可以读取)
        ServletContext servletContext = this.getServletContext();

        //3、设置响应头告诉客户端返回的数据类型
        // 获取要下载的文件类型
        String mimeType = servletContext.getMimeType("/imgs/" + dowloadFileName);
        System.out.println("载的文件类型 = " + mimeType);
        resp.setContentType(mimeType);

        //4、设置响应头告诉客户端收到的数据是用于下载的
        // Content-Disposition 响应头，表示收到的数据如何处理
        // attachment 表示需要下载该数据
        // filename 表示下载的文件名
        // 把中文名进行UTF-8 编码操作。
        String fileName = "attachment; fileName=" + URLEncoder.encode("中文.jpg", "UTF-8");
        String ua = req.getHeader("User-Agent");
        // 判断是否是火狐浏览器
        if (ua.contains("Firefox")) {
            // 使用下面的格式进行BASE64 编码后
            String encode = new BASE64Encoder().encode("中文.jpg".getBytes(StandardCharsets.UTF_8));
            fileName = "attachment; fileName==?utf-8?B?" + encode + "?=";
        }
        // 然后把编码后的字符串设置到响应头中
        resp.setHeader("Content-Disposition", fileName);

        //5、把下载的文件内容回传给客户端
        //webapp目录 就是根目录
        InputStream resourceAsStream = servletContext.getResourceAsStream("/imgs/" + dowloadFileName);
        // 获取响应的输出流
        ServletOutputStream outputStream = resp.getOutputStream();
        // 读取输入流中的全部数据，复制给输出流，输出给客户端
        IOUtils.copy(resourceAsStream, outputStream);

    }

}
