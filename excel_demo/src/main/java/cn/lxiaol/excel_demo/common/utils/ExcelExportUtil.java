package cn.lxiaol.excel_demo.common.utils;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author lxiaol
 * @date 2021年08月15日 14:35
 */
@Data
public class ExcelExportUtil {

    //表头
    private String title;
    //各个列的表头
    private String[] heardList;
    //各个列的元素key值
    private String[] heardKey;
    //需要填充的数据信息
    private List<Map> data;
    //字体大小
    private int fontSize = 14;
    //行高
    private int rowHeight = 30;
    //列宽
    private int columWidth = 200;
    //工作表
    private String sheetName = "sheet1";

    /**
     * 开始导出数据信息
     */
    public void exportExport(HttpServletResponse response) throws IOException {
        //创建工作簿
        Workbook wb = new XSSFWorkbook();
        //创建工作表
        Sheet sheet = wb.createSheet(this.sheetName);
        //设置默认行宽
        sheet.setDefaultColumnWidth(20);

        //设置表头样式，表头居中
        CellStyle titleStyle = wb.createCellStyle();
        //设置字体
        Font titleFont = wb.createFont();
        titleFont.setFontHeightInPoints((short) this.fontSize);
        titleStyle.setFont(titleFont);

        //在第1行创建rows
        Row titleRow = sheet.createRow(0);
        //设置列头元素
        Cell cellHead;

        for (int i = 0; i < heardList.length; i++) {
            //背景填充色
            titleStyle.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.index);
            titleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            //边框
            titleStyle.setBorderLeft(BorderStyle.THIN);//左边框
            titleStyle.setBorderRight(BorderStyle.THIN);//右边框
            cellHead = titleRow.createCell(i);
            cellHead.setCellValue(heardList[i]);
            cellHead.setCellStyle(titleStyle);
        }

        //开始写入实体数据信息
        //设置数据样式
        CellStyle dataStyle = wb.createCellStyle();
        //设置字体
        Font dataFont = wb.createFont();
//        font.setFontHeightInPoints((short) this.fontSize);
        dataFont.setBold(false);
        dataStyle.setFont(dataFont);
        int count = 1;
        for (Map datum : data) {
            Row row = sheet.createRow(count);
            Cell cell;
            int len = heardKey.length;
            for (int j = 0; j < len; j++) {
                cell = row.createCell(j);
                cell.setCellStyle(dataStyle);
                Object valueObject = datum.get(heardKey[j]);
                String value;
                if (valueObject == null) {
                    valueObject = "";
                }
                if (valueObject instanceof String) {
                    //取出的数据是字符串直接赋值
                    value = (String) datum.get(heardKey[j]);
                } else if (valueObject instanceof Integer) {
                    //取出的数据是Integer
                    value = String.valueOf(((Integer) (valueObject)).floatValue());
                } else if (valueObject instanceof BigDecimal) {
                    //取出的数据是BigDecimal
                    value = String.valueOf(((BigDecimal) (valueObject)).floatValue());
                } else {
                    value = valueObject.toString();
                }
                cell.setCellValue(Objects.isNull(value) ? "" : value);
            }
            count++;
        }
        data.clear();

        //导出数据
        try (OutputStream os = response.getOutputStream()) {
            String fileName = URLEncoder.encode(this.title, StandardCharsets.UTF_8);
            //设置Http响应头告诉浏览器下载这个附件
            response.setHeader("Content-Disposition", "attachment;Filename=" + fileName + ".xlsx");
            wb.write(os);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new IOException("导出Excel出现严重异常，异常信息：" + ex.getMessage());
        } finally {
            wb.close();
        }
    }

    /**
     * 设置导出excel 的信息
     * 主要用到了反射，获取类中标注的ExcelProperty注解的字段，
     * 然后根据注解的index进行排序
     * @param maps
     * @return
     */
    public static ExcelExportUtil getExcelExportUtil(List<Map> maps, Field[] fields) {
        List<Field> fieldList = Arrays.stream(fields)
                .filter(field -> {
                    ExcelProperty annotation = field.getAnnotation(ExcelProperty.class);
                    if (annotation != null && annotation.index() > -1) {
                        field.setAccessible(true);
                        return true;
                    }
                    return false;
                }).sorted(Comparator.comparing(field -> {
                    int index = -1;
                    ExcelProperty annotation = field.getAnnotation(ExcelProperty.class);
                    if (annotation != null) {
                        index = annotation.index();
                    }
                    return index;
                })).collect(Collectors.toList());

        List<String> title = new ArrayList<>();
        List<String> properties = new ArrayList<>();
        fieldList.forEach(field -> {
            ExcelProperty annotation = field.getAnnotation(ExcelProperty.class);
            if (annotation != null) {
                final String[] value = annotation.value();
                String tit = value[0];
                title.add(tit);
                final String name = field.getName();
                properties.add(name);
            }
        });

        ExcelExportUtil excelExport = new ExcelExportUtil();
        excelExport.setData(maps);
        excelExport.setHeardKey(properties.toArray(new String[0]));
        excelExport.setFontSize(14);
        excelExport.setHeardList(title.toArray(new String[0]));
        return excelExport;
    }
}
