package cn.lxiaol.myoa.controller;

import cn.lxiaol.myoa.entity.db1.AoaScheduleList;
import cn.lxiaol.myoa.entity.db1.AoaUser;
import cn.lxiaol.myoa.service.AoaScheduleListService;
import cn.lxiaol.myoa.service.AoaTaskListService;
import cn.lxiaol.myoa.service.AoaUserLoginRecordService;
import cn.lxiaol.myoa.service.AoaUserService;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Api(value = "/", tags = {"用户记录相关接口"})
@Controller
@RequestMapping("/")
public class UserLogController {

    @Resource
    private AoaUserLoginRecordService aoaUserLoginRecordService;
    @Resource
    private AoaScheduleListService aoaScheduleListService;
    @Resource
    private AoaUserService aoaUserService;
    @Resource
    private AoaTaskListService aoaTaskListService;


    @ApiOperation(value = "日历的数据显示")
    @GetMapping("littlecalendar")
    public String littlecalendar(HttpSession session, HttpServletResponse response) throws IOException {
        long userid = Long.parseLong(session.getAttribute("userId").toString());

        List<String> list = new ArrayList<>();

        List<AoaScheduleList> dates = aoaScheduleListService.findstart(userid);
        for (AoaScheduleList scheduleList : dates) {
            String day = DateFormatUtils.format(scheduleList.getStartTime(), "yyyy-MM-dd");
            list.add(day);
        }

        String json = JSONObject.toJSONString(list);
        System.out.println("json=" + json);

        response.setHeader("Cache-Control", "no-cache");
        response.setContentType("text/json;charset=UTF-8");
        response.getWriter().write(json);

        return null;
    }

    @ApiOperation(value = "显示任务统计模块数据")
    @GetMapping("counttasknum")
    public String counttasknum(HttpServletResponse response) throws IOException {
        List<AoaUser> uList = aoaUserService.findAll();
        HashMap<String, Integer> hashMap = new HashMap<>();
        int i = 0;
        for (AoaUser user : uList) {
            Integer a = aoaTaskListService.ountfinish(7L, user.getUserId());

            if (0 < a) {
                hashMap.put(user.getUserName(), a);
                i++;
            }
        }
        ArrayList<Map.Entry<String, Integer>> entries = sortMap(hashMap);
        ArrayList<Map.Entry<String, Integer>> entries2 = new ArrayList<Map.Entry<String, Integer>>();

        if (entries.size() >= 5) {//获得前5个s
            for (int j = 0; j < 5; j++) {
                entries2.add(entries.get(j));
            }
        } else {
            entries2 = entries;
        }
        String json = JSONObject.toJSONString(entries2);
        System.out.println(json);
        response.setHeader("Cache-Control", "no-cache");
        response.setContentType("text/json;charset=UTF-8");
        response.getWriter().write(json);
        return null;
    }

    //给hashMap按照value排序

    private ArrayList<Map.Entry<String, Integer>> sortMap(Map<String, Integer> map) {
        ArrayList<Map.Entry<String, Integer>> entries = new ArrayList<>(map.entrySet());
        entries.sort((obj1, obj2) -> obj2.getValue() - obj1.getValue());
        return entries;
    }

    @ApiOperation(value = "显示本周的每天的记录")
    @GetMapping("countweeklogin")
    public String dfsa(HttpServletResponse response) throws IOException {
        Integer[] r = new Integer[7];
        Calendar calendar = Calendar.getInstance();

        setToFirstDay(calendar);

        for (int i = 0; i < 7; i++) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String format = sdf.format(calendar.getTime());
            r[i] = aoaUserLoginRecordService.countlog(format);
            calendar.add(Calendar.DATE, 1);
        }
        String json = JSONObject.toJSONString(r);
        System.out.println(json);
        response.setHeader("Cache-Control", "no-cache");
        response.setContentType("text/json;charset=UTF-8");
        response.getWriter().write(json);
        return null;
    }

    private static void setToFirstDay(Calendar calendar) {
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.add(Calendar.DATE, -1);
        }
    }
}
