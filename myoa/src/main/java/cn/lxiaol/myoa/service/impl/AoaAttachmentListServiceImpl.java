package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaAttachmentList;
import cn.lxiaol.myoa.mapper.db1.AoaAttachmentListMapper;
import cn.lxiaol.myoa.service.AoaAttachmentListService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-23
 */
@Service
public class AoaAttachmentListServiceImpl extends ServiceImpl<AoaAttachmentListMapper, AoaAttachmentList> implements AoaAttachmentListService {

}
