package com.atguigu.spring5;

/**
 * 1 创建接口 定义方法 编写jdk动态代理代码
 */
public interface UserDao {
    int add(int a, int b);
    String update(String id);


}
