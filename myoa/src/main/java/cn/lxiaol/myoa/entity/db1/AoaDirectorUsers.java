package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_director_users")
@ApiModel(value = "AoaDirectorUsers", description = "")
public class AoaDirectorUsers implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "director_users_id", type = IdType.AUTO)

    private Long directorUsersId;
    private String catelogName;
    private Integer isHandle;
    private Long directorId;
    private Long userId;
    private Long shareUserId;
    private Date sharetime;


}

