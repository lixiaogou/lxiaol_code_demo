package com.atguigu.spring5.testdemo;

import com.atguigu.spring5.Book;
import com.atguigu.spring5.Orders;
import com.atguigu.spring5.User;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring5 {

    @Test
    public void testAdd() {
        // 加载spring的配置文件
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean1.xml");
//        BeanFactory ac = new ClassPathXmlApplicationContext("bean1.xml");//不推荐使用
        //获取配置创建的对象
        User user = ac.getBean("user", User.class);
//        User user2 = ac.getBean("user", User.class);
//        System.out.println(user==user2);//true 默认单例
        user.add();
    }

    /**
     * set的方式进行属性注入
     */
    @Test
    public void testBook() {
        // 加载spring的配置文件
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean1.xml");
        //获取配置创建的对象
        Book user = ac.getBean("book", Book.class);
        user.listName();
    }

    /**
     * 有参构造的方式进行属性注入
     */
    @Test
    public void testOrders() {
        // 加载spring的配置文件
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean1.xml");
        //获取配置创建的对象
        Orders user = ac.getBean("orders", Orders.class);
        user.listName();
    }

    /**
     * set方式  命名空间注入
     */
    @Test
    public void testBook2() {
        // 加载spring的配置文件
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean1.xml");
        //获取配置创建的对象
        Book user = ac.getBean("book2", Book.class);
        user.listName();
    }

    /**
     * set方式  属性注入 null
     */
    @Test
    public void testBook3() {
        // 加载spring的配置文件
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean1.xml");
        //获取配置创建的对象
        Book user = ac.getBean("book3", Book.class);
        user.printAddress();
    }
    /**
     * set方式  属性注入 包含特殊符号的值   <![CDATA[文本内容]]>
     */
    @Test
    public void testBook4() {
        // 加载spring的配置文件
        ApplicationContext ac = new ClassPathXmlApplicationContext("bean1.xml");
        //获取配置创建的对象
        Book user = ac.getBean("book4", Book.class);
        user.printOcean();
    }

}
