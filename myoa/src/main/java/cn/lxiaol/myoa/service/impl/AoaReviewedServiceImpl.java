package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaReviewed;
import cn.lxiaol.myoa.mapper.db1.AoaReviewedMapper;
import cn.lxiaol.myoa.service.AoaReviewedService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaReviewedServiceImpl extends ServiceImpl<AoaReviewedMapper, AoaReviewed> implements AoaReviewedService {

}
