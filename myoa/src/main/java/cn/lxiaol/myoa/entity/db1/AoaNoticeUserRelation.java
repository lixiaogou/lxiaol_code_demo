package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_notice_user_relation")
@ApiModel(value = "AoaNoticeUserRelation", description = "")
public class AoaNoticeUserRelation implements Serializable {

    private static final long serialVersionUID = 1L;

    //用户与通知中间关联表主键id
    @TableId(value = "relatin_id", type = IdType.AUTO)
    private Long relatinId;

    //此条通知该用户是否已读 0否1是
    private Integer isRead = 0;

    //通知id
    private Long relatinNoticeId;

    //用户id
    private Long relatinUserId;

    public AoaNoticeUserRelation() {

    }

    public AoaNoticeUserRelation(Long relatinNoticeId, Long relatinUserId, Integer isRead) {
        this.relatinNoticeId = relatinNoticeId;
        this.relatinUserId = relatinUserId;
        this.isRead = isRead;
    }

}

