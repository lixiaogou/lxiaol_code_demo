package cn.lxiaol.myoa.service;

import cn.lxiaol.myoa.entity.db1.AoaSysMenu;
import cn.lxiaol.myoa.entity.db1.AoaUser;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaSysMenuService extends IService<AoaSysMenu> {

    void findMenuSys(HttpServletRequest req, AoaUser aoaUser);
}
