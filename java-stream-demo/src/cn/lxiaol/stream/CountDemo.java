package cn.lxiaol.stream;

import java.util.Arrays;
import java.util.List;

/**
 * 在stream中，针对流进行计算后得出结果，例如求和、求最值等，这样的操作被称为聚合操作。
 * 聚合操作在广义上包含了max、min、count等方法和reduce、collect。
 *
 * @author lxiaol
 * @date 2021年05月18日 17:35
 */
public class CountDemo {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(7, 6, 9);

        long count = list.stream().filter(x -> x > 6).count();
        System.out.println("count = " + count);
    }
}
