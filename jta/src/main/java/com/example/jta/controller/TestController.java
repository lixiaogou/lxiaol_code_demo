package com.example.jta.controller;

import com.example.jta.service.JtaService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author lxiaol
 * @date 2021年08月23日 15:22
 */
@RestController
@RequestMapping(value = "/test")
public class TestController {
    @Resource
    private JtaService jtaService;


    @GetMapping("/add")
    public String add() {
        jtaService.add();
        return "success";
    }
}
