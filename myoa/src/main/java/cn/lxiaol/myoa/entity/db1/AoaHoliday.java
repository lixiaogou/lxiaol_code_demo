package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_holiday")
@ApiModel(value = "AoaHoliday", description = "")
public class AoaHoliday implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "holiday_id", type = IdType.AUTO)

    private Long holidayId;
    private Integer leaveDays;
    private Long typeId;
    private Long proId;
    private String managerAdvice;
    private String personnelAdvice;


}

