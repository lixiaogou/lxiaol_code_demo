package cn.lxiaol.multipledb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 电子运单编号
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-09
 */
@Data
@Accessors(chain = true)
@TableName("common_waybill_num")
public class CommonWaybillNum implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)

    private Integer id;
    /**
     * 公司id
     */
    private String companyId;
    /**
     * 运单号
     */
    private String waybillNum;


}

