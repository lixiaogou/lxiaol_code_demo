package cn.lxiaol.multipledb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.lxiaol.multipledb.entity.CommonWaybillNum;

/**
 * <p>
 * 电子运单编号 服务类
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-09
 */
public interface ICommonWaybillNumService extends IService<CommonWaybillNum> {

}
