package com.example.jta.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jta.dao.Test2;
import com.example.jta.mapper.db2mapper.Test2Mapper;
import com.example.jta.service.Test2Service;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统用户 服务实现类
 * </p>
 *
 * @author lixiaolong
 * @since 2021-08-23
 */
@Service
public class Test2ServiceImpl extends ServiceImpl<Test2Mapper, Test2> implements Test2Service {

}
