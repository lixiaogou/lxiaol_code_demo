package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaMailReciver;
import cn.lxiaol.myoa.mapper.db1.AoaMailReciverMapper;
import cn.lxiaol.myoa.service.AoaMailReciverService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaMailReciverServiceImpl extends ServiceImpl<AoaMailReciverMapper, AoaMailReciver> implements AoaMailReciverService {

    @Resource
    private AoaMailReciverMapper aoaMailReciverMapper;

    @Override
    public List<AoaMailReciver> findByReadAndDelAndReciverId(Long userId) {
        return aoaMailReciverMapper.findByReadAndDelAndReciverId(userId);
    }
}
