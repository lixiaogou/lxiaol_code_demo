package cn.lxiaol.myoa.controller;

import cn.lxiaol.myoa.common.MapToList;
import cn.lxiaol.myoa.common.ResultVO;
import cn.lxiaol.myoa.common.utils.ResultVOUtil;
import cn.lxiaol.myoa.entity.db1.*;
import cn.lxiaol.myoa.enums.ResultEnum;
import cn.lxiaol.myoa.service.*;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Api(value = "/", tags = {"首页相关接口"})
@Controller
@RequestMapping("/")
public class InformManageController {


    @Resource
    private AoaNoticeListService aoaNoticeListService;
    @Resource
    private AoaTypeListService aoaTypeListService;
    @Resource
    private AoaStatusListService aoaStatusListService;
    @Resource
    private AoaUserService aoaUserService;
    @Resource
    private AoaNoticeUserRelationService aoaNoticeUserRelationService;


    /**
     * 通知管理面板
     *
     * @return
     */
    @ApiOperation(value = "通知管理面板")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "uid", value = "", required = true, dataType = "String"),
//			@ApiImplicitParam(name = "isFormal", value = "", required = true, dataType = "String")
//	})
    @GetMapping("infrommanage")
    public String inform(@RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                         @SessionAttribute("userId") Long userId, Model model) {
        PageInfo<AoaNoticeList> page = aoaNoticeListService.pageThis(pageNum, userId);

        List<AoaNoticeList> noticeList = page.getList();

        List<Map<String, Object>> list = aoaNoticeListService.fengZhuang(noticeList);
        model.addAttribute("list", list);
        model.addAttribute("page", page);
        //设置变量，需要load的url；
        model.addAttribute("url", "infrommanagepaging");
        return "inform/informmanage";
    }

    /**
     * 通知管理面板分页
     *
     * @return
     */
    @ApiOperation(value = "通知管理面板分页")
    @RequestMapping("infrommanagepaging")
    public String inform(@RequestParam(defaultValue = "1") int pageNum, @RequestParam(required = false) String baseKey,
                         @RequestParam(required = false) String type, @RequestParam(required = false) String status,
                         @RequestParam(required = false) String time, @RequestParam(required = false) String icon,
                         @SessionAttribute("userId") Long userId, Model model, HttpServletRequest req) {
        System.out.println("baseKey:" + baseKey);

        setSomething(baseKey, type, status, time, icon, model);
        PageInfo<AoaNoticeList> page = aoaNoticeListService.pageThis(pageNum, userId, baseKey, type, status, time);
        List<AoaNoticeList> noticeList = page.getList();
        List<Map<String, Object>> list = aoaNoticeListService.fengZhuang(noticeList);
        model.addAttribute("list", list);
        model.addAttribute("page", page);
        //设置变量，需要load的url；
        model.addAttribute("url", "infrommanagepaging");
        return "inform/informtable";
    }

    private void setSomething(String baseKey, String type, String status, String time, String icon, Model model) {
        if (!Strings.isBlank(icon)) {
            model.addAttribute("icon", icon);
            if (!Strings.isBlank(type)) {
                model.addAttribute("type", type);
                if ("1".equals(type)) {
                    model.addAttribute("sort", "&type=1&icon=" + icon);
                } else {
                    model.addAttribute("sort", "&type=0&icon=" + icon);
                }
            }
            if (!Strings.isBlank(status)) {
                model.addAttribute("status", status);
                if ("1".equals(status)) {
                    model.addAttribute("sort", "&status=1&icon=" + icon);
                } else {
                    model.addAttribute("sort", "&status=0&icon=" + icon);
                }
            }
            if (!Strings.isBlank(time)) {
                model.addAttribute("time", time);
                if ("1".equals(time)) {
                    model.addAttribute("sort", "&time=1&icon=" + icon);
                } else {
                    model.addAttribute("sort", "&time=0&icon=" + icon);
                }
            }
        }
        if (!Strings.isBlank(baseKey)) {
            model.addAttribute("baseKey", baseKey);
        }
    }


    /**
     * 编辑通知界面
     */
    @RequestMapping("informedit")
    public String infromEdit(HttpServletRequest req, HttpSession session, Model model) {
        session.removeAttribute("noticeId");

        List<AoaTypeList> typeList = aoaTypeListService.findByTypeModel("inform");
        List<AoaStatusList> statusList = aoaStatusListService.findByStatusModel("inform");

        if (Objects.nonNull(req.getAttribute("errormess"))) {
            req.setAttribute("errormess", req.getAttribute("errormess"));
        }
        if (Objects.nonNull(req.getAttribute("success"))) {
            req.setAttribute("success", "数据保存成功");
        }
        req.setAttribute("typeList", typeList);
        req.setAttribute("statusList", statusList);
        if (Objects.nonNull(req.getParameter("id"))) {
            Long noticeId = Long.parseLong(req.getParameter("id"));
            AoaNoticeList noticeList = aoaNoticeListService.getById(noticeId);
            model.addAttribute("noticeList", noticeList);
            model.addAttribute("typeName", aoaTypeListService.getById(noticeList.getTypeId()).getTypeName());
            model.addAttribute("statusName", aoaStatusListService.getById(noticeList.getStatusId()).getStatusName());
            session.setAttribute("noticeId", noticeId);
        }

        return "inform/informedit";
    }


    /**
     * 系统管理表单验证
     *
     * @param req
     * @param aoaNoticeList
     * @param br            后台校验表单数据，不通过则回填数据，显示错误信息；通过则直接执行业务，例如新增、编辑等；
     * @return
     */
    @RequestMapping("informcheck")
    public String informcheck(HttpServletRequest req, AoaNoticeList aoaNoticeList, BindingResult br) {
        HttpSession session = req.getSession();
        req.setAttribute("menuObj", aoaNoticeList);
        Long userId = Long.parseLong(session.getAttribute("userId") + "");
        aoaNoticeList.setUserId(userId);

        // 这里返回ResultVO对象，如果校验通过，ResultEnum.SUCCESS.getCode()返回的值为200；否则就是没有通过；
        ResultVO res = ResultVOUtil.hasErrors(br);
        // 校验失败
        if (!ResultEnum.SUCCESS.getCode().equals(res.getCode())) {
            List<Object> list = new MapToList<>().mapToList(res.getData());
            req.setAttribute("errormess", list.get(0).toString());
            // 代码调试阶段，下面是错误的相关信息；
            System.out.println("list错误的实体类信息：" + aoaNoticeList);
            System.out.println("list错误详情:" + list);
            System.out.println("list错误第一条:" + list.get(0));
            System.out.println("啊啊啊错误的信息——：" + list.get(0).toString());
            // 下面的info信息是打印出详细的信息
            log.info("getData:{}", res.getData());
            log.info("getCode:{}", res.getCode());
            log.info("getMsg:{}", res.getMsg());
        }
        // 校验通过，下面写自己的逻辑业务
        else {
            // 判断是否从编辑界面进来的，前面有"session.setAttribute("getId",getId);",在这里获取，并remove掉；
            if (Objects.nonNull(session.getAttribute("noticeId"))) {
                Long noticeId = (Long) session.getAttribute("noticeId"); // 获取进入编辑界面的menuID值
                AoaNoticeList inform = aoaNoticeListService.getById(noticeId);
                //类型 状态 标题 链接 描述 置顶
//                aoaNoticeList.setNoticeTime(inform.getNoticeTime());
//                aoaNoticeList.setNoticeId(noticeId);
                inform.setTypeId(aoaNoticeList.getTypeId()).setStatusId(aoaNoticeList.getStatusId())
                        .setTitle(aoaNoticeList.getTitle())
                        .setUrl(aoaNoticeList.getUrl())
                        .setContent(aoaNoticeList.getContent())
                        .setIsTop(aoaNoticeList.getIsTop())
                        .setModifyTime(new Date());

                session.removeAttribute("noticeId");
                aoaNoticeListService.saveOrUpdate(inform);
            } else {
                aoaNoticeList.setNoticeTime(new Date());
                aoaNoticeList.setUserId(userId);
                aoaNoticeListService.save(aoaNoticeList);
                List<AoaUser> userList = aoaUserService.findByFatherId(userId);

                for (AoaUser user : userList) {
                    AoaNoticeUserRelation aoaNoticeUserRelation =
                            new AoaNoticeUserRelation(aoaNoticeList.getNoticeId(), user.getUserId(), 0);
                    aoaNoticeUserRelationService.save(aoaNoticeUserRelation);
                }

            }
            // 执行业务代码
            System.out.println("此操作是正确的");
            req.setAttribute("success", "后台验证成功");
        }
        System.out.println("是否进入最后的实体类信息：" + aoaNoticeList);

        return "forward:/informedit";
    }

    /**
     * 详细通知显示
     */
    @RequestMapping("informshow")
    public String informShow(HttpServletRequest req, Model model) {
        Long noticeId = Long.parseLong(req.getParameter("id"));
        if (Objects.nonNull(req.getParameter("read"))) {
            if (("0").equals(req.getParameter("read"))) {//未读
                Long relationId = Long.parseLong(req.getParameter("relationid"));
                AoaNoticeUserRelation relation = aoaNoticeUserRelationService.getById(relationId);
                relation.setIsRead(1);
                aoaNoticeUserRelationService.saveOrUpdate(relation);
            }
        }
        AoaNoticeList notice = aoaNoticeListService.getById(noticeId);
        AoaUser user = aoaUserService.findOne(notice.getUserId());
        model.addAttribute("notice", notice);
        model.addAttribute("userName", user.getUserName());
        return "inform/informshow";
    }

    /**
     * 通知管理删除
     */
    @RequestMapping("infromdelete")
    public String infromDelete(HttpSession session, HttpServletRequest req) {
        Long noticeId = Long.parseLong(req.getParameter("id"));
        Long userId = Long.parseLong(session.getAttribute("userId") + "");
        AoaNoticeList notice = aoaNoticeListService.getById(noticeId);

        if (!Objects.equals(userId, notice.getUserId())) {
            System.out.println("权限不匹配，不能删除");
            return "redirect:/notlimit";
        }

        System.out.println(noticeId);
        if (!aoaNoticeListService.removeById(noticeId)) {
            return "redirect:/error";
        }

        return "redirect:/infrommanage";
    }

}
