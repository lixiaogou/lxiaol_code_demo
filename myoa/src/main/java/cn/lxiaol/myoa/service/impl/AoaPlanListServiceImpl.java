package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaPlanList;
import cn.lxiaol.myoa.mapper.db1.AoaPlanListMapper;
import cn.lxiaol.myoa.service.AoaPlanListService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaPlanListServiceImpl extends ServiceImpl<AoaPlanListMapper, AoaPlanList> implements AoaPlanListService {

    @Override
    public List<AoaPlanList> findByUserlimit(Long userId) {
        IPage<AoaPlanList> iPage = new Page<>(1, 2);
        LambdaQueryWrapper<AoaPlanList> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaPlanList::getPlanUserId, userId);
        qw.orderByDesc(AoaPlanList::getCreateTime, AoaPlanList::getEndTime);
        IPage<AoaPlanList> page = this.page(iPage, qw);
        return page.getRecords();
    }
}
