package cn.lxiaol.service;


import com.baomidou.mybatisplus.extension.service.IService;
import cn.lxiaol.dao.User;


/**
 * @author lixiaolong
 */
public interface UserService extends IService<User> {

}
