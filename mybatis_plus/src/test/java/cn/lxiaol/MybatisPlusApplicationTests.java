package cn.lxiaol;

import cn.lxiaol.dao.User;
import cn.lxiaol.mapper.UserMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
class MybatisPlusApplicationTests {
    // 继承了BaseMapper，所有的方法都来自己父类
    // 我们也可以编写自己的扩展方法！
    @Autowired
    UserMapper userMapper;

    @Test
    void contextLoads() {
    }


    @Test
    void testSelectList() {
        // 参数是一个 Wrapper ，条件构造器，这里我们先不用Wrapper 先使用null
        // 查询全部用户
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }

    // 测试插入
    @Test
    void testInsert() {
        User user = new User();
        user.setAge(20);
        user.setEmail("756078252@qq.com");
        user.setName("熊21 ");
        int result = userMapper.insert(user);
        System.out.println(result); // 受影响的行数
        System.out.println(user); // 发现，id会自动回填
    }

    // 测试更新
    @Test
    public void testUpdate() {
        User user = new User();
        // 通过条件自动拼接动态sql
        user.setId(1254396570402791443L);
        user.setName("java成神之路");
        user.setAge(18);
        // 注意：updateById 但是参数是一个 对象！
        int i = userMapper.updateById(user);
        System.out.println(i);
    }

    // 测试乐观锁成功！
    @Test
    public void testOptimisticLocker() {
        // 1、查询用户信息
        User user = userMapper.selectById(1L);
// 2、修改用户信息
        user.setName("kuangshen");
        user.setEmail("1223121@qq.com");
// 3、执行更新操作
        userMapper.updateById(user);
    }

    // 测试乐观锁失败！多线程下
    @Test
    public void testOptimisticLocker2() {
// 线程 1
        User user = userMapper.selectById(1L);
        user.setName("kuangshen111");
        user.setEmail("14141@qq.com");
// 模拟另外一个线程执行了插队操作
        User user2 = userMapper.selectById(1L);
        user2.setName("kuangshen222");
        user2.setEmail("14141@qq.com");
        userMapper.updateById(user2);
// 自旋锁来多次尝试提交！
        userMapper.updateById(user); // 如果没有乐观锁就会覆盖插队线程的值！
    }

    // 测试分页查询
    @Test
    public void testPage() {
// 参数一：当前页
// 参数二：页面大小
// 使用了分页插件之后，所有的分页操作也变得简单的！
        Page<User> page = new Page<>(2, 5);
        userMapper.selectPage(page, null);
        page.getRecords().forEach(System.out::println);
        System.out.println(page.getTotal());
    }


    // 测试删除
    @Test
    public void testDeleteById() {
        userMapper.deleteById(1240620674645544965L);
    }

    // 通过id批量删除
    @Test
    public void testDeleteBatchId() {
        userMapper.deleteBatchIds(Arrays.asList(1240620674645544961L, 1240620674645544962L));
    }

    // 通过map删除
    @Test
    public void testDeleteMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", "狂神说Java");
        userMapper.deleteByMap(map);
    }

    // 测试性能分析 慢sql 查询
    @Test
    void testPerformance() {
// 参数是一个 Wrapper ，条件构造器，这里我们先不用 null
// 查询全部用户
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }

}
