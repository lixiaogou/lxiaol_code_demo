package cn.lxiaol.duoshujuyuan.controller;

import cn.lxiaol.duoshujuyuan.service.TestService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/test")
public class TestController {
    private TestService testService;

    @GetMapping(value = "/add")
    public ResponseEntity createUserWithAnnotation() {
        testService.createUserWithAnnotation();
        return ResponseEntity.status(HttpStatus.OK).body("success");
    }
}