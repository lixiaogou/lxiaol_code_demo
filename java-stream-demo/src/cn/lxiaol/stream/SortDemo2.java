package cn.lxiaol.stream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lxiaol
 * @date 2021年05月19日 14:52
 */
public class SortDemo2 {

    public static void main(String[] args) {
        String[] strArr = {"abc", "m", "M", "acd"};
        List<String> list = Arrays.asList(strArr);
        // 1、按长度自然排序，即长度从小到大
        List<String> collect = list.stream().sorted(Comparator.comparing(String::length)).collect(Collectors.toList());
        System.out.println("collect = " + collect);

        // 2、按长度倒序，即长度从大到小
        List<String> collect1 = list.stream().sorted(Comparator.comparing(String::length).reversed()).collect(Collectors.toList());
        System.out.println("collect1 = " + collect1);

        // 3、首字母自然排序
        List<String> collect2 = list.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
        System.out.println("collect2 = " + collect2);

        // 4、首字母倒序
        List<String> collect3 = list.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        System.out.println("collect3 = " + collect3);

        // 5、先按照首字母排序 之后按照String的长度排序
        List<String> collect4 = list.stream().sorted(Comparator.comparing(x -> ((String) x).charAt(0)).thenComparing(x -> ((String) x).length())).collect(Collectors.toList());
        System.out.println("collect4 = " + collect4);

    }

}
