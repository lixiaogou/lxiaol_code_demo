package cn.lxiaol.myoa.controller;

import cn.lxiaol.myoa.common.StringtoDate;
import cn.lxiaol.myoa.entity.db1.AoaAttendsList;
import cn.lxiaol.myoa.entity.db1.AoaStatusList;
import cn.lxiaol.myoa.entity.db1.AoaTypeList;
import cn.lxiaol.myoa.entity.db1.AoaUser;
import cn.lxiaol.myoa.service.AoaAttendsListService;
import cn.lxiaol.myoa.service.AoaStatusListService;
import cn.lxiaol.myoa.service.AoaTypeListService;
import cn.lxiaol.myoa.service.AoaUserService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@Slf4j
@Api(value = "/", tags = {"考勤相关接口"})
@Controller
@RequestMapping("/")
public class AttendceController {


    @Autowired
    AoaAttendsListService aoaAttendsListService;

    @Autowired
    AoaUserService aoaUserService;
    @Autowired
    AoaTypeListService aoaTypeListService;
    @Autowired
    AoaStatusListService aoaStatusListService;

    // 考勤 前面的签到
    @GetMapping("singin")
    public String singin(HttpSession session, Model model, HttpServletRequest request)
            throws InterruptedException, UnknownHostException {

        // 格式转化导入
        DefaultConversionService service = new DefaultConversionService();
        // 时间规范
        String start = "08:00:00", end = "17:00:00";
        service.addConverter(new StringtoDate());

        //首先获取ip
        InetAddress ia = InetAddress.getLocalHost();
        String attendip = ia.getHostAddress();

        AoaAttendsList attends;
        Long userId = Long.parseLong(session.getAttribute("userId") + "");
        AoaUser user = aoaUserService.findOne(userId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String nowdate = sdf.format(date);
        // 星期 判断该日期是星期几
        SimpleDateFormat sdf3 = new SimpleDateFormat("EEEE");
        // 截取时分
        SimpleDateFormat sdf4 = new SimpleDateFormat("HH:mm");
        // 截取时分秒
        SimpleDateFormat sdf5 = new SimpleDateFormat("HH:mm:ss");

        // 一周当中的星期几
        String weekofday = sdf3.format(date);
        // 时分
        String hourmin = sdf4.format(date);
        // 时分秒
        String hourminsec = sdf5.format(date);
        System.out.println("星期=" + weekofday + "时分=" + hourmin + "时分秒=" + hourminsec);


        if (hourminsec.compareTo(end) > 0) {
            // 在17之后签到无效
            System.out.println("----不能签到");
            model.addAttribute("error", "1");
        }

        if (hourminsec.compareTo("05:00:00") < 0) {
            //在凌晨5点之前不能签到
            System.out.println("----不能签到");
            model.addAttribute("error", "2");

        } else if ((hourminsec.compareTo("05:00:00") > 0) && (hourminsec.compareTo(end) < 0)) {

            String success = "";            // 状态默认是正常
            long typeId, statusId = 10L;

            //5点到17点可进行签到
            // 明确一点就是一个用户一天只能产生两条记录
            // 查找用户当天的所有记录
            Integer count = aoaAttendsListService.countrecord(nowdate, userId);
            if (0 == count) {
                // 没有找到当天的记录就表示此次点击是上班 上班id8
                typeId = 8;
                //end点之前
                if (hourminsec.compareTo(end) < 0) {
                    success = "1";//正常上班

                    // 上班就只有迟到和正常 迟于规定时间 迟到id11
                    if (hourminsec.compareTo(start) > 0) {
                        statusId = 11;
                        success = "2";//迟到
                    }
                    attends = new AoaAttendsList(typeId, statusId, date, hourmin, weekofday, attendip, user.getUserId());
                    boolean boo = aoaAttendsListService.save(attends);
                    if (boo) {
                        model.addAttribute("success", success);
                    }
                }
            }

            if (1 == count) {
                // 找到当天的一条记录就表示此次点击是下班 下班id9
                typeId = 9;
                //end点之前
                if (hourminsec.compareTo(end) < 0) {
                    // 下班就只有早退和正常 早于规定时间 早退id12
                    statusId = 12;
                    success = "3";//早退
                } else {
                    success = "4";//下班正常
                }

                attends = new AoaAttendsList(typeId, statusId, date, hourmin, weekofday, attendip, user.getUserId());
                boolean boo = aoaAttendsListService.save(attends);
                if (boo) {
                    model.addAttribute("success", success);
                }
            }


            if (count >= 2) {
                // 已经是下班的状态了 大于2就是修改考勤时间了
                // 下班id9
                if (hourminsec.compareTo(end) < 0) {
                    // 下班就只有早退和正常 早于规定时间 早退id12
                    statusId = 12;
                    success = "3";//早退
                } else {
                    success = "4";//下班正常
                }

                Long aid = aoaAttendsListService.findoffworkid(nowdate, userId);
                AoaAttendsList attends2 = aoaAttendsListService.getById(aid);
                attends2.setAttendsIp(attendip);
                attends2.setAttendsTime(date).setAttendHmtime(hourmin).setStatusId(statusId);
                boolean boo = aoaAttendsListService.saveOrUpdate(attends2);
                if (boo) {
                    model.addAttribute("success", success);
                }

//                aoaAttendsListService.updatetime(date, hourmin, statusId, aid);
//                AoaAttendsList aList = aoaAttendsListService.findlastest(nowdate, userId);
            }
        }

        // 显示用户当天最新的记录
        AoaAttendsList aList = aoaAttendsListService.findlastest(nowdate, userId);
        if (Objects.nonNull(aList)) {
            String type = aoaTypeListService.findname(aList.getTypeId());
            model.addAttribute("type", type);
        }
        model.addAttribute("alist", aList);

        return "systemcontrol/signin";
    }


    // 考情列表 给单个用户使用
    @GetMapping(value = "attendcelist")
    public String attendcelist(HttpServletRequest request, Model model, HttpSession session,
                               @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                               @RequestParam(required = false) String baseKey,
                               @RequestParam(required = false) String type,
                               @RequestParam(required = false) String status,
                               @RequestParam(required = false) String time,
                               @RequestParam(required = false) String icon) {

        signsortpaging(request, model, session, pageNum, null, type, status, time, icon);
        return "attendce/attendcelist";
    }


    /**
     * 考勤列表条件查询
     *
     * @author lxiaol
     * @date 2021/4/29 14:18
     */
    @ApiOperation(value = "考勤列表条件查询")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "uid", value = "", required = true, dataType = "String"),
//			@ApiImplicitParam(name = "isFormal", value = "", required = true, dataType = "String")
//	})
    @GetMapping(value = "attendcelisttable")
    public String attendcelisttable(HttpServletRequest request, Model model, HttpSession session,
                                    @RequestParam(defaultValue = "1") int pageNum,
                                    @RequestParam(required = false) String baseKey,
                                    @RequestParam(required = false) String type,
                                    @RequestParam(required = false) String status,
                                    @RequestParam(required = false) String time,
                                    @RequestParam(required = false) String icon) {
        signsortpaging(request, model, session, pageNum, baseKey, type, status, time, icon);
        return "attendce/attendcelisttable";
    }


    /**
     * 考勤管理某个管理员下面的所有员工的信息
     *
     * @author lxiaol
     * @date 2021/4/29 14:18
     */
    @ApiOperation(value = "考勤管理某个管理员下面的所有员工的信息")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "uid", value = "", required = true, dataType = "String"),
//			@ApiImplicitParam(name = "isFormal", value = "", required = true, dataType = "String")
//	})
    @RequestMapping("attendceatt")
    public String attendceatt(HttpServletRequest request, HttpSession session,
                              @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
                              @RequestParam(required = false) String baseKey,
                              @RequestParam(required = false) String type,
                              @RequestParam(required = false) String status,
                              @RequestParam(required = false) String time,
                              @RequestParam(required = false) String icon, Model model) {
        allsortpaging(request, session, pageNum, baseKey, type, status, time, icon, model);
        return "attendce/attendceview";
    }

    // 考勤管理某个管理员下面的所有员工的信息分頁分页
    @ApiOperation(value = "考勤管理某个管理员下面的所有员工的信息分頁分页")
    @RequestMapping("attendcetable")
    public String attendcetable(HttpServletRequest request, HttpSession session,
                                @RequestParam(value = "page", defaultValue = "0") int page,
                                @RequestParam(value = "baseKey", required = false) String baseKey,
                                @RequestParam(value = "type", required = false) String type,
                                @RequestParam(value = "status", required = false) String status,
                                @RequestParam(value = "time", required = false) String time,
                                @RequestParam(value = "icon", required = false) String icon, Model model) {
        allsortpaging(request, session, page, baseKey, type, status, time, icon, model);
        return "attendce/attendcetable";
    }

    // 删除
    @ApiOperation(value = "删除")
    @RequestMapping("attdelete")
    public String attdelete(HttpServletRequest request, HttpSession session) {
        long aid = Long.parseLong(request.getParameter("aid"));
        aoaAttendsListService.removeById(aid);
        return "redirect:/attendceatt";
    }

    // 月报表
    @ApiOperation(value = "月报表")
    @RequestMapping("attendcemonth")
    public String attendcemonth(HttpServletRequest request, Model model, HttpSession session,
                                @RequestParam(value = "page", defaultValue = "0") int page,
                                @RequestParam(value = "baseKey", required = false) String baseKey) {
        monthtablepaging(request, model, session, page, baseKey);
        return "attendce/monthtable";
    }


    //单个用户的排序和分页
    private void signsortpaging(HttpServletRequest request, Model model, HttpSession session, int pageNum, String baseKey,
                                String type, String status, String time, String icon) {
        Long userid = Long.valueOf(session.getAttribute("userId") + "");
//        setSomething(baseKey, type, status, time, icon, model);
        PageInfo<AoaAttendsList> pageInfo = aoaAttendsListService.paging(pageNum, baseKey, Collections.singletonList(userid), type, status, time);
//        typestatus(request);

        List<AoaTypeList> typelist = aoaTypeListService.findByTypeModel("aoa_attends_list");
        List<AoaStatusList> statuslist = aoaStatusListService.findByStatusModel("aoa_attends_list");

        request.setAttribute("typelist", typelist);
        request.setAttribute("statuslist", statuslist);
        request.setAttribute("page", pageInfo);
        request.setAttribute("alist", pageInfo.getList());
        request.setAttribute("url", "attendcelisttable");

    }

    //单个用户的排序和分页
    public void setSomething(String baseKey, Object type, Object status, Object time, Object icon, Model model) {
        if (!StringUtils.isEmpty(icon)) {
            model.addAttribute("icon", icon);
            if (!StringUtils.isEmpty(type)) {
                model.addAttribute("type", type);
                if ("1".equals(type)) {
                    model.addAttribute("sort", "&type=1&icon=" + icon);
                } else {
                    model.addAttribute("sort", "&type=0&icon=" + icon);
                }
            }
            if (!StringUtils.isEmpty(status)) {
                model.addAttribute("status", status);
                if ("1".equals(status)) {
                    model.addAttribute("sort", "&status=1&icon=" + icon);
                } else {
                    model.addAttribute("sort", "&status=0&icon=" + icon);
                }
            }
            if (!StringUtils.isEmpty(time)) {
                model.addAttribute("time", time);
                if ("1".equals(time)) {
                    model.addAttribute("sort", "&time=1&icon=" + icon);
                } else {
                    model.addAttribute("sort", "&time=0&icon=" + icon);
                }
            }
        }
        if (!StringUtils.isEmpty(baseKey)) {
            model.addAttribute("sort", "&baseKey=" + baseKey);
        }
    }

    // 状态类型方法
    private void typestatus(HttpServletRequest request) {
        List<AoaTypeList> type = aoaTypeListService.findByTypeModel("aoa_attends_list");
        List<AoaStatusList> status = aoaStatusListService.findByStatusModel("aoa_attends_list");
        request.setAttribute("typelist", type);
        request.setAttribute("statuslist", status);
    }

    //该管理下面的所有用户
    private void allsortpaging(HttpServletRequest request, HttpSession session, int pageNum, String baseKey, String type,
                               String status, String time, String icon, Model model) {
        setSomething(baseKey, type, status, time, icon, model);
        Long userId = Long.parseLong(session.getAttribute("userId") + "");
        List<AoaUser> users = aoaUserService.findByFatherId(userId);

        List<Long> ids = users.stream().map(AoaUser::getUserId).collect(Collectors.toList());
        if (ids.size() == 0) {
            ids.add(0L);
        }

        typestatus(request);
        PageInfo<AoaAttendsList> page2 = aoaAttendsListService.paging(pageNum, baseKey, ids, type, status, time);
        request.setAttribute("alist", page2.getList());
        request.setAttribute("page", page2);
        request.setAttribute("url", "attendcetable");
    }


    //月报表
    private void monthtablepaging(HttpServletRequest request, Model model, HttpSession session, int pageNum,
                                  String baseKey) {
        Long userId = Long.parseLong(session.getAttribute("userId") + "");
        PageInfo<AoaUser> userspage = aoaUserService.findmyemployuser(pageNum, baseKey, userId);
        List<AoaUser> list = userspage.getList();


        List<Long> ids = list.stream().map(AoaUser::getUserId).collect(Collectors.toList());
        if (ids.size() == 0) {
            ids.add(0L);
        }

        String month = request.getParameter("month");
        String month_ = "";

        if (Objects.isNull(month)) {
            month = month_;
        }

        Map<String, List<Integer>> uMap = new HashMap<>();
        List<Integer> result = null;

        Integer offnum, toworknum;
        for (AoaUser user : list) {
            result = new ArrayList<>();
            //当月该用户下班次数
            offnum = aoaAttendsListService.countoffwork(month, user.getUserId());
            //当月该用户上班次数
            toworknum = aoaAttendsListService.counttowork(month, user.getUserId());
            for (long statusId = 10; statusId < 13; statusId++) {
                //这里面记录了正常迟到早退等状态
                if (statusId == 12) {
                    result.add(aoaAttendsListService.countnum(month, statusId, user.getUserId()) + toworknum - offnum);
                } else {
                    result.add(aoaAttendsListService.countnum(month, statusId, user.getUserId()));
                }
            }
            //添加请假和出差的记录//应该是查找 使用sql的sum（）函数来统计出差和请假的次数
//            System.out.println("请假天数" + aoaAttendsListService.countothernum(month, 46L, user.getUserId()));

            if (aoaAttendsListService.countothernum(month, 46L, user.getUserId()) != null) {
                result.add(aoaAttendsListService.countothernum(month, 46L, user.getUserId()));
            } else {
                result.add(0);
            }
            if (aoaAttendsListService.countothernum(month, 47L, user.getUserId()) != null) {
                result.add(aoaAttendsListService.countothernum(month, 47L, user.getUserId()));
            } else {
                result.add(0);
            }
            //这里记录了旷工的次数 还有请假天数没有记录 旷工次数=30-8-请假次数-某天签到次数
            //这里还有请假天数没有写
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
            String dateMonth = sdf.format(date);
            if (month.compareTo(dateMonth) >= 0) {
                result.add(0);
            } else {
                result.add(30 - 8 - offnum);
            }

            uMap.put(user.getUserName(), result);
        }
        model.addAttribute("uMap", uMap);
        model.addAttribute("ulist", list);
        model.addAttribute("page", userspage);
        model.addAttribute("url", "realmonthtable");
    }

}
