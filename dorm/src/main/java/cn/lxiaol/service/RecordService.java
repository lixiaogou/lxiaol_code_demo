package cn.lxiaol.service;

import cn.lxiaol.vo.RecordVO;

import java.util.Map;


public interface RecordService {

    Map queryByPage(RecordVO vo, int start, int pageSize);
    Map del(String id);
    Map add(RecordVO RecordVO);
    Map mod(RecordVO RecordVO);
    Map queryByAjax(RecordVO RecordVO);
   RecordVO queryByAjaxWithNo(String no);
   RecordVO queryByAjaxWithId(String id);
}
