package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_love_user")
@ApiModel(value = "AoaLoveUser", description = "")
public class AoaLoveUser implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long replyId;
    private Long userId;


}

