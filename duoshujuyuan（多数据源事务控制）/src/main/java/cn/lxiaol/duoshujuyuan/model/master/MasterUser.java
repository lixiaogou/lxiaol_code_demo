package cn.lxiaol.duoshujuyuan.model.master;

import lombok.Data;

import java.io.Serializable;

@Data
public class MasterUser implements Serializable {

    private String username;
}