package cn.lxiaol.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lxiaol
 * @date 2021年05月19日 14:04
 */
public class JoiningDemo {
    public static void main(String[] args) {
        List<Person> list = new ArrayList<Person>();
        list.add(new Person("Tom", 8900));
        list.add(new Person("Jack", 7000));
        list.add(new Person("Lily", 9000));

        String nameJoin1 = list.stream().map(Person::getName).collect(Collectors.joining());
        System.out.println("nameJoin1 = " + nameJoin1);


        String nameJoin2 = list.stream().map(Person::getName).collect(Collectors.joining("-"));
        System.out.println("nameJoin2 = " + nameJoin2);


        String nameJoin3 = list.stream().map(Person::getName).collect(Collectors.joining("--", "姓名：", "。"));
        System.out.println("nameJoin3 = " + nameJoin3);


    }
}
