package cn.lxiaol.multipledb;

import cn.lxiaol.multipledb.enums.DBTypeEnum;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.ArrayList;

public class MybatisGenerator {

    public static void main(String[] args) {
        String db = "db1";

        String url = "";
        String username = "";
        String password = "";
        String tableName = "tms_company_list";
        String mapperPackage = "";
        if(DBTypeEnum.db1.getValue().equals(db)){
            mapperPackage = "mapper.db1";
            url = "jdbc:mysql://localhost:3306/tms_yiguan?autoReconnect=true&serverTimezone=GMT%2B8&rewriteBatchedStatements=true&useSSL=false&useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true";
            username = "root";
            password = "root";
        }else if(DBTypeEnum.db2.getValue().equals(db)){
            mapperPackage = "mapper.db2";
            url = "jdbc:mysql://localhost:3306/sq_yundan?autoReconnect=true&serverTimezone=GMT%2B8&rewriteBatchedStatements=true&useSSL=false&useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true";
            username = "root";
            password = "root";
        }

        // 需要引入模板引擎依赖

        // 需要构建一个 代码自动生成器 对象
        AutoGenerator mpg = new AutoGenerator();
//        配置策略
        // 1、全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");// 获取项目路径
        gc.setOutputDir(projectPath + "/src/main/java");// 输出路径
        gc.setAuthor("lixiaolong");//作者
        gc.setOpen(false);// 是否打开资源管理器
        gc.setFileOverride(true); // 是否覆盖
        gc.setEnableCache(false); // 默认false,是否开启二级缓存
        gc.setAuthor("lixiaolong"); // 作者
//        gc.setServiceName("%sService");
        gc.setControllerName("%sController");
        gc.setServiceName("I%sService"); // 去Service文件的I前缀
        gc.setServiceImplName("%sServiceImpl");
        gc.setMapperName("I%sMapper");
        gc.setXmlName("%sMapper");
        gc.setEntityName("%s");
        gc.setIdType(IdType.ID_WORKER);// 若数据库时自增,则此处被数据库的自增覆盖
        gc.setDateType(DateType.ONLY_DATE);//日期格式
//        gc.setBaseResultMap(false); // 默认false
        gc.setSwagger2(true); // 是否生成swagger文档 前提得配置swagger依赖
        mpg.setGlobalConfig(gc);

        //2、设置数据源
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(url);
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername(username);
        dsc.setPassword(password);
        dsc.setDbType(DbType.MYSQL);
        mpg.setDataSource(dsc);

        //3、包的配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("cn.lxiaol");// 生成的文件所在的包名
        pc.setModuleName("migration"); //模块名
        pc.setEntity("entity");//实体类所在的包名
        pc.setMapper(mapperPackage);// mapper包名
        pc.setService("service");// service包名
        pc.setController("controller");// controller包名
        mpg.setPackageInfo(pc);

        //4、策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setInclude(tableName); // 设置要映射的表名
        strategy.setNaming(NamingStrategy.underline_to_camel);// 驼峰命名法
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true); // 自动lombok；
        strategy.setLogicDeleteFieldName("deleted");// 逻辑删除
        // 自动填充配置
        TableFill gmtCreate = new TableFill("gmt_create", FieldFill.INSERT);
        TableFill gmtModified = new TableFill("gmt_modified", FieldFill.INSERT_UPDATE);
        ArrayList<TableFill> tableFills = new ArrayList<>();
        tableFills.add(gmtCreate);
        tableFills.add(gmtModified);
        strategy.setTableFillList(tableFills);// 设置自动填充
        strategy.setVersionFieldName("version");// 设置乐观锁
        strategy.setRestControllerStyle(true);// restful风格 userCopy1
//        strategy.setControllerMappingHyphenStyle(true); //localhost:8080/user-copy-1
//        设置策略
        mpg.setStrategy(strategy);

        // 6、配置模板
        TemplateConfig tc = new TemplateConfig();
        // templates/entity.java 模板路径配置，默认在templates目录下，.vm 后缀不用加
        tc.setEntity("templates/entity.java");//使用自定义模板生成实体类
        tc.setXml("");
        mpg.setTemplate(tc);


        mpg.execute(); //执行
    }
}