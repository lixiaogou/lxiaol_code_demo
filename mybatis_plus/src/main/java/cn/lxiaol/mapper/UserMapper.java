package cn.lxiaol.mapper;

import cn.lxiaol.dao.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 在对应的Mapper上面继承BaseMapper 很多方法都来自父类 我们也可以编写自己的扩展方法！
 *
 * @author lxiaol
 * @date 2021/4/15 21:40
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}
