package com.lxiaol.redisdemo;

import com.lxiaol.redisdemo.utils.JedisPoolUtil;
import com.lxiaol.redisdemo.utils.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.annotation.Resource;
import java.util.Set;

@SpringBootTest
class RedisdemoApplicationTests {

    @Resource
    private RedisUtil redisUtil;

    @Test
    void contextLoads() {
    }

    @Test
    void testJedis() {
        JedisPool jedisPool = JedisPoolUtil.getJedisPoolInstance(0);
        JedisPool jedisPool2 = JedisPoolUtil.getJedisPoolInstance(1);
        System.out.println(jedisPool == jedisPool2);//true 表示是单例

        Jedis jedis = jedisPool.getResource();//获取jedis
        jedis.set("kww", "v44");
        try {
            Thread.sleep(500);
            String s = jedis.get("kww");
            System.out.println(s);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void testRedis() {
        Set<String> keys = redisUtil.keys("*");
        for (String s : keys) {
            System.out.println(s);
        }
    }

}
