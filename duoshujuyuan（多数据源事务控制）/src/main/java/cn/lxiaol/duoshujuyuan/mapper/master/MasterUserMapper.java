package cn.lxiaol.duoshujuyuan.mapper.master;

import cn.lxiaol.duoshujuyuan.model.master.MasterUser;

public interface MasterUserMapper {

    int insertSelective(MasterUser record);
}