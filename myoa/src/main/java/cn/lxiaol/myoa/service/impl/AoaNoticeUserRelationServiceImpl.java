package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaNoticeUserRelation;
import cn.lxiaol.myoa.mapper.db1.AoaNoticeUserRelationMapper;
import cn.lxiaol.myoa.service.AoaNoticeUserRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaNoticeUserRelationServiceImpl extends ServiceImpl<AoaNoticeUserRelationMapper, AoaNoticeUserRelation> implements AoaNoticeUserRelationService {

    @Resource
    private AoaNoticeUserRelationMapper aoaNoticeUserRelationMapper;

    @Override
    public List<AoaNoticeUserRelation> findByReadAndUserId(Long userId) {
        return aoaNoticeUserRelationMapper.findByReadAndUserId(userId);
    }

    @Override
    public List<AoaNoticeUserRelation> findByNoticeId(Long noticeId) {
        return aoaNoticeUserRelationMapper.findByNoticeId(noticeId);
    }
}
