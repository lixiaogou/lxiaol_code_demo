package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_receiver_note")
@ApiModel(value = "AoaReceiverNote", description = "")
public class AoaReceiverNote implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long noteId;
    private Long userId;
    @TableId(value = "id", type = IdType.AUTO)

    private Integer id;


}

