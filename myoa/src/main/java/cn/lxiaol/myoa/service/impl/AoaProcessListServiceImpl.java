package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaProcessList;
import cn.lxiaol.myoa.mapper.db1.AoaProcessListMapper;
import cn.lxiaol.myoa.service.AoaProcessListService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaProcessListServiceImpl extends ServiceImpl<AoaProcessListMapper, AoaProcessList> implements AoaProcessListService {

    @Override
    public List<AoaProcessList> findlastthree(Long userId) {
//select * from aoa_process_list  where aoa_process_list.process_user_id=?1
// ORDER BY aoa_process_list.apply_time DESC LIMIT 0,3
        IPage<AoaProcessList> iPage = new Page<>(1, 3);

        LambdaQueryWrapper<AoaProcessList> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaProcessList::getProcessUserId, userId);
        qw.orderByDesc(AoaProcessList::getApplyTime);
        IPage<AoaProcessList> page = page(iPage, qw);
        return page.getRecords();

    }
}
