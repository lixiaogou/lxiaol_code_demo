package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_in_mail_list")
@ApiModel(value = "AoaInMailList", description = "")
public class AoaInMailList implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "mail_id", type = IdType.AUTO)

    private Long mailId;
    private String mailContent;
    private Date mailCreateTime;
    private Long mailFileId;
    private String mailTitle;
    private Long mailType;
    private Long mailInPushUserId;
    private String inReceiver;
    private Long mailStatusId;
    private Long mailNumberId;
    private Integer mailDel;
    private Integer mailPush;
    private Integer mailStar;


}

