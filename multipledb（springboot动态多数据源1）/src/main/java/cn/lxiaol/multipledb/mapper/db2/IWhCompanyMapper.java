package cn.lxiaol.multipledb.mapper.db2;

import cn.lxiaol.multipledb.entity.WhCompany;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-09
 */
public interface IWhCompanyMapper extends BaseMapper<WhCompany> {

}
