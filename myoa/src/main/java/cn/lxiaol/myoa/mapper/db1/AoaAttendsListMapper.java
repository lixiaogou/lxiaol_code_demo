package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaAttendsList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaAttendsListMapper extends BaseMapper<AoaAttendsList> {
    @Select("<script>" +
            "SELECT *  FROM aoa_attends_list WHERE attends_user_id =#{userId} " +
            "</script>")
    Set<AoaAttendsList> findSetByUserId(Long userId);


    @Select("<script>" +
            "SELECT * FROM aoa_attends_list a " +
            "WHERE DATE_FORMAT(a.attends_time,'%Y-%m-%d') = #{nowdate} " +
            "AND a.attends_user_id=#{userId}  ORDER  BY a.attends_time DESC  LIMIT 1 " +
            "</script>")
    AoaAttendsList findlastest(@Param("nowdate") String nowdate, @Param("userId") Long userId);


    @Select("<script>" +
            "SELECT COUNT(*) FROM aoa_attends_list a " +
            "WHERE DATE_FORMAT(a.attends_time,'%Y-%m-%d') LIKE CONCAT('%',#{nowdate},'%') " +
            "AND a.attends_user_id = #{userId} " +
            "</script>")
    Integer countrecord(@Param("nowdate") String nowdate, @Param("userId") Long userId);

    @Select("<script>" +
            "SELECT a.attends_id FROM aoa_attends_list a " +
            "WHERE DATE_FORMAT(a.attends_time,'%Y-%m-%d') like CONCAT('%',#{nowdate},'%') " +
            "AND a.attends_user_id=#{userId}" +
            "AND a.type_id = 9 " +
            "</script>")
    Long findoffworkid(@Param("nowdate") String nowdate, @Param("userId") Long userId);


    @Select("<script>" +
            "SELECT " +
            "	attends0_.*  " +
            "FROM " +
            "	aoa_attends_list attends0_ " +
            "	left JOIN aoa_user user1_  on attends0_.attends_user_id = user1_.user_id  " +
            "WHERE " +
            "( " +
            "	attends0_.attends_remark LIKE CONCAT('%',#{baseKey},'%') " +
            "	OR date_format( attends0_.attends_time, '%Y-%m-%d' ) LIKE CONCAT('%',#{baseKey},'%') " +
            "	OR user1_.user_name LIKE CONCAT('%',#{baseKey},'%')  " +
            "	OR attends0_.type_id IN ( SELECT systemtype2_.type_id FROM aoa_type_list systemtype2_ WHERE systemtype2_.type_name LIKE CONCAT('%',#{baseKey},'%') )  " +
            "	OR attends0_.status_id IN ( SELECT systemstat3_.status_id FROM aoa_status_list systemstat3_ WHERE systemstat3_.status_name LIKE CONCAT('%',#{baseKey},'%') )  " +
            "	)  " +
            "	AND attends0_.attends_user_id = #{userid} " +
            "</script>")
    List<AoaAttendsList> findonemohu(@Param("baseKey") String baseKey, @Param("userid") Long userid);


    @Select("<script>" +
            "SELECT " +
            "	attends0_.*  " +
            "FROM " +
            "	aoa_attends_list attends0_ " +
            "	left JOIN aoa_user user1_  on attends0_.attends_user_id = user1_.user_id  " +
            "WHERE " +
            "( " +
            "	attends0_.attends_remark LIKE CONCAT('%',#{baseKey},'%') " +
            "	OR date_format( attends0_.attends_time, '%Y-%m-%d' ) LIKE CONCAT('%',#{baseKey},'%') " +
            "	OR user1_.user_name LIKE CONCAT('%',#{baseKey},'%')  " +
            "	OR attends0_.type_id IN ( SELECT systemtype2_.type_id FROM aoa_type_list systemtype2_ WHERE systemtype2_.type_name LIKE CONCAT('%',#{baseKey},'%') )  " +
            "	OR attends0_.status_id IN ( SELECT systemstat3_.status_id FROM aoa_status_list systemstat3_ WHERE systemstat3_.status_name LIKE CONCAT('%',#{baseKey},'%') )  " +
            "	)  " +
            "   AND attends0_.attends_user_id in " +
            "<foreach item='id' collection='ids' open='(' separator=',' close=')'>" +
            " #{id} " +
            "</foreach> " +
            "</script>")
    List<AoaAttendsList> findsomemohu(@Param("baseKey") String baseKey, @Param("ids") List<Long> ids);


    @Select("<script>" +
            "SELECT COUNT(*) FROM aoa_attends_list a " +
            "WHERE DATE_FORMAT(a.attends_time,'%Y-%m') LIKE CONCAT('%',#{month},'%') " +
            "AND a.attends_user_id = #{userId} " +
            "AND a.type_id=9 " +
            "</script>")
    Integer countoffwork(@Param("month") String month, @Param("userId") Long userId);


    @Select("<script>" +
            "SELECT COUNT(*) FROM aoa_attends_list a " +
            "WHERE DATE_FORMAT(a.attends_time,'%Y-%m') LIKE CONCAT('%',#{month},'%') " +
            "AND a.attends_user_id = #{userId} " +
            "AND a.type_id=8 " +
            "</script>")
    Integer counttowork(String month, Long userId);


    @Select("<script>" +
            "SELECT COUNT(*) FROM aoa_attends_list a " +
            "WHERE DATE_FORMAT(a.attends_time,'%Y-%m') LIKE CONCAT('%',#{month},'%') " +
            "AND a.attends_user_id = #{userId} " +
            "AND a.status_id= #{statusId} " +
            "</script>")
    Integer countnum(@Param("month") String month, @Param("statusId") long statusId, @Param("userId") Long userId);

    //SELECT sum(a.holidayDays) from Attends a
    // where DATE_FORMAT(a.holidayStart,'%Y-%m') like %?1% and a.statusId=?2 and a.user.userId=?3
    @Select("<script>" +
            "SELECT sum(a.holiday_days) FROM aoa_attends_list a " +
            "WHERE DATE_FORMAT(a.holiday_start,'%Y-%m') LIKE CONCAT('%',#{month},'%') " +
            "AND a.status_id= #{statusId} " +
            "AND a.attends_user_id = #{userId} " +
            "</script>")
    Integer countothernum(String month, long statusId, Long userId);


}
