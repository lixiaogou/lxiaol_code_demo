package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaUserLoginRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaUserLoginRecordMapper extends BaseMapper<AoaUserLoginRecord> {

    @Select("<script>" +
            "SELECT COUNT(*) FROM aoa_user_login_record " +
            "WHERE DATE_FORMAT(login_time,'%Y-%m-%d')=#{format} " +
            "</script>")
    Integer countlog(String format);

}
