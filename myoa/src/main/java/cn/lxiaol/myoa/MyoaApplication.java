package cn.lxiaol.myoa;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author lixiaolong
 */
@MapperScan({"cn.lxiaol.myoa.mapper"})
@EnableTransactionManagement
@SpringBootApplication
public class MyoaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyoaApplication.class, args);
        System.out.println("success");
    }

}
