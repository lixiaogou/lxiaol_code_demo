package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaAttendsList;
import cn.lxiaol.myoa.mapper.db1.AoaAttendsListMapper;
import cn.lxiaol.myoa.service.AoaAttendsListService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaAttendsListServiceImpl extends ServiceImpl<AoaAttendsListMapper, AoaAttendsList> implements AoaAttendsListService {

    @Resource
    private AoaAttendsListMapper aoaAttendsListMapper;

    @Override
    public AoaAttendsList findlastest(String nowdate, Long userId) {
        return aoaAttendsListMapper.findlastest(nowdate, userId);
    }

    @Override
    public Integer countrecord(String nowdate, Long userId) {
        return aoaAttendsListMapper.countrecord(nowdate, userId);
    }

    @Override
    public Long findoffworkid(String nowdate, Long userId) {
        return aoaAttendsListMapper.findoffworkid(nowdate, userId);
    }

    @Override
    public PageInfo<AoaAttendsList> singlepage(int pageNum, String baseKey, Long userid, String type, String status, String time) {
        PageHelper.startPage(pageNum, 10);
        List<AoaAttendsList> list;
        //0为降序 1为升序
        if (!Strings.isBlank(baseKey)) {
            list = aoaAttendsListMapper.findonemohu(baseKey, userid);
            return new PageInfo<>(list);
        }

        LambdaQueryWrapper<AoaAttendsList> qw = new LambdaQueryWrapper<>();
        if (!Strings.isBlank(type)) {
            qw.eq(AoaAttendsList::getAttendsUserId, userid);
            if ("0".equals(type)) {
                qw.orderByDesc(AoaAttendsList::getTypeId);
            } else {
                qw.orderByAsc(AoaAttendsList::getTypeId);
            }
            list = this.list(qw);
            return new PageInfo<>(list);
        }

        if (!Strings.isBlank(status)) {
            qw.eq(AoaAttendsList::getAttendsUserId, userid);
            if ("0".equals(status)) {
                qw.orderByDesc(AoaAttendsList::getStatusId);
            } else {
                qw.orderByAsc(AoaAttendsList::getStatusId);
            }
            list = this.list(qw);
            return new PageInfo<>(list);
        }

        if (!Strings.isBlank(time)) {
            qw.eq(AoaAttendsList::getAttendsUserId, userid);
            if ("0".equals(time)) {
                qw.orderByDesc(AoaAttendsList::getAttendsTime);
            } else {
                qw.orderByAsc(AoaAttendsList::getAttendsTime);
            }
            list = this.list(qw);
            return new PageInfo<>(list);

        }


        qw.eq(AoaAttendsList::getAttendsUserId, userid);
        qw.orderByDesc(AoaAttendsList::getAttendsTime);

        list = this.list(qw);
        return new PageInfo<>(list);
    }


    @Override
    public PageInfo<AoaAttendsList> paging(int pageNum, String baseKey, List<Long> ids, String type, String status, String time) {
        PageHelper.startPage(pageNum, 10);
        List<AoaAttendsList> list;

        if (!Strings.isBlank(baseKey)) {
            // 模糊查询
            list = aoaAttendsListMapper.findsomemohu(baseKey, ids);
            return new PageInfo<>(list);
        }

        LambdaQueryWrapper<AoaAttendsList> qw = new LambdaQueryWrapper<>();
        if (!Strings.isBlank(type)) {
            qw.in(AoaAttendsList::getAttendsUserId, ids);
            if ("0".equals(type)) {
                qw.orderByDesc(AoaAttendsList::getTypeId);
            } else {
                qw.orderByAsc(AoaAttendsList::getTypeId);
            }
            list = this.list(qw);
            return new PageInfo<>(list);
        }

        if (!Strings.isBlank(status)) {
            qw.in(AoaAttendsList::getAttendsUserId, ids);
            if ("0".equals(status)) {
                qw.orderByDesc(AoaAttendsList::getStatusId);
            } else {
                qw.orderByAsc(AoaAttendsList::getStatusId);
            }
            list = this.list(qw);
            return new PageInfo<>(list);
        }

        if (!Strings.isBlank(time)) {
            qw.in(AoaAttendsList::getAttendsUserId, ids);
            if ("0".equals(time)) {
                qw.orderByDesc(AoaAttendsList::getAttendsTime);
            } else {
                qw.orderByAsc(AoaAttendsList::getAttendsTime);
            }
            list = this.list(qw);
            return new PageInfo<>(list);

        }

        qw.in(AoaAttendsList::getAttendsUserId, ids);
        qw.orderByDesc(AoaAttendsList::getAttendsTime);

        list = this.list(qw);
        return new PageInfo<>(list);

    }

    @Override
    public Integer countoffwork(String month, Long userId) {
        return aoaAttendsListMapper.countoffwork(month, userId);
    }

    @Override
    public Integer counttowork(String month, Long userId) {
        return aoaAttendsListMapper.counttowork(month, userId);
    }

    @Override
    public Integer countnum(String month, long statusId, Long userId) {
        return aoaAttendsListMapper.countnum(month, statusId, userId);
    }

    @Override
    public Integer countothernum(String month, long statusId, Long userId) {
        return aoaAttendsListMapper.countothernum(month, statusId, userId);
    }
}
