/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : dorm

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 11/05/2021 11:59:46
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for build
-- ----------------------------
DROP TABLE IF EXISTS `build`;
CREATE TABLE `build`
(
    `id`      int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name`    varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名字',
    `remark`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
    `isValid` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'Y' COMMENT '是否有效，Y有效，其他无效',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '宿舍楼' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of build
-- ----------------------------
INSERT INTO `build`
VALUES (1, '北1宿舍楼', '男生宿舍', 'Y');
INSERT INTO `build`
VALUES (2, '南1宿舍楼', '女生宿舍', 'Y');

-- ----------------------------
-- Table structure for dorm
-- ----------------------------
DROP TABLE IF EXISTS `dorm`;
CREATE TABLE `dorm`
(
    `id`       int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name`     varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名字',
    `remark`   varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
    `build_id` int(11) NULL DEFAULT NULL COMMENT '宿舍楼id',
    `max_num`  int(11) NULL DEFAULT 0 COMMENT '最大人数',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '宿舍' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dorm
-- ----------------------------
INSERT INTO `dorm`
VALUES (1, '101', '体育学院', 1, 6);
INSERT INTO `dorm`
VALUES (2, '101', '马克思学院', 2, 6);

-- ----------------------------
-- Table structure for manage_build_rel
-- ----------------------------
DROP TABLE IF EXISTS `manage_build_rel`;
CREATE TABLE `manage_build_rel`
(
    `id`       int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `user_id`  int(11) NULL DEFAULT NULL COMMENT '管理员id',
    `build_id` int(11) NULL DEFAULT NULL COMMENT '宿舍楼id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '宿舍楼与管理员关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manage_build_rel
-- ----------------------------
INSERT INTO `manage_build_rel`
VALUES (1, 4, 1);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`
(
    `id`             int(11) NOT NULL,
    `menuCode`       varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单编码',
    `menuName`       varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单名字',
    `menuLevel`      varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单级别',
    `menuParentCode` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单的父code',
    `menuClick`      varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '点击触发的函数',
    `menuRight`      varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限2表示学生，1表示管理员，0超级管理员，可以用逗号组合使用',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu`
VALUES (1, '001', '宿舍管理员管理', '1', NULL, 'adminManage', '0');
INSERT INTO `menu`
VALUES (2, '002', '学生管理', '1', NULL, 'studentManage', '0,1');
INSERT INTO `menu`
VALUES (3, '003', '宿舍楼管理', '1', NULL, 'buildManage', '0');
INSERT INTO `menu`
VALUES (4, '004', '宿舍管理', '1', NULL, 'dormManage', '0,1');
INSERT INTO `menu`
VALUES (5, '005', '考勤管理', '1', NULL, 'recordManage', '0,1,2');
INSERT INTO `menu`
VALUES (6, '006', '修改密码', '1', NULL, 'modPwd', '0,1,2');

-- ----------------------------
-- Table structure for record
-- ----------------------------
DROP TABLE IF EXISTS `record`;
CREATE TABLE `record`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `student_id`  int(11) NULL DEFAULT NULL COMMENT '学生id',
    `date`        date NULL DEFAULT NULL COMMENT '考勤日期',
    `record_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '考勤类型',
    `remark`      varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '考勤表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of record
-- ----------------------------
INSERT INTO `record`
VALUES (1, 2, '2021-05-10', '1', '晚归');
INSERT INTO `record`
VALUES (2, 3, '2021-05-01', '2', '系楼开会');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`
(
    `id`       int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name`     varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名字',
    `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
    `no`       varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '账号-学生一般用学号',
    `dorm_no`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '宿舍编号',
    `build_id` int(11) NULL DEFAULT NULL COMMENT '宿舍楼id',
    `sex`      varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
    `phone`    varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话',
    `role_id`  int(11) NULL DEFAULT NULL COMMENT '角色 0超级管理员，1管理员，2学生',
    `creator`  int(11) NULL DEFAULT NULL COMMENT '创建人id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user`
VALUES (1, '超级管理员1', '123456', 'admin', NULL, NULL, '1', '123445', 0, NULL);
INSERT INTO `user`
VALUES (2, '张三', 'zhangsan', 'zhangsan', '1', 1, '1', '16677778888', 2, NULL);
INSERT INTO `user`
VALUES (3, '冷檬', 'lengmeng', 'lengmeng', '2', 2, '0', '12412412', 2, NULL);
INSERT INTO `user`
VALUES (4, '鲁智深', 'luzhishen', 'luzhishen', NULL, NULL, '1', '16677778888', 1, NULL);

SET
FOREIGN_KEY_CHECKS = 1;
