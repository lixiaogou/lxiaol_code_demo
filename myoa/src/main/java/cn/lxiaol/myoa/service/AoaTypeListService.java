package cn.lxiaol.myoa.service;

import cn.lxiaol.myoa.entity.db1.AoaTypeList;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaTypeListService extends IService<AoaTypeList> {

    String findname(Long typeId);

    List<AoaTypeList> findByTypeModel(String typeModel);

}
