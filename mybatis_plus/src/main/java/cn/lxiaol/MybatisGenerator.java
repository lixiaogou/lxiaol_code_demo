package cn.lxiaol;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.ArrayList;

/**
 * 代码生成器
 * 需要引入模板引擎依赖
 *
 * @author lixiaolong
 */
public class MybatisGenerator {

    public static void main(String[] args) {
        String tableName = "app_5m5fu2iskt_customer_base_info";
        String url = "jdbc:mysql://localhost:3306/mybatis_plus?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowMultiQueries=true";
        String username = "root";
        String password = "root";
        String driverName = "com.mysql.jdbc.Driver";
        DbType dbType = DbType.MYSQL;

        // 需要构建一个 代码自动生成器 对象
        AutoGenerator mpg = new AutoGenerator();
//        配置策略
        // 1、全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");// 获取项目路径
        gc.setOutputDir(projectPath + "/src/main/java");// 输出路径
        gc.setAuthor("lixiaolong");//作者
        gc.setOpen(false);// 是否打开资源管理器
        gc.setFileOverride(true); // 是否覆盖
        gc.setEnableCache(false); // 默认false,是否开启二级缓存
        gc.setAuthor("lixiaolong"); // 作者
//        gc.setServiceName("%sService");
        gc.setControllerName("%sController");
        gc.setServiceName("I%sService"); // 去Service文件的I前缀
        gc.setServiceImplName("%sServiceImpl");
        gc.setMapperName("I%sMapper");
        gc.setXmlName("%sMapper");
        gc.setEntityName("%s");
        gc.setIdType(IdType.ID_WORKER);// 若数据库时自增,则此处被数据库的自增覆盖
        gc.setDateType(DateType.ONLY_DATE);//日期格式
//        gc.setBaseResultMap(false); // 默认false
        gc.setSwagger2(true); // 是否生成swagger文档 前提得配置swagger依赖
        mpg.setGlobalConfig(gc);

        //2、设置数据源
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(url);
        dsc.setDriverName(driverName);
        dsc.setUsername(username);
        dsc.setPassword(password);
        dsc.setDbType(dbType);
        mpg.setDataSource(dsc);

        //3、包的配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("cn.lxiaol");// 生成的文件所在的包名
        pc.setModuleName("generator"); //模块名
        pc.setEntity("dao");//实体类所在的包名
        pc.setMapper("mapper");// mapper包名
        pc.setService("service");// service包名
        pc.setController("controller");// controller包名
        mpg.setPackageInfo(pc);

        //4、策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setInclude(tableName); // 设置要映射的表名
        strategy.setNaming(NamingStrategy.underline_to_camel);// 驼峰命名法
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityLombokModel(true); // 自动lombok；
        strategy.setLogicDeleteFieldName("deleted");// 逻辑删除
        // 自动填充配置
        TableFill gmtCreate = new TableFill("gmt_create", FieldFill.INSERT);
        TableFill gmtModified = new TableFill("gmt_modified", FieldFill.INSERT_UPDATE);
        ArrayList<TableFill> tableFills = new ArrayList<>();
        tableFills.add(gmtCreate);
        tableFills.add(gmtModified);
        strategy.setTableFillList(tableFills);// 设置自动填充
        strategy.setVersionFieldName("version");// 设置乐观锁
        strategy.setRestControllerStyle(true);// restful风格 userCopy1
//        strategy.setControllerMappingHyphenStyle(true); //localhost:8080/user-copy-1
//        设置策略
        mpg.setStrategy(strategy);

        // 6、配置模板
        TemplateConfig tc = new TemplateConfig();
        // templates/entity.java 模板路径配置，默认在templates目录下，.vm 后缀不用加
        tc.setEntity("templates/entity.java");//使用自定义模板生成实体类
        tc.setController(null);
        tc.setXml(null);
        mpg.setTemplate(tc);

        mpg.execute(); //执行
    }
}