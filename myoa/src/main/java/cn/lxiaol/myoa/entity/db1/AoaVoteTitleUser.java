package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_vote_title_user")
@ApiModel(value = "AoaVoteTitleUser", description = "")
public class AoaVoteTitleUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "vote_title_user_id", type = IdType.AUTO)

    private Long voteTitleUserId;
    private Long voteId;
    private Long userId;
    private Long titleId;


}

