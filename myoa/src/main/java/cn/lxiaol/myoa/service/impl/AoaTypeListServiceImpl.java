package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaTypeList;
import cn.lxiaol.myoa.mapper.db1.AoaTypeListMapper;
import cn.lxiaol.myoa.service.AoaTypeListService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaTypeListServiceImpl extends ServiceImpl<AoaTypeListMapper, AoaTypeList> implements AoaTypeListService {

    @Override
    public String findname(Long typeId) {
        return this.getById(typeId).getTypeName();
    }

    @Override
    public List<AoaTypeList> findByTypeModel(String typeModel) {
        LambdaQueryWrapper<AoaTypeList> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaTypeList::getTypeModel, typeModel);
        return this.list(qw);
    }
}
