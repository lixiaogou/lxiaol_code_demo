package com.atguigu.spring5;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;
/*
 * 接口实现类
 * 实现InvocationHandler类创建代理对象
 */
public class UserDaoProxy implements InvocationHandler {
    //1 把创建的代理对象传递过来, 代理谁就传谁
    private Object obj;

    // 有参构造传递 被代理的对象
    public UserDaoProxy(Object obj) {
        this.obj = obj;
    }

    // 增强的逻辑
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // 方法之前
        System.out.println("方法之前执行...."+method.getName()+" :传递的参数..."+ Arrays.toString(args));

        // 执行 被增强的方法
        Object res = method.invoke(obj, args);

        // 方法之后
        System.out.println("方法之后执行...."+obj);
        return res;
    }
}
