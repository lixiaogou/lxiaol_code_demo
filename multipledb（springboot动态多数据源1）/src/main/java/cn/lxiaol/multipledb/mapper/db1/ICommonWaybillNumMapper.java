package cn.lxiaol.multipledb.mapper.db1;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.lxiaol.multipledb.entity.CommonWaybillNum;

/**
 * <p>
 * 电子运单编号 Mapper 接口
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-09
 */
public interface ICommonWaybillNumMapper extends BaseMapper<CommonWaybillNum> {

}
