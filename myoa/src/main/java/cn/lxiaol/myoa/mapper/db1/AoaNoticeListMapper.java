package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaNoticeList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaNoticeListMapper extends BaseMapper<AoaNoticeList> {
    @Select("<script>" +
            "SELECT " +
            "	n.*, " +
            "	u.*  " +
            "FROM " +
            "	aoa_notice_list AS n " +
            "	LEFT JOIN aoa_notice_user_relation AS u ON n.notice_id = u.relatin_notice_id  " +
            "WHERE " +
            "	u.relatin_user_id = #{userId}  " +
            "	ORDER BY n.is_top DESC, u.is_read ASC, n.modify_time DESC " +
            "	LIMIT 5 " +
            "</script>")
    List<Map<String, Object>> findMyNoticeLimit(Long userId);


}
