package cn.lxiaol.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author lxiaol
 * @date 2021年05月19日 14:38
 */
public class MapDemo {

    public static void main(String[] args) {
        String[] strArr = {"abcd", "bcdd", "defde", "ftr"};
        List<String> strings = Arrays.asList(strArr);
        Stream<String> stringStream = Arrays.stream(strArr).map(String::toUpperCase);
        stringStream.forEach(System.out::println);
        System.out.println("==========================================");

        person();
        System.out.println("==========================================");

        person2();
        System.out.println("==========================================");

    }

    // 2、对象集合>>数据
    public static void person() {
        List<Person> list = new ArrayList<Person>();
        list.add(new Person("Tom", 8900));
        list.add(new Person("Jack", 7000));
        list.add(new Person("Lily", 9000));
        list.add(new Person("Lily", 5000));

        list.stream().map(Person::getName).forEach(System.out::println);
    }


    // 3、对象集合>>对象集合
    public static void person2() {
        List<Person> list = new ArrayList<Person>();
        list.add(new Person("Tom", 8900));
        list.add(new Person("Jack", 7000));
        list.add(new Person("Lily", 9000));
        list.add(new Person("Lily", 5000));

        List<Person> list1 = list.stream().peek(person -> {
            person.setSalary(person.getSalary() + 1);
            person.setName(person.getName() + "-");
        }).collect(Collectors.toList());
        System.out.println("list1 = " + list1);


        List<Person> list2 = list.stream().map(person -> {
            Person personNew = new Person(null, 0);
            personNew.setName(person.getName());
            personNew.setSalary(person.getSalary() + 10000);
            return personNew;
        }).collect(Collectors.toList());
        System.out.println("list2 = " + list2);


    }

}
