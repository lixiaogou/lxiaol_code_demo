package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaVoteTitles;
import cn.lxiaol.myoa.mapper.db1.AoaVoteTitlesMapper;
import cn.lxiaol.myoa.service.AoaVoteTitlesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaVoteTitlesServiceImpl extends ServiceImpl<AoaVoteTitlesMapper, AoaVoteTitles> implements AoaVoteTitlesService {

}
