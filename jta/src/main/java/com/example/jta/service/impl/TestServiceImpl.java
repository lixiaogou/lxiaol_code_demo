package com.example.jta.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.jta.dao.Test;
import com.example.jta.mapper.db1mapper.TestMapper;
import com.example.jta.service.TestService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统用户 服务实现类
 * </p>
 *
 * @author lixiaolong
 * @since 2021-08-23
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements TestService {

}
