package cn.lxiaol.stream;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * averagingDouble、averagingInt、averagingLong三个方法处理过程是相同的，都是返回stream的平均值，只是返回结果的类型不同。
 *
 * @author lxiaol
 * @date 2021年05月19日 11:48
 */
public class AveragingDemo {

    public static void main(String[] args) {
        List<Person> list = new ArrayList<Person>();
        list.add(new Person("Tom", 8000));
        list.add(new Person("Jack", 7000));
        list.add(new Person("Lily", 9000));

        //平均值
        Double averaging1 = list.stream().collect(Collectors.averagingInt(Person::getSalary));
        System.out.println("averaging1 = " + averaging1);

        Double averaging2 = list.stream().collect(Collectors.averagingDouble(Person::getSalary));
        System.out.println("averaging2 = " + averaging2);


        Double averaging3 = list.stream().collect(Collectors.averagingLong(Person::getSalary));
        System.out.println("averaging3 = " + averaging3);

    }
}
