package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaNoticeList;
import cn.lxiaol.myoa.entity.db1.AoaStatusList;
import cn.lxiaol.myoa.entity.db1.AoaUser;
import cn.lxiaol.myoa.mapper.db1.AoaNoticeListMapper;
import cn.lxiaol.myoa.service.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaNoticeListServiceImpl extends ServiceImpl<AoaNoticeListMapper, AoaNoticeList> implements AoaNoticeListService {

    @Resource
    private AoaNoticeListMapper aoaNoticeListMapper;
    @Resource
    private AoaTypeListService aoaTypeListService;
    @Resource
    private AoaStatusListService aoaStatusListService;
    @Resource
    private AoaUserService aoaUserService;
    @Resource
    private AoaNoticeUserRelationService aoaNoticeUserRelationService;


    @Override
    public List<Map<String, Object>> findMyNoticeLimit(Long userId) {
        return aoaNoticeListMapper.findMyNoticeLimit(userId);
    }

    @Override
    public PageInfo<AoaNoticeList> pageThis(int pageNum, Long userId) {
        PageHelper.startPage(pageNum, 10);

        LambdaQueryWrapper<AoaNoticeList> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaNoticeList::getUserId, userId);
        qw.orderByDesc(AoaNoticeList::getIsTop, AoaNoticeList::getModifyTime);

        List<AoaNoticeList> list = this.list(qw);
        PageInfo<AoaNoticeList> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public List<Map<String, Object>> fengZhuang(List<AoaNoticeList> noticeList) {
        List<Map<String, Object>> list = new ArrayList<>();

        for (AoaNoticeList obj : noticeList) {
            Long statusId = obj.getStatusId();
            AoaStatusList aoaStatusList = aoaStatusListService.getById(statusId);
            AoaUser aoaUser = aoaUserService.findOne(obj.getUserId());

            Map<String, Object> result = new HashMap<>();
            result.put("noticeId", obj.getNoticeId());
            result.put("typename", aoaTypeListService.findname(obj.getTypeId()));
            result.put("statusname", aoaStatusList.getStatusName());
            result.put("statuscolor", aoaStatusList.getStatusColor());
            result.put("title", obj.getTitle());
            result.put("noticeTime", obj.getNoticeTime());
            result.put("top", obj.getIsTop() == 1);
            result.put("url", obj.getUrl());
            result.put("username", aoaUser.getUserName());
            result.put("deptname", aoaUser.getDept().getDeptName());
            list.add(result);
        }

        return list;
    }

    @Override
    public PageInfo<AoaNoticeList> pageThis(int pageNum, Long userId, String baseKey,
                                            String type, String status, String time) {
        PageHelper.startPage(pageNum, 10);

        LambdaQueryWrapper<AoaNoticeList> qw = new LambdaQueryWrapper<>();
        qw.eq(AoaNoticeList::getUserId, userId);
//        qw.orderByDesc(AoaNoticeList::getIsTop, AoaNoticeList::getModifyTime);

        //根据类型排序
        if (!Strings.isBlank(type)) {
            if ("1".equals(type)) {
                qw.orderByDesc(AoaNoticeList::getTypeId);
            } else {
                qw.orderByAsc(AoaNoticeList::getTypeId);
            }
        }
        //根据状态排序
        else if (!Strings.isBlank(status)) {
            if ("1".equals(status)) {
                qw.orderByDesc(AoaNoticeList::getStatusId);
            } else {
                qw.orderByAsc(AoaNoticeList::getStatusId);
            }
        }
//        //根据时间排序
        else if (!Strings.isBlank(time)) {
            if ("1".equals(time)) {
                qw.orderByDesc(AoaNoticeList::getModifyTime);
            } else {
                qw.orderByAsc(AoaNoticeList::getModifyTime);
            }
        } else if (!Strings.isBlank(baseKey)) {
            qw.like(AoaNoticeList::getTitle, baseKey);
        }

        List<AoaNoticeList> list = this.list(qw);
        PageInfo<AoaNoticeList> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public boolean deleteOne(Long noticeId) {

        // 数据库外键改为了 CASCADE：父表delete、update的时候，子表会delete、update掉关联记录；所以注释掉字表的删除代码
//        AoaNoticeList notice = this.getById(noticeId);
//        List<AoaNoticeUserRelation> relationList =
//                aoaNoticeUserRelationService.findByNoticeId(notice.getNoticeId());
//        List<Long> collect = relationList.stream()
//                .map(AoaNoticeUserRelation::getRelatinNoticeId).collect(Collectors.toList());
//        aoaNoticeUserRelationService.removeByIds(collect);

        this.removeById(noticeId);
        System.out.println("通知删除成功！");
        return true;

    }


}
