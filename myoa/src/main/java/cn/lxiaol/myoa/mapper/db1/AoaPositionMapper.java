package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaPosition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaPositionMapper extends BaseMapper<AoaPosition> {
    @Select("<script>" +
            "SELECT * FROM aoa_position WHERE position_id =#{positionId} limit 1 " +
            "</script>")
    AoaPosition findOneByPositionId(@Param("positionId") Long positionId);
}
