package cn.lxiaol.stream;

/**
 * @author lxiaol
 * @date 2021年05月18日 17:22
 */
public class Student {
    String name;
    int age;

    Student() {

    }

    Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

}
