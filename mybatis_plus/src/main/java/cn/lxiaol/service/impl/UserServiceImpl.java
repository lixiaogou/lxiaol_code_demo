package cn.lxiaol.service.impl;

import cn.lxiaol.dao.User;
import cn.lxiaol.mapper.UserMapper;
import cn.lxiaol.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;


/**
 * @author lixiaolong
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
