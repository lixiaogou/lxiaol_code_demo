package cn.lxiaol.myoa.service;

import cn.lxiaol.myoa.entity.db1.AoaRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaRoleService extends IService<AoaRole> {

}
