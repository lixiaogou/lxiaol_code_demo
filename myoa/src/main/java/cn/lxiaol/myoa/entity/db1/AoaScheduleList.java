package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_schedule_list")
@ApiModel(value = "AoaScheduleList", description = "")
public class AoaScheduleList implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "rc_id", type = IdType.AUTO)
    private Long rcId;
    private Date createTime = new Date();
    private Date endTime;
    private String filedescribe;
    private Integer isRemind = 0;
    private Date startTime;
    private Long statusId;
    private String title;
    private Long typeId;
    private Long userId;
    private String miaoshu;
    private Integer isreminded = 0;

    @TableField(exist = false)
    private String username;

    @TableField(exist = false)
    private AoaUser user;            //日程所属人

    @TableField(exist = false)
    private List<AoaUser> users;    //联系人集合
}

