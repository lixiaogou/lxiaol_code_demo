package cn.lxiaol.multipledb.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-09
 */
@Data
@Accessors(chain = true)
@TableName("wh_company")
public class WhCompany implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)

    private Integer id;
    /**
     * 企业名称
     */
    private String companyName;
    /**
     * 行政区划代码
     */
    private String companyArea;
    /**
     * 经营状态
     */
    private String companyStatus;
    /**
     * 经营许可证号
     */
    private String companyNo;
    /**
     * 有效期起
     */
    private String companyValidityStart;
    /**
     * 有效期止
     */
    private String companyValidityStop;
    /**
     * 证照状态
     */
    private String licenseStatus;
    /**
     * 负责人姓名
     */
    private String name;
    /**
     * 统一社会信用代码
     */
    private String tyshxydm;
    /**
     * 经营范围
     */
    private String businessScope;
    private Integer uid;
    /**
     * 添加时间
     */
    private String addtime;
    /**
     * 修改时间
     */
    private String edittime;
    private Integer edituid;
    /**
     * 公章
     */
    private String imgzhang;


}

