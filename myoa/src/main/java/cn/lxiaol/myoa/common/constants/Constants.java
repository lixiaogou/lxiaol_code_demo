package cn.lxiaol.myoa.common.constants;

/**
 * 常量
 * @author lxiaol
 * @date 2021年04月22日 19:15
 */
public class Constants {

    public static final String CAPTCHA_KEY = "session_captcha";

}
