package cn.lxiaol.filter;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;

/**
 * @author lxiaol
 * @date 2021年05月08日 21:49
 */
public class AdminFilter extends HttpFilter {

    public AdminFilter() {
        System.out.println("过滤器创建");
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        super.init(config);
        System.out.println("过滤器初始化");

        System.out.println("filterName = " + config.getFilterName());

        System.out.println("servletContext = " + config.getServletContext());

        Enumeration<String> initParameterNames = config.getInitParameterNames();
        while (initParameterNames.hasMoreElements()) {
            System.out.println("initParameterNames = " + initParameterNames.nextElement());
        }

        System.out.println("initParameter = " + config.getInitParameter("username"));

    }

    /**
     * doFilter 方法，专门用于拦截请求。可以做权限检查
     */
    @Override
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("doFilter");
        HttpSession session = req.getSession();
        Object user = session.getAttribute("user");
        // 如果等于null，说明还没有登录
        if (user == null) {
            resp.sendRedirect("/s1/admin.jsp");
//            req.getRequestDispatcher("/admin.jsp").forward(req, resp);
        } else {
            // 让程序继续往下访问用户的目标资源
            filterChain.doFilter(req, resp);
        }
    }

    @Override
    public void destroy() {
        System.out.println("过滤器销毁了");
    }
}
