package cn.lxiaol.threadlocal;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author lxiaol
 * @date 2021年05月09日 17:27
 */
public class ThreadLocalTest {

    /**
     * 默认值
     */
    public static Map<String, Object> data = new ConcurrentHashMap<>();
    private static final Random RANDOM = new Random();

    public static ThreadLocal<Object> threadLocal = ThreadLocal.withInitial(() -> "童伟的默认值");


    public static class Task implements Runnable {
        @Override
        public void run() {
            // 在Run方法中，随机生成一个变量（线程要关联的数据），然后以当前线程名为key保存到map中
            Integer i = RANDOM.nextInt(1000);
            String name = Thread.currentThread().getName();
            System.out.println("线程[" + name + "]生成的随机数是：" + i);
            data.put(name, i);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // 在Run方法结束之前，以当前线程名获取出数据并打印。查看是否可以取出操作
            Object o = data.get(name);
            System.out.println("在线程[" + name + "]快结束时取出关联的数据是：" + o);

        }

    }

    public static class Task2 implements Runnable {
        @Override
        public void run() {
            // 在Run方法中，随机生成一个变量（线程要关联的数据），然后以当前线程名为key保存到map中
            Integer i = RANDOM.nextInt(1000);
            String name = Thread.currentThread().getName();
            threadLocal.set(i);
            System.out.println("在线程[" + name + "]保存的值是：" + i);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // 在Run方法结束之前，以当前线程名获取出数据并打印。查看是否可以取出操作
            Object o = threadLocal.get();
            System.out.println("在线程[" + name + "]快结束时取出关联的数据是：" + o);

        }

    }

    public static void main(String[] args) {
        // 开三个线程
        for (int i = 0; i < 3; i++) {
            new Thread(new Task2()).start();
//            new Thread(new Task2()).start();
        }

//        Map<String, Object> map = new HashMap<>();
//        System.out.println(map.get("key"));
//
//        System.out.println(threadLocal.get()); // 不保存，也想取一个有效的值。怎么办，需要你事务准备这个值。

    }


}
