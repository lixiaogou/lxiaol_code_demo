package cn.lxiaol.myoa.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Slf4j
@Api(value = "/", tags = {"日历相关接口"})
@Controller
@RequestMapping("/")
public class DaymanageController {
	/**
	 * 日历controller
	 *
	 * @return
	 */
	@GetMapping("daycalendar")
	private String daycalendar() {
		return "daymanage/daycalendar";
	}
}
