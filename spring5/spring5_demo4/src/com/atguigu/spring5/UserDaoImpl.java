package com.atguigu.spring5;

/**
 * 2 创建接口实现类，重写方法
 */
public class UserDaoImpl implements UserDao{
    @Override
    public int add(int a, int b) {
        return a+b;
    }

    @Override
    public String update(String id) {
        return id;
    }
}
