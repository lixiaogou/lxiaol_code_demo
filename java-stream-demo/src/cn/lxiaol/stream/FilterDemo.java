package cn.lxiaol.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 流的筛选，即filter，是按照一定的规则校验流中的元素，将符合条件的元素提取出来的操作。
 * filter通常要配合collect（收集），将筛选结果收集成一个新的集合。
 *
 * @author lxiaol
 * @date 2021年05月18日 18:02
 */
public class FilterDemo {

    public static void main(String[] args) {
        List<Integer> intList = Arrays.asList(6, 7, 3, 8, 1, 2, 9);
        List<Integer> collect = intList.stream().filter(x -> x > 7).collect(Collectors.toList());
        System.out.println(collect);


    }


}
