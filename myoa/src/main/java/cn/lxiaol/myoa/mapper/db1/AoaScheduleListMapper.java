package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaScheduleList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaScheduleListMapper extends BaseMapper<AoaScheduleList> {

    @Select("<script>" +
            "SELECT " +
            "	scheduleli0_.rc_id, " +
            "	scheduleli0_.create_time, " +
            "	scheduleli0_.miaoshu, " +
            "	scheduleli0_.end_time , " +
            "	scheduleli0_.is_remind, " +
            "	scheduleli0_.isreminded, " +
            "	scheduleli0_.start_time, " +
            "	scheduleli0_.status_id, " +
            "	scheduleli0_.title, " +
            "	scheduleli0_.type_id, " +
            "	scheduleli0_.user_id " +
            "FROM " +
            "	aoa_schedule_list scheduleli0_ " +
            "	LEFT OUTER JOIN aoa_user user1_ ON scheduleli0_.user_id = user1_.user_id  " +
            "WHERE " +
            "	user1_.user_id = #{userId}" +
            "</script>")
    @Results({
            @Result(column = "user_id", property = "userId"),
            @Result(column = "user_id", property = "user", one = @One(select = "cn.lxiaol.myoa.mapper.db1.AoaUserMapper.findOne"))
    })
    List<AoaScheduleList> myschedule(@Param("userId") Long userId);


    @Select("<script>" +
            "SELECT  " +
            "	scheduleli0_.rc_id, " +
            "	scheduleli0_.create_time, " +
            "	scheduleli0_.miaoshu, " +
            "	scheduleli0_.end_time, " +
            "	scheduleli0_.is_remind, " +
            "	scheduleli0_.isreminded, " +
            "	scheduleli0_.start_time, " +
            "	scheduleli0_.status_id, " +
            "	scheduleli0_.title, " +
            "	scheduleli0_.type_id, " +
            "	scheduleli0_.user_id " +
            "FROM  " +
            "	aoa_schedule_list scheduleli0_  " +
            "	LEFT OUTER JOIN aoa_schedule_user users1_ ON scheduleli0_.rc_id = users1_.rcid   " +
            "	LEFT OUTER JOIN aoa_user user2_ ON users1_.user_id = user2_.user_id  " +
            "WHERE  " +
            "	user2_.user_id =#{userId}" +
            "</script>")
    @Results({
            @Result(column = "user_id", property = "userId"),
            @Result(column = "user_id", property = "user", one = @One(select = "cn.lxiaol.myoa.mapper.db1.AoaUserMapper.findOne"))
    })
    List<AoaScheduleList> otherschedule(@Param("userId") Long userId);
}
