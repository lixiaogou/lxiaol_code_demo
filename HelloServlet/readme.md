# servlet_demo

#### 介绍
# 尚硅谷最新版JavaWeb的121-159p学习笔记，对应视频在b站
> https://www.bilibili.com/video/BV1Y7411K7zz 

## 该笔记只是121-159p的内容 主要是想学习下servlet部分。
## 代码已经放在gitee上面了：
> https://gitee.com/lixiaogou/lxiaol_code_demo/tree/master/HelloServlet
## 笔记在doc

> 视频和所需资料在： 
> 链接：https://pan.baidu.com/s/1gYmU78JRmqQlnB6DV5mlvg
> 提取码：xlgc

> 连接如果挂了 麻烦请联系我，及时补发。

**如有侵权，请联系我删除**

#### 软件架构

1. java 8
2. tomcat 8.5
3. maven 3.6


#### 使用说明

1. clone 代码
2. idea/eclipse 中打开项目
3. 配置tomcat
4. 启动tomcat

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
