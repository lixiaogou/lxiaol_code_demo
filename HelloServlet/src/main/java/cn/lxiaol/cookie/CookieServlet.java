package cn.lxiaol.cookie;

import cn.lxiaol.base.BaseServlet;
import cn.lxiaol.util.CookieUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * @author lxiaol
 * @date 2021年05月08日 15:25
 */
public class CookieServlet extends BaseServlet {

    /**
     * 创建cookie
     *
     * @param req
     * @param resp
     * @throws Exception
     */
    public void createCookie(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        //1 创建Cookie对象
        Cookie cookie = new Cookie("key4", "value4");
        //2 通知客户端保存Cookie
        resp.addCookie(cookie);

        //1 创建Cookie对象
        Cookie cookie1 = new Cookie("key5", "value5");
        //2 通知客户端保存Cookie
        resp.addCookie(cookie1);

        System.out.println("cookie1 = " + cookie1);
        resp.getWriter().write("Cookie创建成功");
    }

    /**
     * 获取cookie
     *
     * @param req
     * @param resp
     * @throws Exception
     */
    public void getCookie(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        Cookie[] cookies = req.getCookies();

        for (Cookie cookie : cookies) {
            String name = cookie.getName();
            String value = cookie.getValue();
            // getName方法返回Cookie的key（名）
            // getValue方法返回Cookie的value值
            resp.getWriter().write("Cookie[" + name + "=" + value + "] <br/>");
        }
        System.out.println("============获取全部cookie成功============");


        Cookie iWantCookie = CookieUtils.findCookie("key5", cookies);

        // 如果不等于null，说明赋过值，也就是找到了需要的Cookie
        if (Objects.nonNull(iWantCookie)) {
            resp.getWriter().write("找到了需要的Cookie");
        } else {
            resp.getWriter().write("没找到了需要的Cookie");
        }

    }


    /**
     * 修改cookie
     *
     * @param req
     * @param resp
     * @throws Exception
     */
    public void updateCookie(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        方案一：
//        1、先创建一个要修改的同名的Cookie对象
//        2、在构造器，同时赋于新的Cookie值。
//        Cookie cookie = new Cookie("key1","newValue1");
//        3、调用response.addCookie( Cookie ); 通知 客户端 保存修改
//        resp.addCookie(cookie);

//        方案二：
//        1、先查找到需要修改的Cookie对象
        Cookie cookie = CookieUtils.findCookie("key4", req.getCookies());
        if (Objects.nonNull(cookie)) {
//          2、调用setValue()方法赋于新的Cookie值。
            cookie.setValue("newValue2");
            cookie.setComment(":fewaefaw");
//        3、调用response.addCookie()通知客户端保存修改
            resp.addCookie(cookie);
        }

        resp.getWriter().write("key4的Cookie已经修改好");

    }

    /**
     * 默认的会话级别的Cookie
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    public void defaultLife(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie cookie = new Cookie("defalutLife", "defaultLife");
        cookie.setMaxAge(-1);//设置存活时间
        resp.addCookie(cookie);
        resp.getWriter().write("默认的会话级别的Cookie");
    }


    /**
     * 马上删除一个Cookie
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    public void deleteNow(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 先找到你要删除的Cookie对象
        Cookie cookie = CookieUtils.findCookie("key4", req.getCookies());
        if (cookie != null) {
            // 调用setMaxAge(0);
            cookie.setMaxAge(0); // 表示马上删除，都不需要等待浏览器关闭
            // 调用response.addCookie(cookie);
            resp.addCookie(cookie);

            resp.getWriter().write("key4的Cookie已经被删除");
        }

    }

    /**
     * 设置存活1个小时的Cooie
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    public void life3600(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cookie cookie = new Cookie("life3600", "life3600");
        cookie.setMaxAge(60 * 60); // 设置Cookie一小时之后被删除。无效
        resp.addCookie(cookie);
        resp.getWriter().write("已经创建了一个存活一小时的Cookie");

    }

    /**
     * Cookie 有效路径Path 的设置
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    public void testPath(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie cookie = new Cookie("path1", "path1");
        // getContextPath() ===>>>>  得到工程路径
        cookie.setPath(req.getContextPath() + "/abc"); // ===>>>>  /工程路径/abc
        resp.addCookie(cookie);
        resp.getWriter().write("创建了一个带有Path路径的Cookie");
    }
}

