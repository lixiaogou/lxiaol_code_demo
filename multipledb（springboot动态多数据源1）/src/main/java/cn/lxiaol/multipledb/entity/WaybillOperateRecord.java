package cn.lxiaol.multipledb.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 电子运单对接日志
 * </p>
 *
 * @author lixiaolong
 * @since 2020-09-03
 */
@Data
@Accessors(chain = true)
@TableName("waybill_operate_record")
public class WaybillOperateRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)

    private Long id;
    /**
     * 操作员：用户id
     */
    private String userId;
    /**
     * 公司id
     */
    private String companyId;
    /**
     * 子公司id
     */
    private String subCompanyId;
    /**
     * 模块标识：管理员查看
     */
    private String moduleCode;
    /**
     * 模块名：系统展示
     */
    private String moduleName;
    /**
     * 方法标识
     */
    private String methodCode;
    /**
     * 方法名：系统展示
     */
    private String methodName;
    /**
     * 操作ip地址
     */
    private String ip;
    /**
     * 入参
     */
    private String reqParams;
    /**
     * 出参
     */
    private String resParams;
    /**
     * 添加时间yyyy-MM-dd HH:mm:ss
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 修改时间yyyy-MM-dd HH:mm:ss
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    /**
     * 乐观锁
     */
    @Version
    private Integer version;
    /**
     * 是否逻辑删除 0否1是
     */
    @TableLogic
    private Integer deleted;
    /**
     * 省平台返回的data
     */
    private String data;
    /**
     * 省平台返回的body
     */
    private String body;
    /**
     * 运单号
     */
    private String waybillNum;
    /**
     * 省平台返回的msg
     */
    private String msg;
    /**
     * 省平台返回的code
     */
    private String code;
    /**
     * 电子运单来源 1php电子运单 2tms电子运单 3app电子运单
     */
    private String source;


}

