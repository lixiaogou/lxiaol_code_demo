package cn.lxiaol.myoa.service;

import cn.lxiaol.myoa.entity.db1.AoaNoticeList;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaNoticeListService extends IService<AoaNoticeList> {

    List<Map<String, Object>> findMyNoticeLimit(Long userId);

    PageInfo<AoaNoticeList> pageThis(int page, Long userId);

    List<Map<String, Object>> fengZhuang(List<AoaNoticeList> noticeList);

    PageInfo<AoaNoticeList> pageThis(int pageNum, Long userId, String baseKey, String type, String status, String time);

    boolean deleteOne(Long noticeId);

}
