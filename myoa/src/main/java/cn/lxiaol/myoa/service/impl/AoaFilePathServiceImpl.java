package cn.lxiaol.myoa.service.impl;

import cn.lxiaol.myoa.entity.db1.AoaFilePath;
import cn.lxiaol.myoa.mapper.db1.AoaFilePathMapper;
import cn.lxiaol.myoa.service.AoaFilePathService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Service
public class AoaFilePathServiceImpl extends ServiceImpl<AoaFilePathMapper, AoaFilePath> implements AoaFilePathService {

}
