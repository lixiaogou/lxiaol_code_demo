package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_traffic")
@ApiModel(value = "AoaTraffic", description = "")
public class AoaTraffic implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "traffic_id", type = IdType.AUTO)

    private Long trafficId;
    private String departName;
    private Date departTime;
    private String reachName;
    private String seatType;
    private Double trafficMoney;
    private String trafficName;
    private Long evectionId;
    private Long userName;


}

