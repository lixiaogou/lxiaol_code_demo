package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_notepaper")
@ApiModel(value = "AoaNotepaper", description = "")
public class AoaNotepaper implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "notepaper_id", type = IdType.AUTO)

    private Long notepaperId;
    private String concent;
    private Date createTime;
    private String title;
    private Long notepaperUserId;


}

