package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_love_discuss_user")
@ApiModel(value = "AoaLoveDiscussUser", description = "")
public class AoaLoveDiscussUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "discuss_id", type = IdType.ID_WORKER)
    private Long discussId;
    private Long userId;


}

