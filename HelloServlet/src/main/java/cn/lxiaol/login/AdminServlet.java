package cn.lxiaol.login;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author lxiaol
 * @date 2021年05月08日 15:30
 */

public class AdminServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 解决响应中文乱码问题
        resp.setContentType("text/html; charset=UTF-8");

        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if ("123456".equals(username) && "123456".equals(password)) {
            req.getSession().setAttribute("user", username);
            resp.getWriter().write("登录成功！！！");
        } else {
            req.getRequestDispatcher("/admin.jsp").forward(req, resp);
        }
    }
}
