package cn.lxiaol.myoa.service;

import cn.lxiaol.myoa.entity.db1.AoaFilePath;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaFilePathService extends IService<AoaFilePath> {

}
