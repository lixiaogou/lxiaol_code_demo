package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_attends_list")
@ApiModel(value = "AoaAttendsList", description = "")
public class AoaAttendsList implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "attends_id", type = IdType.AUTO)

    private Long attendsId;
    private String attendsIp;
    private String attendsRemark;
    private Date attendsTime;
    private Long statusId;
    private Long typeId;
    private Long attendsUserId;
    private String attendHmtime;
    private String weekOfday;
    private Double holidayDays;
    private Date holidayStart;

    public AoaAttendsList() {
    }

    public AoaAttendsList(Long typeId, Long statusId, Date attendsTime, String attendHmtime, String weekOfday,
                          String attendsIp, Long attendsUserId) {
        super();
        this.typeId = typeId;
        this.statusId = statusId;
        this.attendsTime = attendsTime;
        this.attendHmtime = attendHmtime;
        this.weekOfday = weekOfday;
        this.attendsIp = attendsIp;
        this.attendsUserId = attendsUserId;
    }

}

