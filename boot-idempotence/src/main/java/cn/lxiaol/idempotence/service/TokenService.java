package cn.lxiaol.idempotence.service;

import cn.lxiaol.idempotence.constants.RedisKeyConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Token 服务层
 *
 * @author lixiaolong
 * @date 2021/3/30 10:20
 */
@Slf4j
@Service
public class TokenService {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    /**
     * 创建 Token 存入 Redis，并返回该 Token
     *
     * @param value 用户信息，用于辅助验证
     * @return 生成的 Token 串
     */
    public String generateToken(String value) {
        // 使用uuid 生成token
        String token = UUID.randomUUID().toString();

        // 设置 Key
        String key = RedisKeyConstants.IDEMPOTENT_TOKEN_PREFIX + token;

        // 存储 Token 到 Redis，且设置过期时间为5分钟
        redisTemplate.opsForValue().set(key, value, 5, TimeUnit.MINUTES);

        // 返回 Token
        return token;
    }

    /**
     * 验证 Token
     *
     * @param token token 字符串
     * @param value value 存储在Redis中的辅助验证信息
     * @return 验证结果
     */
    public boolean validToken(String token, String value) {

        // 根据 Key 前缀拼接 Key
        String key = RedisKeyConstants.IDEMPOTENT_TOKEN_PREFIX + token;

        // 设置 Lua 脚本，其中 KEYS[1] 是 key，KEYS[2] 是 value
        String script = "if redis.call('get', KEYS[1]) == KEYS[2] then return redis.call('del', KEYS[1]) else return 0 end";
        RedisScript<Long> redisScript = new DefaultRedisScript<>(script, Long.class);

        // 执行 Lua 脚本
        Long result = redisTemplate.execute(redisScript, Arrays.asList(key, value));

        // 根据返回结果判断是否成功成功匹配并删除 Redis 键值对，若果结果不为空和0，则验证通过
        if (result != null && result != 0L) {
            log.info("验证 token={},key={},value={} 成功", token, key, value);
            return true;
        }

        log.info("验证 token={},key={},value={} 失败", token, key, value);
        return false;
    }


}
