package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_task_list")
@ApiModel(value = "AoaTaskList", description = "")
public class AoaTaskList implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "task_id", type = IdType.AUTO)

    private Long taskId;
    private String comment;
    private Date endTime;
    private Integer isCancel;
    private Integer isTop;
    private Date modifyTime;
    private Date publishTime;
    private Date starTime;
    private Integer statusId;
    private String taskDescribe;
    private String ticking;
    private String title;
    private Long typeId;
    private Long taskPushUserId;
    private String reciverlist;


}

