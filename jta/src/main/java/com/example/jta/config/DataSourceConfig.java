package com.example.jta.config;

import com.alibaba.druid.pool.xa.DruidXADataSource;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {

    @Bean(name = "db1")
    @Primary
    public DataSource db1(Db1Properties db1Properties) {
        DruidXADataSource xaDataSource = new DruidXADataSource();
        BeanUtils.copyProperties(db1Properties, xaDataSource);
        AtomikosDataSourceBean dataSource = new AtomikosDataSourceBean();
        dataSource.setXaDataSourceClassName("com.alibaba.druid.pool.xa.DruidXADataSource");
        dataSource.setUniqueResourceName("db1Name");
        dataSource.setXaDataSource(xaDataSource);
        return dataSource;
    }

    @Bean(name = "db2")
    public DataSource db2(Db2Properties db2Properties) {
        DruidXADataSource xaDataSource = new DruidXADataSource();
        BeanUtils.copyProperties(db2Properties, xaDataSource);
        AtomikosDataSourceBean dataSource = new AtomikosDataSourceBean();
        dataSource.setXaDataSourceClassName("com.alibaba.druid.pool.xa.DruidXADataSource");
        dataSource.setUniqueResourceName("db2Name");
        dataSource.setXaDataSource(xaDataSource);
        return dataSource;
    }
}
