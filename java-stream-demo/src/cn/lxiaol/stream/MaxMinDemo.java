package cn.lxiaol.stream;

import java.util.*;

/**
 * 在stream中，针对流进行计算后得出结果，例如求和、求最值等，这样的操作被称为聚合操作。
 * 聚合操作在广义上包含了max、min、count等方法和reduce、collect。
 *
 * @author lxiaol
 * @date 2021年05月18日 17:35
 */
public class MaxMinDemo {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("adn", "admmt", "pot");

        Optional<String> max = list.stream().max(Comparator.comparing(String::length));
        Optional<String> min = list.stream().min(Comparator.comparing(String::length));

        System.out.println("max = " + max);
        System.out.println("min = " + min);


        List<Integer> list2 = Arrays.asList(7, 6, 9);
        Optional<Integer> max2 = list2.stream().max(Integer::compareTo);
        Optional<Integer> min2 = list2.stream().min(Integer::compareTo);
        System.out.println("max2 = " + max2);
        System.out.println("min2 = " + min2);


        List<Person> list3 = new ArrayList<>();
        list3.add(new Person("a", 4));
        list3.add(new Person("ab", 7));
        list3.add(new Person("abc", 6));

        Optional<Person> max3 = list3.stream().max(Comparator.comparingInt(Person::getSalary));
        Optional<Person> min3 = list3.stream().min(Comparator.comparingInt(Person::getSalary));
        System.out.println("max3 = " + max3);
        System.out.println("min3 = " + min3);


        Optional<Person> max4 = list3.stream().max(Comparator.comparing(Person::getName));
        Optional<Person> min4 = list3.stream().min(Comparator.comparing(Person::getName));
        System.out.println("max4 = " + max4);
        System.out.println("min4 = " + min4);

    }
}
