package cn.lxiaol.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * TMS 从数据源 配置
 *
 * @author lxiaol
 * @date 2021/4/15 20:42
 */
@Configuration
@MapperScan(basePackages = "cn.lxiaol.dao.tms", sqlSessionTemplateRef = "tmsSqlSessionTemplate")
public class TmsDataSourceConfig {

    @Bean(name = "tmsDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.tms")
    public DataSource setDataSource() {
        return new DruidDataSource();
    }

    @Bean(name = "tmsTransactionManager")
    public DataSourceTransactionManager setTransactionManager(@Qualifier("tmsDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "tmsSqlSessionFactory")
    public SqlSessionFactory setSqlSessionFactory(@Qualifier("tmsDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        //映射驼峰命名
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        configuration.setMapUnderscoreToCamelCase(true);
        bean.setConfiguration(configuration);
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mybatis/tms/*.xml"));
        return bean.getObject();
    }

    @Bean(name = "tmsSqlSessionTemplate")
    public SqlSessionTemplate setSqlSessionTemplate(@Qualifier("tmsSqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }


}
