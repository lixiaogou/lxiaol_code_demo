package cn.lxiaol.myoa.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
@Data
@Accessors(chain = true)
@TableName("aoa_task_logger")
@ApiModel(value = "AoaTaskLogger", description = "")
public class AoaTaskLogger implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "logger_id", type = IdType.AUTO)

    private Long loggerId;
    private Date createTime;
    private String loggerTicking;
    private Long taskId;
    private String username;
    private Integer loggerStatusid;


}

