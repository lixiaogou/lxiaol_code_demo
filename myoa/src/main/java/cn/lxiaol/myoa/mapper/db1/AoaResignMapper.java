package cn.lxiaol.myoa.mapper.db1;

import cn.lxiaol.myoa.entity.db1.AoaResign;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lxiaol
 * @since 2021-04-20
 */
public interface AoaResignMapper extends BaseMapper<AoaResign> {

}
